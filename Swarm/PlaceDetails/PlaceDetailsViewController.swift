import UIKit
////
////  PlaceDetailsViewController.swift
////  Swarn
////
////  Created by Waseem Ahmed on 27/11/2021.
////
//
//import UIKit
//
//class PlaceDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ImageCollectionDelegate {
//
//    var dataDict : [[String : Any]] = [["cell" : "ImagesCollectionsTableViewCell"] , ["cell" : "CompanyDetailTableViewCell"], ["cell" : "ButtonCollectionTableViewCell"] , ["cell" : "ShowPlaceTableViewCell"],["cell" : "InfoTableViewCell"] , ["cell" : "StatsTableViewCell"] ]
//    var dataDict2 : [[String : Any]] = [["cell" : "HourInfoTableViewCell"] ,["cell" : "HourInfoTableViewCell"] ,["cell" : "HourInfoTableViewCell"] , ["cell" : "InfoMapViewTableViewCell"] , ["cell" : "InfoPlaceDetailTableViewCell"] ]
//    var dataDict3 : [[String : Any]] = [["cell" : "VisitorTableViewCell"] , ["cell" : "VisitorTableViewCell"] , ["cell" : "AddedByTableViewCell"] , ["cell" : "GuideTableViewCell"]]
//    @IBOutlet weak var tableVw: UITableView!
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        tableVw.delegate = self
//        tableVw.dataSource = self
//        tableVw.tag = 1
//        tableVw.register(types: ImagesCollectionsTableViewCell.self, CompanyDetailTableViewCell.self , ButtonCollectionTableViewCell.self , ShowPlaceTableViewCell.self , InfoTableViewCell.self , StatsTableViewCell.self)
//
//        // Do any additional setup after loading the view.
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch tableView.tag {
//        case 1 :
//            return dataDict.count
//        case 2 :
//            return dataDict2.count
//        case 3 :
//            return dataDict3.count
//        default :
//
//            return 0
//        }
//
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//
//        switch tableView.tag {
//        case 1 :
//            let dataCell = dataDict[indexPath.row]
//            let cell =  dataCell["cell"] as? String ?? ""
//
//            switch cell {
//            case "ImagesCollectionsTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: ImagesCollectionsTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//                cellCollection.collectionImageView.delegate = self
//                cellCollection.collectionImageView.dataSource = self
//                cellCollection.collectionImageView.tag = 1
//                cellCollection.collectionImageView.register(types: ImagesCollectionViewCell.self)
//                    return cellCollection
//
//            case "CompanyDetailTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: CompanyDetailTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//
//                    return cellCollection
//
//            case "ButtonCollectionTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: ButtonCollectionTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//                cellCollection.collectionVw.delegate = self
//                cellCollection.collectionVw.dataSource = self
//                cellCollection.collectionVw.tag = 2
//                cellCollection.collectionVw.register(types: ButtonsCollectionViewCell.self)
//                    return cellCollection
//
//            case "ShowPlaceTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: ShowPlaceTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//
//                    return cellCollection
//
//            case "InfoTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: InfoTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//                cellCollection.infoTableVw.delegate = self
//                cellCollection.infoTableVw.dataSource = self
//                cellCollection.infoTableVw.tag = 2
//                cellCollection.infoTableVw.register(types: HourInfoTableViewCell.self , InfoMapViewTableViewCell.self ,InfoPlaceDetailTableViewCell.self )
//
//
//                    return cellCollection
//
//
//            case "StatsTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: StatsTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//                cellCollection.statsTbVw.delegate = self
//                cellCollection.statsTbVw.dataSource = self
//                cellCollection.statsTbVw.tag = 3
//                cellCollection.statsTbVw.register(types: VisitorTableViewCell.self , AddedByTableViewCell.self ,GuideTableViewCell.self )
//
//
//                    return cellCollection
//            default :
//                return UITableViewCell.init(frame: .zero)
//            }
//        case 2 :
//            let dataCell = dataDict2[indexPath.row]
//            let cell =  dataCell["cell"] as? String ?? ""
//
//            switch cell {
//            case "HourInfoTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: HourInfoTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//
//                    return cellCollection
//            case "InfoMapViewTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: InfoMapViewTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//
//                    return cellCollection
//
//            case "InfoPlaceDetailTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: InfoPlaceDetailTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//
//                    return cellCollection
//            default :
//                return UITableViewCell.init(frame: .zero)
//            }
//
//
//        case 3 :
//            let dataCell = dataDict3[indexPath.row]
//            let cell =  dataCell["cell"] as? String ?? ""
//
//            switch cell {
//            case "VisitorTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: VisitorTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//
//                    return cellCollection
//            case "AddedByTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: AddedByTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//
//                    return cellCollection
//
//            case "GuideTableViewCell" :
//                guard let cellCollection = tableView.getCell(type: GuideTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//
//                    return cellCollection
//
//            default :
//                return UITableViewCell.init(frame: .zero)
//
//
//            }
//
//
//
//        default :
//            return UITableViewCell.init(frame: .zero)
//        }
//
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        switch collectionView.tag {
//        case 1 :
//            return 4
//        case 2 :
//            return 3
//        default :
//            return 0
//        }
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        switch collectionView.tag {
//        case 1 :
//            let collCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCollectionViewCell", for: indexPath) as! ImagesCollectionViewCell
//            collCell.imageCollectionDelegate = self
//            return collCell
//        case 2 :
//            let collCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ButtonsCollectionViewCell", for: indexPath) as! ButtonsCollectionViewCell
//
//            return collCell
//        default :
//            return UICollectionViewCell.init(frame: .zero)
//        }
//
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        switch collectionView.tag {
//        case 1 :
//            return CGSize(width: UIScreen.main.bounds.width, height: 300)
//        case 2 :
//            return CGSize(width: UIScreen.main.bounds.width/3 - 10, height: 120)
//        default :
//            return CGSize(width: 0, height: 0)
//        }
//
//    }
//
//    func didTapOnImagePalace() {
//        moveToImageView()
//    }
//    func moveToImageView() {
//        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ImagePreviewerViewController") as! ImagePreviewerViewController
//        controller.modalPresentationStyle = .fullScreen
//        controller.modalTransitionStyle = .crossDissolve
//        self.present(controller, animated: true, completion: nil)
//    }
//
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}

class PlaceDetailsViewController: UIViewController {
    
    @IBOutlet weak var thoughView: UIView!
    @IBOutlet weak var thoughtViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var thoughtSubView: UIView!
    @IBOutlet weak var rateButton: UIButton!
    
    @IBOutlet weak var rate1Button: UIButton!
    @IBOutlet weak var rate2Button: UIButton!
    @IBOutlet weak var rate3Button: UIButton!
    @IBOutlet weak var placeImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        thoughtViewConstraint.constant = 0
        thoughtSubView.isHidden = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageDetailTapped))
        placeImageView.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func writeTipTapped(_ sender: UIButton) {
        let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TipPostViewController") as! TipPostViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    @IBAction func rateTapped(_ sender: Any) {
        thoughtSubView.isHidden = true
        thoughtViewConstraint.constant = thoughtViewConstraint.constant == 0 ? 110 : 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func averageRateButton(_ sender: UIButton) {
        rateButton.setImage(UIImage(named: "brokenHeartGray"), for: .normal)
        rateButton.tintColor = .black
        rate3Button.tintColor = .black
        thoughtSubView.isHidden = false
        thoughtViewConstraint.constant = 160
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func rate1Tapped(_ sender: Any) {
        rateButton.setImage(UIImage(named: "heartGray"), for: .normal)
        rateButton.tintColor = .red
        rate1Button.tintColor = .red
    }
    
    @IBAction func rate2Tapped(_ sender: Any) {
        rateButton.setImage(UIImage(named: "smileyGray"), for: .normal)
        rateButton.tintColor = .black
        rate2Button.tintColor = .black
    }
    
    @objc func imageDetailTapped() {
        let vc = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "ImagePreviewerViewController") as! ImagePreviewerViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func moreTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Check in here", style: .default, handler: { _ in
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChangeLocationViewController") as! ChangeLocationViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Suggest an edit", style: .default, handler: { _ in
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        
    }
    
    @IBAction func crossTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func hoursTapped(_ sender: UIButton) {
        openTextFieldAlert(text: "Enter hours")
    }
    
    @IBAction func phoneTapped(_ sender: UIButton) {
        openTextFieldAlert(text: "Enter phone")
    }
    
    @IBAction func websiteTapped(_ sender: UIButton) {
        openTextFieldAlert(text: "Enter website")
    }
    
    func openTextFieldAlert(text: String) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = text
        }
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
        self.present(alert, animated: true, completion: nil)
    }
    
}
