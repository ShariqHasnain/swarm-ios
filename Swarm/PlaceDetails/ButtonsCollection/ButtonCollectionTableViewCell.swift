//
//  ButtonCollectionTableViewCell.swift
//  Swarn
//
//  Created by Waseem Ahmed on 27/11/2021.
//

import UIKit

class ButtonCollectionTableViewCell: UITableViewCell , Registerable{

    @IBOutlet weak var collectionVw: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
