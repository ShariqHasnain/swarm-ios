//
//  ButtonsCollectionViewCell.swift
//  Swarn
//
//  Created by Waseem Ahmed on 27/11/2021.
//

import UIKit

class ButtonsCollectionViewCell: UICollectionViewCell , Registerable {
    
    @IBOutlet weak var btnName: UILabel!
    @IBOutlet weak var buttonsbtn: UIButton!
}
