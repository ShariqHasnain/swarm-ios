//
//  CompanyDetailTableViewCell.swift
//  Swarn
//
//  Created by Waseem Ahmed on 27/11/2021.
//

import UIKit

class CompanyDetailTableViewCell: UITableViewCell,Registerable {

    @IBOutlet weak var placeTitle: UILabel!
    
    @IBOutlet weak var placeAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
