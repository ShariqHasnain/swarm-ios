//
//  ImagesCollectionsTableViewCell.swift
//  Swarn
//
//  Created by Waseem Ahmed on 27/11/2021.
//

import UIKit

class ImagesCollectionsTableViewCell: UITableViewCell , Registerable {

    @IBOutlet weak var paginationControl: UIPageControl!
    @IBOutlet weak var collectionImageView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
