//
//  ImagesCollectionViewCell.swift
//  Swarn
//
//  Created by Waseem Ahmed on 27/11/2021.
//

import UIKit

protocol ImageCollectionDelegate  {
    func didTapOnImagePalace()
}
class ImagesCollectionViewCell: UICollectionViewCell , Registerable {
    
    @IBOutlet weak var imageVw: UIImageView!
    var imageCollectionDelegate : ImageCollectionDelegate?
    override func awakeFromNib() {
        let tapgeasture = UITapGestureRecognizer.init(target: self, action: #selector(didTapOnImage))
        imageVw.addGestureRecognizer(tapgeasture)
        imageVw.isUserInteractionEnabled = true
    }
    
    @objc func didTapOnImage() {
        imageCollectionDelegate?.didTapOnImagePalace()
    }
}
