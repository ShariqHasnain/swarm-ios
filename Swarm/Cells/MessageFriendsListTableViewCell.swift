//
//  MessageFriendsListTableViewCell.swift
//  Swarn
//
//  Created by Q.M.S on 18/01/2022.
//

import Foundation
import UIKit

class MessageFriendsListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    
    func enableCheckButton(bool: Bool) {
        checkButton.isHidden = !bool
    }
    
}
