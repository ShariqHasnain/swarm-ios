//
//  FriendsVisitedTableViewCell.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class FriendsVisitedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var datatableView: UITableView!
    @IBOutlet weak var friendDataHeightConstraint:
    NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        datatableView.register(UINib(nibName: "FriendVisitedDataTableCell", bundle: nil), forCellReuseIdentifier: "FriendVisitedDataTableCell")
        datatableView.delegate = self
        datatableView.dataSource = self
      
        self.separatorInset = .zero
        self.selectionStyle = .none
        
        
        
        
        initCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCell() {
        self.datatableView.reloadData()
    }
    
}

extension FriendsVisitedTableViewCell : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FriendVisitedDataTableCell", for: indexPath) as? FriendVisitedDataTableCell else {fatalError()}
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0
    }
}
