//
//  FriendVisitedDataTableCell.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class FriendVisitedDataTableCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.separatorInset = .zero
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
