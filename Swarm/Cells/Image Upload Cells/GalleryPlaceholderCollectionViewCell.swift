//
//  GalleryPlaceholderCollectionViewCell.swift
//  Swarn
//
//  Created by Q.M.S on 15/12/2021.
//

import UIKit

class GalleryPlaceholderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with iconName: String, title: String) {
        iconImageView.image = UIImage(named: iconName)
        nameLabel.text = title
    }
    

}
