//
//  Registerable.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 10/11/2021.
//

import UIKit

protocol Registerable: AnyObject {
    static var identifier: String { get }
    static func getNIB() -> UINib
}

extension Registerable {

    static var identifier: String {
        return String(describing: self)
    }
    
    static func getNIB() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
}

protocol Tranistion:AnyObject {
    static func notifyController()
}
extension Tranistion {
    static func notifyController() {}
}
