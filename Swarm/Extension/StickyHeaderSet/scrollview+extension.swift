//
//  scrollview+extension.swift
//  Swarn
//
//  Created by Ruttab Haroon on 28/12/2021.
//

import UIKit

private var xoStickyHeaderKey: UInt8 = 0
extension UIScrollView {
    
    public var stickyHeader: StickyHeader! {
        
        get {
            var header = objc_getAssociatedObject(self, &xoStickyHeaderKey) as? StickyHeader
            
            if header == nil {
                header = StickyHeader()
                header!.scrollView = self
                objc_setAssociatedObject(self, &xoStickyHeaderKey, header, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
            return header!
        }
    }
}
