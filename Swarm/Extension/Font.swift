//
//  Font.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 13/11/2021.
//

import UIKit


public enum Font: String {
    case regular = "Raleway-Regular"
    case medium = "Raleway-Medium"
    case semiBold = "Raleway-SemiBold"
    case bold = "Raleway-Bold"
}

public extension UIFont {
    
    class func regular(of size: CGFloat = 13) -> UIFont {
        return get(font: .regular, size: size)
    }
    
    class func medium(of size: CGFloat = 13) -> UIFont {
        return get(font: .medium, size: size)
    }
    
    class func semiBold(of size: CGFloat = 13) -> UIFont {
        return get(font: .semiBold, size: size)
    }
    
    class func bold(of size: CGFloat = 13) -> UIFont {
        return get(font: .bold, size: size)
    }
    
    
    
    class func get(font: Font = .regular, size: CGFloat = 13) -> UIFont {
        guard let customFont = UIFont(name: font.rawValue, size: size) else {
            fatalError("""
                Failed to load the font.
                Make sure the font file is included in the project and the font name is spelled correctly.
                """
            )
        }
        
        return customFont
    }
}
