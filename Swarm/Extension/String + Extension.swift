//
//  String + Extension.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 10/11/2021.
//

import Foundation
import UIKit

extension String {
    
    var isValidPassword: Bool {
        // least one uppercase,
        // least one digit
        // least one lowercase
        // least one symbol
        //  min 8 characters total
        let password = self.trimmingCharacters(in: CharacterSet.whitespaces)
        let passwordRegx = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&<>*~:`-]).{8,}$"
        let passwordCheck = NSPredicate(format: "SELF MATCHES %@",passwordRegx)
        return passwordCheck.evaluate(with: password)

    }
    
    var isReallyEmpty: Bool {
        return self.trimmingCharacters(in: .whitespaces).isEmpty
    }
    
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func convertToDate(formatter:String) -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter
        if let date = dateFormatter.date(from: self) {
            return date
        }
        
        return nil
    }
    
    func convertDateFormater(from:String, to:String) -> String?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = from
        if let date = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = to
            return dateFormatter.string(from: date)
        }
        
        return nil
    }
    
    func formatCreditCard() -> String {
        if count == 0 {
            return ""
        }
        
        let newString: String = (self as NSString).replacingCharacters(in: (self as NSString).range(of: self), with: self)
        let components: [Any] = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        let decimalString: String = (components as NSArray).componentsJoined(by: "")
        
        let length: Int = decimalString.count
        var index: Int = 0
        var formattedString = String()
        
        if length - index > 4 {
            let fourDigitSet: String = (decimalString as NSString).substring(with: NSRange(location: index, length: 4))
            formattedString += "\(fourDigitSet) "
            index += 4
        }
        
        if length - index > 4 {
            let fourDigitSet: String = (decimalString as NSString).substring(with: NSRange(location: index, length: 4))
            formattedString += "\(fourDigitSet) "
            index += 4
        }
        
        if length - index > 4 {
            let fourDigitSet: String = (decimalString as NSString).substring(with: NSRange(location: index, length: 4))
            formattedString += "\(fourDigitSet) "
            index += 4
        }
        
        if length - index > 4 {
            let fourDigitSet: String = (decimalString as NSString).substring(with: NSRange(location: index, length: 4))
            formattedString += fourDigitSet
            index += 4
        }
        
        let startIndex = decimalString.index(decimalString.startIndex, offsetBy: index)
        let remainder = String(decimalString[startIndex...])
        formattedString.append(remainder)
        return formattedString
    }
    
    func extractDecimalDigits() -> String {
        let strComponents = components(separatedBy: CharacterSet.decimalDigits.inverted)
        return strComponents.joined(separator: "")
    }
    
    var normalize: String {
        return trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var capitalizeFirstLetter: String {
        return prefix(1).uppercased() + dropFirst()
    }

   
}
extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(
            x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
            y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y
        )
        let locationOfTouchInTextContainer = CGPoint(
            x: locationOfTouchInLabel.x - textContainerOffset.x,
            y: locationOfTouchInLabel.y - textContainerOffset.y
        )
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)

        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}

extension RangeExpression where Bound == String.Index  {
    func nsRange<S: StringProtocol>(in string: S) -> NSRange { .init(self, in: string) }
}

func log(_ items: Any..., separator: String = " ", terminator: String = "\n"){
    #if DEBUG
        debugPrint(items)
    #endif
}

extension String
{
    func trim() -> String
   {
    return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
   }
    
    var bearer:[String:String] {
       return ["Authorization" : "Bearer \(self)"]
    }
    
    
    
}

extension Double {
    var priceFormat:String{
        return String(format: "$%.2f", self)
    }
}

extension Encodable {

    /// Converting object to postable dictionary
    func toDictionary(_ encoder: JSONEncoder = JSONEncoder()) throws -> [String: Any] {
        let data = try encoder.encode(self)
        let object = try JSONSerialization.jsonObject(with: data)
        guard let json = object as? [String: Any] else {
            let context = DecodingError.Context(codingPath: [], debugDescription: "Deserialized object is not a dictionary")
            throw DecodingError.typeMismatch(type(of: object), context)
        }
        return json
    }
}

extension String {
    
    var toDate:Date? {
       let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormats.server.rawValue
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.date(from:self)
    }
    
}


enum DateFormats:String {
    case timeonly = "hh:mm a"
    case time24 = "HH:mm:ss"
    case server = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    case serverNonSec = "yyyy-MM-dd'T'HH:mm:ssZ"
    case eeee = "EEEE"
    case withDaysLong = "E, MMM d, hh:mm a"
    case withMonthNameShort = "MMM dd"
    
}
extension NSNotification {
    public static let updateCards: NSNotification.Name  = NSNotification.Name(rawValue:"updateCards")
    public static let updatePricing: NSNotification.Name = NSNotification.Name(rawValue:"updatePricing")
    public static let cartUpdate: NSNotification.Name = NSNotification.Name(rawValue:"cartUpdate")
    
}

