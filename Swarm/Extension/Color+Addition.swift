//
//  Color+Addition.swift
//  FeedU
//
//  Created by Altaf Rehman on 22/06/2021.
//

import UIKit


extension UIColor {
    
    //MARK:-  Color from Hexa code
    func colorFromHexString (_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
        
    }
    
    //MARK:- Swarm theme defined colors
    class var swarmSegmentedTextColor: UIColor {
      return UIColor(red: 254, green: 167, blue: 51, alpha: 1)
    }
    
    class var swarmSegmentedUnderlineColor : UIColor {
        return UIColor(red: 208, green: 120, blue: 22, alpha: 1)
    }
    
    class var swarmBlue : UIColor {
        return UIColor(named: "SwarmBlue")!
    }
    
    class var selectedTextColor : UIColor {
        return UIColor(named: "SegmentSelectedText")!
    }
    
    class var underlineColor : UIColor {
        return UIColor(named: "SegmentUnderLineDark")!
    }
    
    class var btnTextColor : UIColor {
        return UIColor(named: "ButtonTextColor")!
    }
    
    class var backgroundDark : UIColor {
        return UIColor(named: "BackgroundDark")!
    }


}
