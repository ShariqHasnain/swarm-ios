//
//  UIViewExtension.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 10/11/2021.
//

import Foundation
import UIKit

enum GradientDirection {
    case leftToRight
    case rightToLeft
    case topToBottom
    case bottomToTop
    case topLeftToBottomRight
    case bottomLeftToTopRight
    case topRightToBottomLeft
    case bottomRightToTopLeft
}

extension UIView {
    func addGradientBackground(from startColor: UIColor, to endColor: UIColor, direction: GradientDirection, gradientFrame: CGRect? = nil) {
        let gradient = CAGradientLayer()
        if let gFrame = gradientFrame {
            gradient.frame = gFrame
        } else {
            gradient.frame = self.bounds
        }
        
        gradient.colors = [startColor.cgColor, endColor.cgColor]

        switch direction {
        case .topLeftToBottomRight:
            gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        case .bottomLeftToTopRight:
            gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        case .topRightToBottomLeft:
            gradient.startPoint = CGPoint(x: 1.0, y: 0.0)
            gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        case .bottomRightToTopLeft:
            gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
            gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
        case .leftToRight:
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        case .rightToLeft:
            gradient.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
        case .bottomToTop:
            gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradient.endPoint = CGPoint(x: 0.5, y: 0.0)
        default:
            break
        }

        self.layer.insertSublayer(gradient, at: 0)
    }
    
    public func updateGradientLayerFrame() {
        if let subLayer = self.layer.sublayers {
            for layer in subLayer {
                if let gLayer = layer as? CAGradientLayer {
                    gLayer.frame = self.bounds
                }
            }
        }
    }
    
    //MARK: - CONSTRAINT
    var centerXConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .centerX && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    var centerYConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .centerY //&& $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    var heightConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .height && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    var bottomConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .bottom //&& $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    public func round(corners: Corners = .all, radius: CGFloat = 12) {
        clipsToBounds = true
        layer.cornerRadius = radius
        switch corners {
        case .top:
            layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        case .bottom:
            layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        case .topLeft:
            layer.maskedCorners = [.layerMinXMinYCorner]
        case .topRight:
            layer.maskedCorners = [.layerMaxXMinYCorner]
        case .bottomLeft:
            layer.maskedCorners = [.layerMinXMaxYCorner]
        case .bottomRight:
            layer.maskedCorners = [.layerMaxXMaxYCorner]
        case .left:
            layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        case .right:
            layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        case .all:
            layer.maskedCorners = [.layerMinXMinYCorner,
                                   .layerMaxXMinYCorner,
                                   .layerMinXMaxYCorner,
                                   .layerMaxXMaxYCorner]
        case .none:
            log("Do nothing")
        }
    }
    
    public func addBorder(width:CGFloat, color:UIColor = UIColor.black) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        self.layer.masksToBounds = true
    }
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)! [0] as! T
    }
   
    func addBottomShadow(size:CGSize = CGSize(width: 0 , height: 2),opacity:Float = 1,radius:CGFloat = 1) {
        clipsToBounds = false
        layer.masksToBounds = false
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        layer.shadowColor =  #colorLiteral(red: 0.1176470588, green: 0.1529411765, blue: 0.1803921569, alpha: 0.4470572566)
        layer.shadowOffset = size
        layer.shadowPath = UIBezierPath(rect: CGRect(x: 0,
                                                     y: bounds.maxY - layer.shadowRadius,
                                                     width: bounds.width,
                                                     height: layer.shadowRadius)).cgPath
    }
    
    func addTopShadow(size:CGSize = CGSize(width: 0 , height: -2),opacity:Float = 1,radius:CGFloat = 1) {
        clipsToBounds = false
        layer.masksToBounds = false
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        layer.shadowColor =  #colorLiteral(red: 0.1176470588, green: 0.1529411765, blue: 0.1803921569, alpha: 0.4470572566)
        layer.shadowOffset = size
        layer.shadowPath = UIBezierPath(rect: CGRect(x: 0,
                                                     y: bounds.maxY - layer.shadowRadius,
                                                     width: bounds.width,
                                                     height: layer.shadowRadius)).cgPath
    }
    
    
}


public enum Corners {
    case top
    case bottom
    case topLeft
    case topRight
    case left
    case right
    case bottomLeft
    case bottomRight
    case all
    case none
    
}


extension UIViewController {

    func statusBarColorChange(color : UIColor) {

    if #available(iOS 13.0, *) {

        let statusBar = UIView(frame: UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        statusBar.backgroundColor = color
            statusBar.tag = 100
        UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(statusBar)

    } else {

            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
        statusBar?.backgroundColor = .white

        }
    } }


extension UIViewController {
        
  func removeChild() {
    self.children.forEach {
      $0.willMove(toParent: nil)
      $0.view.removeFromSuperview()
      $0.removeFromParent()
    }
  }
}
