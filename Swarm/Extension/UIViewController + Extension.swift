//
//  UIViewController + Extension.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 10/11/2021.
//

import UIKit
import PopupDialog

extension UIViewController {
    
    // Not using static as it wont be possible to override to provide custom storyboardID then
    class var storyboardID : String {
        
        //if your storyboard name is same as ControllerName uncomment it
        //return "\(self)"
        return "\(self)" + "_ID"
        
    }
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
    func showAlert(title: String, message: String) {
        let popup = PopupDialog(title: title, message: message)
        let buttonOne = DefaultButton(title: "Ok", dismissOnTap: false) {
            popup.dismiss(nil)
        }
        popup.addButtons([buttonOne])
        self.present(popup, animated: true, completion: nil)
    }
}


