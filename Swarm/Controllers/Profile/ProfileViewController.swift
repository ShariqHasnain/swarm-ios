//
//  ProfileViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 10/11/2021.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var txtFirstName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtPhone : UITextField!
    @IBOutlet weak var txtGender : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtAddress : UITextField!
    @IBOutlet weak var txtDescription : UITextView!
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var btnProfile : UIButton!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Actions
    @IBAction func btnProfileTapped(_ sender : UIButton) {}
}
