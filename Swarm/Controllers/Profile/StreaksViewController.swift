//
//  StreaksViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 13/11/2021.
//

import UIKit

class StreaksViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        let menuBtn1 = UIButton(type: .custom)
        menuBtn1.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn1.imageView?.contentMode = .scaleAspectFit
        menuBtn1.setImage(UIImage(named:"upload_white"), for: .normal)
        menuBtn1.addTarget(self, action: #selector(tapOnShareBtn), for: .touchUpInside)
        let menuBarItem1 = UIBarButtonItem(customView: menuBtn1)
        let currWidth1 = menuBarItem1.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth1?.isActive = true
        let currHeight1 = menuBarItem1.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight1?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [menuBarItem1], NavigationTitle: "Check-in Streaks" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func tapOnShareBtn() {
        
    }

    // MARK: - Navigation

}
