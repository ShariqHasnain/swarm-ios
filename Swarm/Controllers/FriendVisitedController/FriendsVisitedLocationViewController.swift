//
//  FriendsVisitedLocationViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class FriendsVisitedLocationViewController: BaseViewController {

    @IBOutlet weak var friendsTablView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        friendsTablView.delegate = self
        friendsTablView.dataSource = self
        friendsTablView.register(UINib(nibName: "FriendsVisitedTableViewCell", bundle: nil), forCellReuseIdentifier: "FriendsVisitedTableViewCell")
        friendsTablView.register(UINib(nibName: "OtherPeopleVisitedTableViewCell", bundle: nil), forCellReuseIdentifier: "OtherPeopleVisitedTableViewCell")
        self.friendsTablView.reloadData()
        
        setupNavigationBar()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(moveTo))
        self.view.addGestureRecognizer(tap)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [], NavigationTitle: "Muvi Cinema" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func moveTo() {
        let controller  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FriendSuggestCheckinPlaceViewController") as! FriendSuggestCheckinPlaceViewController
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}


extension FriendsVisitedLocationViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsVisitedTableViewCell", for: indexPath) as? FriendsVisitedTableViewCell else {fatalError()}
            cell.initCell()
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "OtherPeopleVisitedTableViewCell", for: indexPath) as? OtherPeopleVisitedTableViewCell else {fatalError()}

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item == 1 {
            return 52
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item == 1 {
            return 52
        }
         return 268
    }
}
