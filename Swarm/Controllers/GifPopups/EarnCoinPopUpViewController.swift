//
//  EarnCoinPopUpViewController.swift
//  Swarn
//
//  Created by Waseem Ahmed on 28/11/2021.
//

import UIKit
import SwiftGifOrigin

class EarnCoinPopUpViewController: UIViewController {

    @IBOutlet weak var coinImg: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        coinImg.loadGif(name: "coin-small")
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
