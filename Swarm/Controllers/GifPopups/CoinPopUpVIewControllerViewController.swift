//
//  CoinPopUpVIewControllerViewController.swift
//  Swarn
//
//  Created by Waseem Ahmed on 27/11/2021.
//

import UIKit
import SwiftGifOrigin

class CoinPopUpVIewControllerViewController: UIViewController {

    @IBOutlet weak var coinImage: UIImageView!
    var closeController : ((Bool)->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        coinImage.loadGif(name: "Coins")

        
        DispatchQueue.main.asyncAfter(wallDeadline: .now() + 2) {
            self.dismiss(animated: true) {
                self.closeController?(true)
            }
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
