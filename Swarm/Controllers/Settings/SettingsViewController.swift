//
//  SettingsViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 14/11/2021.
//

import UIKit

class SettingsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            
            tableView.register(types: SettingCell.self, SettingsHeaderCell.self)
            tableView.tableFooterView = UIView()
        }
    }
    
    enum SettingRows: String {
        
        case notify = "Notify me when my friends check in",
             notification = "Notification Settings",
             notificationSound = "Notification Sound",
             ringtone = "Ringtone",
             userid = "User ID",
             profile = "Edit your profile",
             password = "Change your password",
             privacy = "Privacy Settings",
             thirdParty = "Third party applications",
             social = "Facebook & Twitter",
             rate = "Like Swarm? Rate this app.",
             support = "Support And FAQ",
             feedback = "Send Feedback",
             bug = "Send bug report",
             distance = "Distance Unit",
             sound = "Sound effects",
             about = "About swarm",
             beta = "Beta Program",
             logout = "Log out"
            
    }
    
    enum SettingSections: String , CaseIterable {
        case notifications = "Notifications",
             account = "Account Settings",
             feedback = "Feedback & Support",
             device = "Device",
             about = "About"
    }
    
    var sections: [SettingSections: [SettingRows]] = [
        .notifications: [
            .notify, .notification, .notificationSound, .ringtone
        ],
        .account:[
            .userid, .profile, .password, .privacy, .thirdParty, .social
        ],
        .feedback : [
            .rate, .support, .feedback, .bug
        ],
        .device: [
            .distance, .sound
        ],
        .about: [
            .about , .beta , .logout
        ]
    ]
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
//        self.navigationController?.isNavigationBarHidden = false
        setupNavigationBar()
        //Remove shadow bar image
//        self.navigationController?.navigationBar.shadowImage = UIImage(named: "")

    }
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [], NavigationTitle: "Setting" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    @objc func  tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension SettingsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.getCell(type: SettingsHeaderCell.self) else { return UITableViewCell() }
        
        let section = SettingSections.allCases[section]
        cell.set(title: section.rawValue)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = SettingSections.allCases[section]
        return sections[section]?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = SettingSections.allCases[indexPath.section]
        guard let cell = tableView.getCell(type: SettingCell.self),
              let row = sections[section] else { return UITableViewCell() }
        cell.selectionStyle = .none
        if indexPath.section == 0 {
            if indexPath.row == 2 {
                cell.toggle.isHidden = false
            } else {
                cell.toggle.isHidden = true
            }
        }
        
        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            cell.configure(title: row[indexPath.row].rawValue, addSeperator: true)
            
        } else {
            cell.configure(title: row[indexPath.row].rawValue, addSeperator: false)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row == 1 { //profile
                let vc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.section == 4 {
            if indexPath.row == 2 { //logout
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
                vc.modalPresentationStyle = .overFullScreen
                let navVc = UINavigationController(rootViewController: vc)
                self.navigationController?.dismiss(animated: true, completion: {
                    self.present(navVc, animated: true, completion: nil)
                })
            }
        }
    }
    
}
