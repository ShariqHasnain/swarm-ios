//
//  SettingsHeaderCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 14/11/2021.
//

import UIKit

class SettingsHeaderCell: UITableViewCell, Registerable {

    @IBOutlet weak var lblTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func set(title: String?) {
        lblTitle.text = title
    }

}
