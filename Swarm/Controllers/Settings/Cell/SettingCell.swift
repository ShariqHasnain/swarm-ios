//
//  SettingCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 14/11/2021.
//

import UIKit

class SettingCell: UITableViewCell , Registerable , Shadowable {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var seperator: UIView!
    @IBOutlet weak var toggle: UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(title: String, addSeperator: Bool = true) {
        lblTitle.text = title
        
        if addSeperator {
            seperator.isHidden = false
        } else {
            seperator.isHidden = true
        }
    }
    
}
