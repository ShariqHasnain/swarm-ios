//
//  UpdatePasswordViewController.swift
//  Swarm
//
//  Created by Syed Zeeshan Rizvi on 2/25/22.
//

import UIKit
import PopupDialog
import ProgressHUD
import Alamofire

class UpdatePasswordViewController: UIViewController {

    @IBOutlet weak var textFieldOtp: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var btnUpdate: UIButton!
    
    var email: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textFieldOtp.keyboardType = .numberPad
    }

    @IBAction func btnUpdateTappedHandler(_ sender: Any) {
        guard let otp = textFieldOtp.text, !otp.isReallyEmpty else {
            self.showAlert(title: "Message", message: "OTP is required")
            return
        }
        
        guard let password = textFieldPassword.text, !password.isReallyEmpty else {
            self.showAlert(title: "Message", message: "Password is required")
            return
        }
        guard password.isValidPassword else {
            self.showAlert(title: "Message", message: "Password must have at least one uppercase, one lower case, one digit and one symbol.")
            return
        }
        
        ProgressHUD.show()
        Networking.shared.updatePassword(endPoint: .update, parameters: ["email": email!, "otp": otp, "password": password]) { response in
            ProgressHUD.dismiss()
            switch response {
            case .success(let value):
                if value.status {
                    for item in self.navigationController!.viewControllers {
                        if item is LoginViewController {
                            self.navigationController?.popToViewController(item, animated: true)
                            break
                        }
                    }
                } else {
                    self.showAlert(title: "Message", message: value.message)
                }
               
            case .failure(let error):
                self.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
