//
//  ForgetPasswordViewController.swift
//  Swarm
//
//  Created by Syed Zeeshan Rizvi on 2/25/22.
//

import UIKit
import PopupDialog
import ProgressHUD

class ForgetPasswordViewController: UIViewController {
    
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        textFieldEmail.keyboardType = .emailAddress
    }
    
    @IBAction func btnNextTappedHandler(_ sender: Any) {
        guard let email = textFieldEmail.text, !email.isReallyEmpty else {
            showAlert(title: "Message", message: "Email is required")
            return
        }
        guard email.isEmail else {
            showAlert(title: "Message", message: "Email fomat is incorrect")
            return
        }
        
        ProgressHUD.show()
        Networking.shared.forgetPassword(endPoint: .forgot, parameters: ["email": email]) { response in
            ProgressHUD.dismiss()
            switch response {
            case .success(let value):
                if value.status {
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "UpdatePasswordViewController") as! UpdatePasswordViewController
                    controller.email = email
                    self.navigationController?.pushViewController(controller, animated: true)
                } else {
                    self.showAlert(title: "Message", message: value.message)
                }
            case .failure(let error):
                self.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
