//
//  LoginViewController.swift
//  Swarn
//
// Created by Muhammad Essa Ali on on 06/11/2021.
//

import UIKit
import ProgressHUD
import PopupDialog
import RxSwift

class LoginViewController: BaseViewController, UITextFieldDelegate, MRSelectCountryDelegate {

    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var forgetPass: UIButton!
    @IBOutlet weak var phoneNumberStack: UIStackView!
    var leftnavigationBar : [UIBarButtonItem]?
    var rightnavigationBar : [UIBarButtonItem]?
    @IBOutlet weak var emailAddressStack: UIStackView!
    
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var emailAddtxt: UITextField!
    @IBOutlet weak var emailAddressText: UIButton!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var stackPassword: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailAddtxt.delegate = self
        phoneText.delegate = self
        phoneText.keyboardType = .numberPad
        emailAddtxt.keyboardType = .emailAddress
        
       #if DEBUG
        emailAddtxt.text = "test@user.com" // "syedzeeshanrizvi@hotmail.com"
        txtPassword.text = "click123" // "Shan123!"
       #endif
        
        // Do any additional setup after loading the view.
        let button1 = UIBarButtonItem(image: UIImage(named: "Icon feather-arrow-down"), style: .plain, target: self, action: #selector(taponBackBtn))
    
        ForgetPasswordCustom(navigationLeftBarButtonItems: [button1], navigationRightBarButtonsItems: [], NavigationTitle: "Log in", isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
               UIColor.white]
    }
    
    @IBAction func btnForgetTappedHandler(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            phoneNumberStack.isHidden = true
            emailAddressStack.isHidden = false
            stackPassword.isHidden = false
            loginBtn.setTitle("Login", for: .normal)
            loginBtn.isHidden = false
            forgetPass.isHidden = false
        }else if textField.tag == 0 {
            emailAddressStack.isHidden = true
            phoneNumberStack.isHidden = false
            stackPassword.isHidden = true
            loginBtn.isHidden = false
            loginBtn.setTitle("Next", for: .normal)
            forgetPass.isHidden = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 0 {
            if let text = phoneText.text, text.isReallyEmpty {
                emailAddressStack.isHidden = false
                phoneNumberStack.isHidden = false
                stackPassword.isHidden = false
                loginBtn.isHidden = true
                forgetPass.isHidden = true
            } else {
                emailAddressStack.isHidden = true
                stackPassword.isHidden = false
                loginBtn.isHidden = false
                forgetPass.isHidden = false
            }
           
        }else if textField.tag == 1 {
            if let text = emailAddtxt.text, text.isReallyEmpty {
                emailAddressStack.isHidden = false
                phoneNumberStack.isHidden = false
                stackPassword.isHidden = false
                loginBtn.isHidden = true
                forgetPass.isHidden = true
            } else {
                phoneNumberStack.isHidden = true
                stackPassword.isHidden = false
                loginBtn.isHidden = false
                forgetPass.isHidden = false
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @objc func taponBackBtn() {
        self.navigationController?.popViewController(animated: true)
     
    }
    
    func movetoHome() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ScreenBoardViewController") as! ScreenBoardViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
  
    @IBAction func btnCountryTappedHandler(_ sender: Any) {
        let controller = MRSelectCountry.getMRSelectCountryController(delegate: self)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction func didTapOnLoginBtn(_ sender: Any) {
//            self.login(parameter: ["fb_id": "4234234", "social_data": ["email": "syed@asds.com"], "type": LoginType.fb.rawValue])
//return
        if phoneNumberStack.isHidden {
            if let text = emailAddtxt.text, text.isReallyEmpty {
                self.showAlert(title: "Message", message: "Email is required")
                return
            } else if let text = emailAddtxt.text, !text.isEmail {
                self.showAlert(title: "Message", message: "Email format is incorrect")
                return
            } else {
                if let text = txtPassword.text, text.isReallyEmpty {
                    self.showAlert(title: "Message", message: "Password is required")
                    return
                }
                if let text = txtPassword.text, !text.isValidPassword {
                    self.showAlert(title: "Message", message: "Password must have at least one uppercase, one lower case, one digit and one symbol.")
                    return
                }
                login(parameter: ["password": txtPassword.text!, "email": emailAddtxt.text!, "type": LoginType.defaultLogin.rawValue])
            }
        } else if emailAddressStack.isHidden {
            if let text = phoneText.text, text.isReallyEmpty {
                self.showAlert(title: "Message", message: "Phone Number is required")
                return
            } else {
                if loginBtn.title(for: .normal) == "Next" {
                    stackPassword.isHidden = false
                    loginBtn.isHidden = false
                    forgetPass.isHidden = false
                    loginBtn.setTitle("Login", for: .normal)
                    return
                }
                if let text = txtPassword.text, text.isReallyEmpty {
                    self.showAlert(title: "Message", message: "Password is required")
                    return
                }
                login(parameter: ["password": txtPassword.text!, "Phone_number": lblNumber.text ?? "" + phoneText.text!, "type": LoginType.defaultLogin.rawValue])
            }
        }
       
        
        

    }
    
    func didSelectCountry(controller: MRSelectCountryTableViewController, country: MRCountry) {
        lblNumber.text = country.dialCode
        controller.navigationController?.popViewController(animated: true)
    }

    func login(parameter: [String: Any]) {
        ProgressHUD.show()
        Networking.shared.login(endPoint: .login, parameters: parameter) { [weak self] result in
            ProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            switch result {
            case.success(let value):
                if value.status {
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(value.data!.token, forKey: "token")
                    userDefaults.synchronize()
                    strongSelf.movetoHome()
                } else {
                    
                    strongSelf.showAlert(title: "Message", message: value.message)
                }
              
            case .failure(let error):
                strongSelf.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
    
   

}

