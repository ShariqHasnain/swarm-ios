//
//  MakeSomeFriendTableViewCell.swift
//  Swarn
//
// Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit

class MakeSomeFriendTableViewCell: UITableViewCell , Registerable{

    var navigate: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onAddFriendClick(_ sender: Any) {
        if let v = navigate {
            v()
        }
    }
    
}
