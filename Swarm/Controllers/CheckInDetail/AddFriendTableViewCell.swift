//
//  AddFriendTableViewCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit

class AddFriendTableViewCell: UITableViewCell,Registerable {

    @IBOutlet weak var friendsCheckinLabel: UILabel!
    @IBOutlet weak var checkinLabel: UILabel!
    var onComplete : (()->Void)?
    var onAddFriendTappedComplete : (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    @IBAction func addFriendTapped(_ sender: Any) {
        if let v = onAddFriendTappedComplete {
            v()
        }
    }
    
    
    
    @IBAction func onDateTapped(_ sender: Any) {
        if let v = onComplete {
            v()
        }
    }
}
