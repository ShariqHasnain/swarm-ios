//
//  CheckInDetailViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit

class CheckInLikeDetailViewController: BaseViewController,UITableViewDelegate , UITableViewDataSource, CellDidTapDelegate {


   
    @IBOutlet weak var tableview: UITableView!
    var dataTable : [[String : Any]] = [["cell" : "MakeSomeFriendTableViewCell"] , ["cell" : "AddFriendTableViewCell"] , ["cell" : "UserDetailTableViewCell"]]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(types: MakeSomeFriendTableViewCell.self , AddFriendTableViewCell.self,UserDetailTableViewCell.self)
        
//             if #available(iOS 15.0, *) {
//                 UITableView.appearance().sectionHeaderTopPadding = CGFloat(0)
//             }
        setupNavigationBar()
        
       
    }
    
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"checkImage"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnProfileBtn), for: .touchUpInside)
        let menuBtn2 = UIButton(type: .custom)
        menuBtn2.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn2.imageView?.contentMode = .scaleAspectFit
        menuBtn2.setImage(UIImage(named:"Icon awesome-inbox"), for: .normal)
        menuBtn2.addTarget(self, action: #selector(tapOnInboxBoxBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        let menuBarItem2 = UIBarButtonItem(customView: menuBtn2)
        let currWidth2 = menuBarItem2.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth2?.isActive = true
        let currHeight2 = menuBarItem2.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight2?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [menuBarItem2], NavigationTitle: "")
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = false
        setupNavigationBar()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataTable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let datatable = dataTable[indexPath.row]
        let data = datatable["cell"] as? String ?? ""
        switch data {
        case "MakeSomeFriendTableViewCell" :
            guard let cellCollection = tableView.getCell(type: MakeSomeFriendTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
            cellCollection.navigate = { [weak self] in
                guard let sb = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckinLeaderboardViewController") as? CheckinLeaderboardViewController else {return}
                self?.navigationController?.pushViewController(sb, animated: true)
            }
            return cellCollection

        case "AddFriendTableViewCell" :
            guard let cellCollection = tableView.getCell(type: AddFriendTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
            
            cellCollection.onAddFriendTappedComplete = { [weak self] in
                
                let controller  = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "PersonProfileViewController") as! PersonProfileViewController
                self?.navigationController?.isNavigationBarHidden = false
                self?.navigationController?.pushViewController(controller, animated: true)
            }
            
            cellCollection.onComplete = { [weak self] in
                let controller  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FriendsVisitedLocationViewController") as! FriendsVisitedLocationViewController
                self?.navigationController?.isNavigationBarHidden = false
                self?.navigationController?.pushViewController(controller, animated: true)
            }
            
                return cellCollection
        case "UserDetailTableViewCell" :
            guard let cellCollection = tableView.getCell(type: UserDetailTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
            cellCollection.cellDidTapDelegate = self
            return cellCollection
        default :
            return UITableViewCell.init(frame: .zero)
        }
      
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func tapOnProfileBtn() {
        let storyboardprofile = UIStoryboard(name: "Profile", bundle: nil)
        let controller = storyboardprofile.instantiateViewController(withIdentifier: "ProfileDetailsViewController") as! ProfileDetailsViewController
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func tapOnInboxBoxBtn() {
        let storyboardprofile = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboardprofile.instantiateViewController(withIdentifier: "InboxViewController") as! InboxViewController
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func didCellDidTapDelegate() {
        print("Click tap on cell")
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

}
