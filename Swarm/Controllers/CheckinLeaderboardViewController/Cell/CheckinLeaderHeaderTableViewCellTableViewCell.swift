//
//  CheckinLeaderHeaderTableViewCellTableViewCell.swift
//  Swarn
//
//  Created by Ruttab Haroon on 26/12/2021.
//

import UIKit

protocol CheckinLeaderHeaderCellDelegate {
    func openLeaderBoard()
    func openFriends()
}

class CheckinLeaderHeaderTableViewCellTableViewCell: UITableViewCell, Registerable {

    @IBOutlet weak var leaderboardView: UIView!
    
    var delegate: CheckinLeaderHeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(openLeaderBoard))
        leaderboardView.addGestureRecognizer(gesture)
        // Configure the view for the selected state
    }
    
    
    @IBAction func addFriendsTapped(_ sender: UIButton) {
        delegate?.openFriends()
    }
    
    @objc func openLeaderBoard() {
        delegate?.openLeaderBoard()
    }
    
    
}
