//
//  LeaderboardFriendsTableViewCell.swift
//  Swarn
//
//  Created by Ruttab Haroon on 30/12/2021.
//

import UIKit

class LeaderboardFriendsTableViewCell: UITableViewCell, Registerable {

    @IBOutlet weak var btnnHeart: UIButton!
    
    var oncomplete : (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onHeartTapped(_ sender: Any) {
        btnnHeart.setImage(UIImage(named: "redHeart"), for: .normal)
        if let v = oncomplete {
            v()
        }

    }
    
}
