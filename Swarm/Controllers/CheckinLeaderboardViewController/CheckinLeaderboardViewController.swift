//
//  CheckinLeaderboardViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 26/12/2021.
//

import UIKit


//
//  CheckInDetailViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit

class CheckinLeaderboardViewController: BaseViewController,UITableViewDelegate , UITableViewDataSource, CellDidTapDelegate {

    @IBOutlet weak var tableViiew: UITableView!
    
    var dataTable : [[String : Any]] = [
        ["cell" : "CheckinLeaderHeaderTableViewCellTableViewCell"],
        ["cell" : "LeaderboardFriendsTableViewCell"],
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViiew.delegate = self
        tableViiew.dataSource = self
        tableViiew.register(types: CheckinLeaderHeaderTableViewCellTableViewCell.self ,
                            AddFriendTableViewCell.self,
                            LeaderboardFriendsTableViewCell.self)
        
         if #available(iOS 15.0, *) {
             UITableView.appearance().sectionHeaderTopPadding = CGFloat(0)
         }
        setupNavigationBar()
        
       
    }
    
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"checkImage"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnProfileBtn), for: .touchUpInside)
        let menuBtn2 = UIButton(type: .custom)
        menuBtn2.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn2.imageView?.contentMode = .scaleAspectFit
        menuBtn2.setImage(UIImage(named:"Icon awesome-inbox"), for: .normal)
        menuBtn2.addTarget(self, action: #selector(tapOnInboxBoxBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        let menuBarItem2 = UIBarButtonItem(customView: menuBtn2)
        let currWidth2 = menuBarItem2.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth2?.isActive = true
        let currHeight2 = menuBarItem2.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight2?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [menuBarItem2], NavigationTitle: "")
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = false
        setupNavigationBar()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataTable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let datatable = dataTable[indexPath.row]
        let data = datatable["cell"] as? String ?? ""
        switch data {
        case "CheckinLeaderHeaderTableViewCellTableViewCell" :
            guard let cellCollection = tableView.getCell(type: CheckinLeaderHeaderTableViewCellTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
            cellCollection.delegate = self
                return cellCollection
        case "LeaderboardFriendsTableViewCell":
            guard let cellCollection = tableView.getCell(type: LeaderboardFriendsTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
            cellCollection.selectionStyle = .none
            cellCollection.oncomplete = {
                self.tableViiew.reloadData()
            }
                return cellCollection
        default :
            return UITableViewCell.init(frame: .zero)
        }
      
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 155 : 127
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func tapOnProfileBtn() {
        let storyboardprofile = UIStoryboard(name: "Profile", bundle: nil)
        let controller = storyboardprofile.instantiateViewController(withIdentifier: "ProfileDetailsViewController") as! ProfileDetailsViewController
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func tapOnInboxBoxBtn() {
        let storyboardprofile = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboardprofile.instantiateViewController(withIdentifier: "InboxViewController") as! InboxViewController
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func didCellDidTapDelegate() {
        print("Click tap on cell")
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

}

extension CheckinLeaderboardViewController: CheckinLeaderHeaderCellDelegate {
    
    func openLeaderBoard() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeadershipBoardViewController") as! LeadershipBoardViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openFriends() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactListViewController") as! ContactListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
