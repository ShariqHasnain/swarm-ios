//
//  CheckInViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 10/11/2021.
//

import UIKit
import AttachmentInput
import RxSwift
import RxDataSources

class CheckInViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, CellDidTapDelegate, WritePostDelegate {

    @IBOutlet weak var Cemrabutton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var smileyButton: UIButton!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    private var attachmentInput: AttachmentInput!
    private var showInputView = false
    private var bottomView: UIView!
    var dataTable : [[[String : Any]]] = [
        [["cell" : "UserDetailTableViewCell" ]],
         [["cell" : "PhotoUploadTableViewCell" ]],
        [["cell" : "CheckInDetailTableViewCell" ]],
         [["cell" : "CoinDetailTableViewCell" ]],
          [["cell" : "TipSectionTableViewCell"]],
        [["cell" : "PlaceVisitedStatsTableViewCell"]],
         [["cell": "CommentTableViewCell"]]
    ]
    
    var selectedImage: UIImage? = nil
    
    let pickerController = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Cemrabutton.setTitle("", for: .normal)
        smileyButton.setTitle("", for: .normal)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.mediaTypes = ["public.image"]
        pickerController.sourceType = .photoLibrary
//        smileyButton.setImage(UIImage(named: "smile"), for: .normal)
        self.tableView.register(types: UserDetailTableViewCell.self,PhotoUploadTableViewCell.self,CheckInDetailTableViewCell.self,CoinDetailTableViewCell.self,CommentTableViewCell.self, TipSectionTableViewCell.self, PlaceVisitedStatsTableViewCell.self)
        setupBottomView()
        setupAttachmentInput()
        commentTextField.delegate = self
    
//        if #available(iOS 15.0, *) {
//            UITableView.appearance().sectionHeaderTopPadding = CGFloat(0)
//        } else {
//            // Fallback on earlier versions
//        }
            
        setupNavigationBar()

        
    }
    
    private func setupBottomView() {

        self.bottomView = CheckInAccessoryView.getView(target: self, action: #selector(toggleFirstResponder))
        self.bottomView.isHidden = true
    }
    
    private func setupAttachmentInput() {
        let config = AttachmentInputConfiguration()
        config.thumbnailSize = CGSize(width: 105 * UIScreen.main.scale, height: 105 * UIScreen.main.scale)
        self.attachmentInput = AttachmentInput(configuration: config)
        self.attachmentInput.delegate = self
    }
    
    @objc func toggleFirstResponder() {
        if self.showInputView {
            self.showInputView = false
            self.bottomView.isHidden = true
            self.reloadInputViews()
        } else {
            self.showInputView = true
            self.bottomView.isHidden = false
            self.reloadInputViews()
        }
    }

    override var canBecomeFirstResponder: Bool {
        return true
    }

    override var inputAccessoryView: UIView? {
        if self.isFirstResponder {
            return self.bottomView
        } else {
            return nil
        }
    }

    override var inputView: UIView? {
        if self.isFirstResponder && self.showInputView {
            return self.attachmentInput.view
        } else {
            return nil
        }
    }


    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBtn2 = UIButton(type: .custom)
        menuBtn2.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn2.imageView?.contentMode = .scaleAspectFit
        menuBtn2.setImage(UIImage(named: "morebtn"), for: .normal)
        menuBtn2.tintColor = .white
        menuBtn2.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBtn3 = UIButton(type: .custom)
        menuBtn3.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn3.imageView?.contentMode = .scaleAspectFit
        menuBtn3.setImage(UIImage(named: "upload_white"), for: .normal)
        menuBtn3.tintColor = .white
        menuBtn3.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        let menuBarItem2 = UIBarButtonItem(customView: menuBtn2)
        let currWidth2 = menuBarItem2.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth2?.isActive = true
        let currHeight2 = menuBarItem2.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight2?.isActive = true
        let menuBarItem3 = UIBarButtonItem(customView: menuBtn3)
        let currWidth3 = menuBarItem3.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth3?.isActive = true
        let currHeight3 = menuBarItem3.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight3?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [menuBarItem2,menuBarItem3], NavigationTitle: "AS-Check-in" , isSearchBar: false , isLable: true , isBackColor: false)
       
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            // Sets shadow (line below the bar) to a blank image
        self.navigationController?.navigationBar.shadowImage = UIImage()
     
            // Sets the translucent background color
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataTable.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataTable[section].count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let secRow = dataTable[indexPath.section]
        let data = secRow[indexPath.row]
        let newdata = data["cell"] as? String ?? ""
        switch newdata {
        case "UserDetailTableViewCell":
            guard let cellCollection = tableView.getCell(type: UserDetailTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
            cellCollection.cellDidTapDelegate = self
            cellCollection.commentImageView.image = selectedImage ?? UIImage(named: "fourth")
                return cellCollection
        
        case "PhotoUploadTableViewCell":
            guard let cellCollection = tableView.getCell(type: PhotoUploadTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    
                return cellCollection
        
        case "CheckInDetailTableViewCell" :
            guard let cellCollection = tableView.getCell(type: CheckInDetailTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    
                return cellCollection
        
        case "CoinDetailTableViewCell" :
            guard let cellCollection = tableView.getCell(type: CoinDetailTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    
                return cellCollection
        case "CommentTableViewCell" :
            guard let cellCollection = tableView.getCell(type: CommentTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
            cellCollection.lblTxt.text = data["text"] as? String
                return cellCollection
            
        case "TipSectionTableViewCell" :
            guard let cellCollection = tableView.getCell(type: TipSectionTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    
                return cellCollection
        case "PlaceVisitedStatsTableViewCell":
            guard let cellCollection = tableView.getCell(type: PlaceVisitedStatsTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    
                return cellCollection
        default :
            print("not Found")
            return UITableViewCell.init(frame: .zero)
        }
       
            
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView = Bundle.main.loadNibNamed("CheckInHeader", owner: self, options: nil)?.first as! CheckInHeader
            headerView.writePostDelegate = self
            
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let footerView = Bundle.main.loadNibNamed("CheckInFooterView", owner: self, options: nil)?.first as! CheckInFooterView
        
        if section == 5 {
            let nib = UINib.init(nibName: "CheckInAccessoryView", bundle: nil)
            let viewcustom = nib.instantiate(withOwner: self, options: nil).first as! CheckInAccessoryView
           
            viewcustom.commentTxt.delegate = self
            viewcustom.onPhotoTapped = {
                let vc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "GalleryViewController")
                self.present(vc, animated: true, completion: nil)
            }
            
            return viewcustom
        }
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = .groupTableViewBackground
        seperatorView.height = 50
        return seperatorView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 200
        }
        return 0
    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "ProfileDetailsViewController") as! ProfileDetailsViewController
//                self.tabBarController?.tabBar.isTranslucent = true
//                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func didCellDidTapDelegate() {
        let storyboardprofile = UIStoryboard(name: "Profile", bundle: nil)
                let controller = storyboardprofile.instantiateViewController(withIdentifier: "ProfileDetailsViewController") as! ProfileDetailsViewController
//                self.tabBarController?.tabBar.isTranslucent = true
//                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    func didTapOnWritePost() {
        
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TipPostViewController") as! TipPostViewController
        
        
//        let storyboardprofile = UIStoryboard(name: "Main", bundle: nil)
//                let controller = storyboardprofile.instantiateViewController(withIdentifier: "TipPostViewController") as! TipPostViewController
////                self.tabBarController?.tabBar.isTranslucent = true
////                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func didTapOnHeartBtn(sender: UIButton) {
//        sender.isSelected = !sender.isSelected
//        if sender.isSelected {
//            sender.setImage(UIImage(named: "redHeart-1"), for: .normal)
//        }else {
//            sender.setImage(UIImage(named: "selectedHeart"), for: .normal)
//        }
      
    }
    
    func didTapOnSmileBtn(sender: UIButton) {
//        sender.isSelected = !sender.isSelected
//        if sender.isSelected {
//
////            sender.setImage(UIImage(named: "smilepng"), for: .normal)
//        }else {
////            sender.setImage(UIImage(named: "SelectedSmiley"), for: .normal)
//        }
        
    }
    
    func didTapOnBreakHeart(sender: UIButton) {
//        sender.isSelected = !sender.isSelected
//        if sender.isSelected {
////            sender.setImage(UIImage(named: "heartBreak"), for: .normal)
//        }else {
////            sender.setImage(UIImage(named: "BrokenSelectedHeart"), for: .normal)
//        }
//        sender.tintColor = .gray
    }
    
    func placeDetailTapped() {
        let vc = AppStoryboard.Dashboard.instance.instantiateViewController(withIdentifier: "PlaceDetailsViewController") as! PlaceDetailsViewController
        hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func cameraTapped(_ sender: Any) {
        self.present(pickerController, animated: true, completion: nil)
    }
    
    @IBAction func sendTapped(_ sender: Any) {
//        if smileyButton.title(for: .normal) == "Send" {
            
            dataTable.append([["cell": "CommentTableViewCell", "text": commentTextField.text ?? ""]])
            commentTextField.text = ""
            self.view.endEditing(true)
//        }
    }
    @IBAction func smileyTapped(_ sender: Any) {
        self.view.endEditing(true)

    }
    
    // MARK: - Navigation

}


extension CheckInViewController: AttachmentInputDelegate {
    func inputImage(imageData: Data, fileName: String, fileSize: Int64, fileId: String, imageThumbnail: Data?) {
//        self.viewModel.addData(fileName: fileName, fileSize: fileSize, fileId: fileId, imageThumbnail: imageThumbnail)
    }
    
    func inputMedia(url: URL, fileName: String, fileSize: Int64, fileId: String, imageThumbnail: Data?) {
//        self.viewModel.addData(fileName: fileName, fileSize: fileSize, fileId: fileId, imageThumbnail: imageThumbnail)
    }
    
    func removeFile(fileId: String) {
//        self.viewModel.removeData(fileId: fileId)
    }
    
    func imagePickerControllerDidDismiss() {
        // Do nothing
    }
    
    func onError(error: Error) {
        let nserror = error as NSError
        if let attachmentInputError = error as? AttachmentInputError {
            print(attachmentInputError.debugDescription)
        } else {
            print(nserror.localizedDescription)
        }
    }
}

extension CheckInViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else {
                    return
                }
        selectedImage = image
        tableView.reloadData()
        pickerController.dismiss(animated: true, completion: nil)
    }
}

extension CheckInViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        smileyButton.isHidden = true
        sendButton.isHidden = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField.text?.count == 0 {
            smileyButton.isHidden = false
            sendButton.isHidden = true
        }
        self.tableView.reloadData()
    }
}
