//
//  FriendsViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 10/11/2021.
//

import UIKit

class FriendsViewController: UIViewController {
    

    @IBOutlet weak var searchBarParentView : UIView!
    @IBOutlet weak var tableView : UITableView! {
        didSet {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(types: SwarmingCell.self, HeaderCell.self)
        tableView.tableFooterView = UIView()
        }
    }
    
    private lazy var searchBarView :SearchBar = {
        let searchBar: SearchBar  =  .fromNib()
        searchBar.frame  = CGRect(x: 0, y: 0, width: searchBarParentView.frame
                                    .width, height: searchBarParentView.frame
                                        .height)
        searchBar.placeHolder = "search contacts"
        searchBar.searchdelegate = self
        return searchBar
    }()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

extension FriendsViewController : SearchBarDelegate {
    func textDidChanged(text: String) {
        
    }
    
    func textDidEnd(text: String) {
        
    }
}


extension FriendsViewController : UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let _ : SocialView = .fromNib()
        } else {
            guard let cell = tableView.getCell(type: HeaderCell.self) else { return UIView()}
            return cell
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.getCell(type: SwarmingCell.self) else { return UITableViewCell() }
        return cell
    }
}
