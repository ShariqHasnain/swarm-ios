//
//  FindFriendsViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 13/11/2021.
//

import UIKit
import Contacts

class FindFriendsViewController: BaseViewController {

    @IBOutlet weak var searchBarParentView : UIView!
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var nameHeaderLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    var friendsArray : [FetchedContact]  = []
    
    var isSearching = false
    
    var searchedArray = [FetchedContact]()
    
    let footerView = Bundle.main.loadNibNamed("ButtonAccessoryView", owner: self, options: nil)!.first as! ButtonAccessoryView
    
    private lazy var searchBarView :SearchBar = {
        let searchBar: SearchBar  =  .fromNib()
        searchBar.frame  = CGRect(x: 0, y: 0, width: searchBarParentView.frame
                                    .width, height: searchBarParentView.frame
                                        .height)
        searchBar.placeHolder = "search contacts"
        searchBar.searchdelegate = self
        return searchBar
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        fetchContacts()
        footerView.frame = bottomView.bounds
        bottomView.addSubview(footerView)
        bottomView.isHidden = true
        nameHeaderLabel.isHidden = true
        searchTextField.delegate = self
    }
    func setupNavigationBar() {
           let menuBtn = UIButton(type: .custom)
           menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
           menuBtn.imageView?.contentMode = .scaleAspectFit
           menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
           menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
           let menuBarItem = UIBarButtonItem(customView: menuBtn)
           let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
           currWidth?.isActive = true
           let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
           currHeight?.isActive = true
           
           ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [], NavigationTitle: "New Message" , isSearchBar: false , isLable: true)
           
           UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                   UIColor.white]
       }

     func textViewDidChange(_ textView: UITextView) {
//           placeholderLabel.isHidden = !textView.text.isEmpty
       }
       
       @objc func tapOnBackBtn() {
           self.navigationController?.popViewController(animated: true)
       }

    func fetchContacts() {
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            if granted {
                let keys = [CNContactGivenNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                do {
                    try store.enumerateContacts(with: request, usingBlock: { contact, stopPointer in
                        self.friendsArray.append(FetchedContact(name: contact.givenName, number: contact.phoneNumbers.first?.value.stringValue ?? ""))
                    })
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } catch let err {
                    print(err)
                }
            } else {
                print("Contacts access denied")
            }
        }
    }
    @IBAction func searchTextChanged(_ sender: UITextField) {
        if let text = sender.text {
            filterData(text: text)
            bottomView.isHidden = false
        }
        bottomView.isHidden = true
    }
    
    func filterData(text: String) {
        searchedArray = friendsArray.filter { $0.name.localizedCaseInsensitiveContains(text) }
        if searchedArray.count == 0 {
            isSearching = false
        } else {
            isSearching = true
        }
        self.tableView.reloadData()
    }
    
    @IBAction func btnAddFriendsTapped(_ sender : UIButton) {}
}

extension FindFriendsViewController : SearchBarDelegate {
    func textDidChanged(text: String) {
        
    }
    
    func textDidEnd(text: String) {
        
    }
}

extension FindFriendsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return searchedArray.count
        } else {
            return friendsArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSearching {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageFriendsListTableViewCell", for: indexPath) as! MessageFriendsListTableViewCell
            cell.name.text = searchedArray[indexPath.row].name
            cell.checkButton.isHidden = true
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageFriendsListTableViewCell", for: indexPath) as! MessageFriendsListTableViewCell
            cell.name.text = friendsArray[indexPath.row].name
            cell.checkButton.isHidden = true
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MessageFriendsListTableViewCell
        cell.checkButton.isHidden = false
        nameHeaderLabel.isHidden = false
        if isSearching {
            nameHeaderLabel.text = "  \(searchedArray[indexPath.row].name)  "
        } else {
            nameHeaderLabel.text = "  \(friendsArray[indexPath.row].name)  "
        }
    }
    
}

extension FindFriendsViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            nameHeaderLabel.text = ""
            nameHeaderLabel.isHidden = true
        }
        return true
    }
    
}
