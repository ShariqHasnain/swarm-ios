//
//  CheckInFooterView.swift
//  Swarn
//
//   Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit

class CheckInFooterView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        self.addDropShadowToViewTop(color: .black, topY: 2)
    }

}
