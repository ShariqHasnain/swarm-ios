//
//  SwarmingCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 10/11/2021.
//

import UIKit

class SwarmingCell: UITableViewCell, Registerable {

    @IBOutlet weak var btnAdd : UIButton!
    @IBOutlet weak var lblUser : UIButton!
    @IBOutlet weak var imgProfile : UIImageView!
    
    var didTapAdd : (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnAddTapped(_ sender : UIButton) {
        self.didTapAdd?()
    }
}
