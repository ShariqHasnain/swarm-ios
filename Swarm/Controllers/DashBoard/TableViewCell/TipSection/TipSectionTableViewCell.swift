//
//  TipSectionTableViewCell.swift
//  Swarn
//
//  Created by Ruttab Haroon on 30/12/2021.
//

import UIKit

class TipSectionTableViewCell: UITableViewCell, Registerable {

    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        tableView.register(UINib(nibName: "TipperTableViewCell", bundle: nil), forCellReuseIdentifier: "TipperTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
      
        self.tableView.separatorColor = .clear
        
        initCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCell() {
        self.tableView.reloadData()
    }
    
    
}

extension TipSectionTableViewCell : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TipperTableViewCell", for: indexPath) as? TipperTableViewCell else {fatalError()}
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 112
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0
    }
}
