//
//  TipperTableViewCell.swift
//  Swarn
//
//  Created by Ruttab Haroon on 30/12/2021.
//

import UIKit

class TipperTableViewCell: UITableViewCell, Registerable {

    @IBOutlet weak var upVotedButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        upVotedButton.setTitle("", for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onUpvoted(_ sender: Any) {
        upVotedButton.setImage(UIImage(named:"upvoted_green"), for: .normal)
    }
}
