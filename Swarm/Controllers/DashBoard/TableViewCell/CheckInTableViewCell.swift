//
//  CheckInTableViewCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 10/11/2021.
//

import UIKit

class CheckInTableViewCell: UITableViewCell , Registerable {

    @IBOutlet weak var lblCheckInHotel : UILabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblLocation : UILabel!
    @IBOutlet weak var lblCoin : UILabel!
    @IBOutlet weak var imgCoin : UIImageView!
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var lblDateTime : UILabel!
    @IBOutlet weak var lblBottomLabel : UILabel!
    @IBOutlet weak var btnHeart : UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnHeartTapped(_ sender : UIButton) {}
    
}
