//
//  UserDetailTableViewCell.swift
//  Swarn
//
//   Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit
protocol CellDidTapDelegate {
    func didCellDidTapDelegate()
}
class UserDetailTableViewCell: UITableViewCell,Registerable {

    @IBOutlet weak var imgHeart : UIImageView!
    @IBOutlet weak var lblHeartCount : UILabel!
    @IBOutlet weak var btnHeart : UIButton!
    @IBOutlet weak var commentImageView: UIImageView!
    
    var cellDidTapDelegate : CellDidTapDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func didSelectCell(_ sender: UIButton) {
        self.cellDidTapDelegate?.didCellDidTapDelegate()
    }
    
    @IBAction func btnHeartTapped(_ sender : UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            btnHeart.setImage(UIImage(named: "redHeart"), for: .selected)
            imgHeart.isHidden = false
            lblHeartCount.isHidden = false
            
        } else {
            btnHeart.setImage(UIImage(named: "orangeheart"), for: .normal)
            imgHeart.isHidden = true
            lblHeartCount.isHidden = true
        }
    }
}
