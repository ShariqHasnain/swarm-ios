//
//  MapViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 12/11/2021.
//

import UIKit
import FloatingPanel
import CoreLocation
import GoogleMaps

class MapViewController: UIViewController,FloatingPanelControllerDelegate , CLLocationManagerDelegate{

    @IBOutlet weak var mapView : GMSMapView!
    let fpc = FloatingPanelController()
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        let vc = AppStoryboard.Components.instance.instantiateViewController(identifier: "BottomSheetViewController") as! BottomSheetViewController
        vc.delegate = self
        fpc.delegate = vc
        fpc.contentMode = .fitToBounds
        
        fpc.set(contentViewController: vc)
        
        let appearence = SurfaceAppearance()
        fpc.surfaceView.appearance = appearence
        
        fpc.isRemovalInteractionEnabled = true
//        present(fpc, animated: true, completion: nil)
        mapView.isMyLocationEnabled = true
        
        //Location Manager code to fetch current location
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        
    }
    
    func getCurrentLoc() {
        
        //Your map initiation code
        let camera = GMSCameraPosition.camera(withLatitude: 0, longitude: 0, zoom: 17.0)
//        let mapView = GMSMapView.map(withFrame: CGRectZero, camera: camera)
        self.view = mapView
        self.mapView?.isMyLocationEnabled = true

        //Location Manager code to fetch current location
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        present(fpc, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        fpc.dismiss(animated: true, completion: nil)
        self.navigationController?.navigationBar.isHidden = false
    }
    // MARK: - Floating Panel
    func floatingPanel(_ fpc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout {
        IntrinsicPanelLayout()
    }
    
    
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let location = locations.last

        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)

        self.mapView?.animate(to: camera)

        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()

    }
    

class IntrinsicPanelLayout: FloatingPanelLayout {
    
    let position: FloatingPanelPosition = .bottom
       let initialState: FloatingPanelState = .half
       var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
           return [
            
            .half: FloatingPanelLayoutAnchor(fractionalInset: 0.3, edge: .bottom, referenceGuide: .safeArea),
            
           ]
    }
}
    
    
    
    // MARK: - Actions
    
    @IBAction func btnCrossTapped(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
        fpc.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnShareTapped(_ sender : UIButton) {}


}

extension MapViewController: BottomSheetNavigation {
    func navigateToVisited() {
        fpc.dismiss(animated: true, completion: nil)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlaceCheckinListViewController") as! PlaceCheckinListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSaved() {
        fpc.dismiss(animated: true, completion: nil)
        let vc = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "PlaceDetailsViewController") as! PlaceDetailsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
  
    
}
