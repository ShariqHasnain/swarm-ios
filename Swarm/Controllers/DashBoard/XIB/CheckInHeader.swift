//
//  CheckInHeader.swift
//  Swarn
//
//   Created by Muhammad Essa Ali on 10/11/2021.
//

import UIKit
protocol WritePostDelegate {
    func didTapOnWritePost()
    func didTapOnHeartBtn(sender : UIButton)
    func didTapOnSmileBtn(sender : UIButton)
    func didTapOnBreakHeart(sender : UIButton)
    func placeDetailTapped()
}
class CheckInHeader: UIView {

    @IBOutlet weak var heartbtn: UIButton!
    @IBOutlet weak var smileyButton: UIButton!
    @IBOutlet weak var disHeartButton: UIButton!
    
    @IBOutlet weak var placeDetailButton: UIButton!
    @IBOutlet weak var subView: UIView!
    var writePostDelegate : WritePostDelegate?
    override func awakeFromNib() {
        placeDetailButton.setTitle("", for: .normal)
        

    }
    @IBAction func didTapOnWriteBtn(_ sender: Any) {
        writePostDelegate?.didTapOnWritePost()
    }
    
    
    @IBAction func didTapOnHeartBtn(_ sender: UIButton) {
        deselectAll()
        heartbtn.tintColor = .darkGray
        writePostDelegate?.didTapOnHeartBtn(sender: sender)
    }
    
    @IBAction func didTapOnSmileBtn(_ sender: UIButton) {
        deselectAll()
        smileyButton.tintColor = .darkGray
        writePostDelegate?.didTapOnSmileBtn(sender: sender)
    }
    
    @IBAction func didTapOnHeartBreak(_ sender: UIButton) {
        deselectAll()
        disHeartButton.tintColor = .darkGray
        writePostDelegate?.didTapOnBreakHeart(sender: sender)
    }
    
    func deselectAll() {
        heartbtn.tintColor = .gray
        smileyButton.tintColor = .gray
        disHeartButton.tintColor = .gray
    }
    
    @IBAction func placeDetailTapped(_ sender: UIButton) {
        writePostDelegate?.placeDetailTapped()
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
