//
//  ButtonAccessoryView.swift
//  Example
//
//  Created by daiki-matsumoto on 2019/02/06.
//  Copyright © 2019 cybozu. All rights reserved.
//

import UIKit

class CheckInAccessoryView: UIView {
    
    var onPhotoTapped: (()->Void)?
    
    @IBOutlet weak var photoButton: UIButton!

    @IBOutlet weak var commentTxt: UITextField!
    
   
    static func getView(target: UIViewController, action: Selector) -> CheckInAccessoryView? {
        let nib = UINib.init(nibName: "CheckInAccessoryView", bundle: nil)
        if let view = nib.instantiate(withOwner: self, options: nil).first as? CheckInAccessoryView {
            view.photoButton.addTarget(target, action: action, for: .touchUpInside)
            return view
        }
        return nil
    }

    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize.init(width: size.width, height: 46)
    }
    
    override func layoutMarginsDidChange() {
        super.layoutMarginsDidChange()
        self.invalidateIntrinsicContentSize()
    }
    @IBAction func onPhotoTapped(_ sender: Any) {
        if let v = onPhotoTapped {
            v()
        }
    }
}
