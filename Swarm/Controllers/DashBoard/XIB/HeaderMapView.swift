//
//  HeaderMapView.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit
import GoogleMaps

protocol HeaderMapDelegate: AnyObject {
    func visitedTapped()
    func savedTapped()
    func categoriesTapped()
}

class HeaderMapView: UIView {

    @IBOutlet weak var savedStackVie: UIStackView!
    @IBOutlet weak var visitedStackView: UIStackView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var visitedButton: UIButton!
    @IBOutlet weak var categoriesButton: UIButton!
    
    var delegate: HeaderMapDelegate?
    @IBOutlet weak var savedButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        visitedButton.setTitle("", for: .normal)
        savedButton.setTitle("", for: .normal)
        categoriesButton.setTitle("", for: .normal)
    }
    
    @IBAction func visitedTapped(_ sender: UIButton) {
        delegate?.visitedTapped()
    }
    
    @IBAction func savedTapped(_ sender: UIButton) {
        delegate?.savedTapped()
    }
    
    @IBAction func categoriesTapped(_ sender: UIButton) {
        delegate?.categoriesTapped()
    }
    
}
