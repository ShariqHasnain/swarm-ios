//
//  ImageCollectionViewCell.swift
//  Swarn
//
//  Created by Waseem Ahmed on 26/11/2021.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell , Registerable {
    
    @IBOutlet weak var imagePlace: UIImageView!
}
