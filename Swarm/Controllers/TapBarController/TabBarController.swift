//
//  TabBarController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 12/11/2021.
//

import UIKit
import STTabbar
protocol coinDelegate {
    func didOpenCoin()
}

var isOpenCoin : Bool = false

class TabBarController: UITabBarController {
   
   
    override func viewDidLoad() {
        super.viewDidLoad()
        if let myTabbar = tabBar as? STTabbar {
            
            myTabbar.buttonImage = UIImage(named: "tabMain")
            myTabbar.tintColor = .clear
            myTabbar.centerButtonActionHandler = {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
                self.navigationController?.pushViewController(controller, animated: true)
                print("Center Button Tapped")
                controller.coinPopView = { status in
                    if status {
                       isOpenCoin = true
                    }
                }
                
            }
            
        
        }
        
        let tabGradientView = UIView(frame: self.tabBar.bounds)
               tabGradientView.backgroundColor = UIColor.white
               tabGradientView.translatesAutoresizingMaskIntoConstraints = false;


               self.tabBar.addSubview(tabGradientView)
               self.tabBar.sendSubviewToBack(tabGradientView)
               tabGradientView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

               tabGradientView.layer.shadowOffset = CGSize(width: 0, height: 0)
               tabGradientView.layer.shadowRadius = 4.0
               tabGradientView.layer.shadowColor = UIColor.gray.cgColor
               tabGradientView.layer.shadowOpacity = 0.6
               self.tabBar.clipsToBounds = false
               self.tabBar.backgroundImage = UIImage()
               self.tabBar.shadowImage = UIImage()
               self.tabBar.tintColor = .systemOrange
      
        
      
    }
    
    // MARK: - Navigation


}

