//
//  ViewTipViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 28/11/2021.
//

import UIKit

class ViewTipViewController: UIViewController {

    
    
    @IBOutlet weak var tableView : UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.tableFooterView = UIView()
            tableView.register(types: ViewTipCell.self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
extension ViewTipViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.getCell(type: ViewTipCell.self) else {
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 286
    }
}
