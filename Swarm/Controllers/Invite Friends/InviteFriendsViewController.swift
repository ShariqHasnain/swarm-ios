//
//  InviteFriendsViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 01/12/2021.
//

import UIKit
import Contacts

class InviteFriendsViewController: BaseViewController, SkipViewDelegate {
 
    var tableViewData : [[String : Any]] = [["cell" : "InviteFriendsCell" ] , ["cell" : "InviteFriendsCell"] , ["cell" : "EmptyTableViewCell"] , ["cell" : "EmptyTableViewCell"] , ["cell" : "EmptyTableViewCell"],["cell" : "EmptyTableViewCell"],["cell" : "EmptyTableViewCell"]]
    var friends: [FetchedContact] = []
    
    @IBOutlet weak var tableView : UITableView! {
        didSet {
            tableView.register(types:EmptyTableViewCell.self)
            tableView.delegate = self
            tableView.dataSource = self
      }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     setupNavigationBar()
    fetchContacts()
    }
    
    func fetchContacts() {
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            if granted {
                let keys = [CNContactGivenNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                do {
                    try store.enumerateContacts(with: request, usingBlock: { contact, stopPointer in
                        self.friends.append(FetchedContact(name: contact.givenName, number: contact.phoneNumbers.first?.value.stringValue ?? ""))
                    })
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } catch let err {
                    print(err)
                }
            } else {
                print("Contacts access denied")
            }
        }
    }

    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        let menuBtn1 = UIButton(type: .custom)
        menuBtn1.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn1.imageView?.contentMode = .scaleAspectFit
        menuBtn1.setImage(UIImage(named:"SAVE"), for: .normal)
        menuBtn1.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem1 = UIBarButtonItem(customView: menuBtn1)
        let currWidth1 = menuBarItem1.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth1?.isActive = true
        let currHeight1 = menuBarItem1.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight1?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [menuBarItem1], NavigationTitle: "Add Friends" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didTapOnSkip() {
        print("Skip Btn")
        self.goToDashboard()
        
    }
    
    @IBAction func btnSkipTapped(_ sender : UIButton) {
        goToDashboard()
    }
    
    func goToDashboard(){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "NavigationTabBarViewController") as! NavigationTabBarViewController
        controller.navigationController?.navigationBar.isHidden = true
        controller.modalTransitionStyle = .flipHorizontal
        controller.modalPresentationStyle = .fullScreen
     
        self.present(controller, animated: true, completion: nil)
    }
    
}
extension InviteFriendsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = tableViewData[indexPath.row]
        let dataCell = data["cell"] as? String ?? ""
//        if dataCell == "InviteFriendsCell" {
//            guard let cell = tableView.getCell(type: InviteFriendsCell.self) else { return UITableViewCell()}
//            cell.configureCell(image: imageArray[indexPath.row], btn: btnTitle[indexPath.row])
//            return cell
//        } else
        if dataCell == "EmptyTableViewCell" {
            guard let cell = tableView.getCell(type: EmptyTableViewCell.self) else { return UITableViewCell()}
           
            return cell
        }else {
            return UITableViewCell.init(frame: .zero)
        }
       
    }
  
}
