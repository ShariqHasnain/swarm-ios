//
//  SkipView.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 01/12/2021.
//

import Foundation
import UIKit
protocol SkipViewDelegate {
    func didTapOnSkip()
}
class SkipView : UIView {
    
    var skipViewDelegate : SkipViewDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func btnSkipTapped(_ sender : UIButton) {
        skipViewDelegate?.didTapOnSkip()
    }
}
