//
//  InviteFriendsCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 01/12/2021.
//

import UIKit

class InviteFriendsCell: UITableViewCell, Registerable {

    @IBOutlet weak var btnContacts : UIButton!
    @IBOutlet weak var imgContacts : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func configureCell(image : String , btn : String) {
        btnContacts.setImage(UIImage(named: btn), for: .normal)
        imgContacts.image = UIImage(named: image)
    }
    
    
    @IBAction func btnContactsTapped(_ sender : UIButton) {}
}
