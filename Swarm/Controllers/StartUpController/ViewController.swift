//
//  ViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 05/11/2021.
//

import UIKit
import SwiftGifOrigin
import AuthenticationServices
import FBSDKCoreKit
import FBSDKLoginKit
import PopupDialog
import ProgressHUD

class ViewController: UIViewController {
    

    @IBOutlet weak var splashImg: UIImageView!
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var btnApple: UIButton!
    @IBOutlet weak var btnSignUpEmail: UIButton!    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.'
        setupLoginButtonText()
        let animated_image =   UIImage.gif(name: "splash")
        splashImg.animationImages = animated_image?.images
        splashImg.animationDuration   = (animated_image?.duration)!
        splashImg.animationRepeatCount = 1
        splashImg.image = animated_image?.images?.last
        splashImg.startAnimating()
        
        facebookBtn.layer.cornerRadius = 22
        btnApple.layer.cornerRadius = 22
        btnSignUpEmail.layer.cornerRadius = 22
        self.title = ""
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func getFacebookUserInfo(){
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email ], viewController: self) { (result) in
            switch result{
            case .cancelled:
                print("Cancel button click")
            case .success:
                let params = ["fields" : "id, name, first_name, last_name, picture.type(large), email "]
                let graphRequest = GraphRequest.init(graphPath: "/me", parameters: params)
                let Connection = GraphRequestConnection()
                Connection.add(graphRequest) { (Connection, result, error) in
                    if let info = result as? [String : AnyObject] {
                        print(info["id"] as! String)
                        self.login(parameter: ["fb_id": info["id"] as! String,
                                               "social_data": "{email: \(info["email"] as! String)}",
                                               "type": LoginType.fb.rawValue])
                    } else if let err = error {
                        self.showAlert(title: "Message", message: err.localizedDescription)
                    }
                }
                Connection.start()
            default:
                print("??")
            }
        }
    }
    
    func setupLoginButtonText() {
        let str1 = "Have a Foursquare account? Log in"
        let str2 = "Log in"
        
        let range = (str1 as NSString).range(of: str2)
        let attributedString = NSMutableAttributedString.init(string: str1)
        attributedString.addAttribute(.font, value: UIFont(name: "Raleway Bold", size: 17), range: range)
        loginButton.setAttributedTitle(attributedString, for: .normal)
    }

    @IBAction func loginWithEmail(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let controller  = storyboard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        controller.isCommingFromLogin = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(controller, animated: true)

    }
    
    
    @IBAction func didTapOnAlreadyHaveAccount(_ sender: Any) {
        let controller  = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.isNavigationBarHidden = false
//        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction func onAppleSigninTapped(_ sender: Any) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
           let request = appleIDProvider.createRequest()
           request.requestedScopes = [.fullName, .email]
           
           let authorizationController = ASAuthorizationController(authorizationRequests: [request])
           authorizationController.delegate = self
           authorizationController.presentationContextProvider = self
           authorizationController.performRequests()
    }
    
    @IBAction func onFBSigninTapped(_ sender: Any) {
        getFacebookUserInfo()
    }
    
    func login(parameter: [String: Any]) {
        ProgressHUD.show()
        Networking.shared.login(endPoint: .login, parameters: parameter) { [weak self] result in
            ProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            switch result {
            case.success(let value):
                if value.status {
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(value.data!.token, forKey: "token")
                    userDefaults.synchronize()
                    strongSelf.movetoHome()
                } else {
                    
                    strongSelf.showAlert(title: "Message", message: value.message)
                }
              
            case .failure(let error):
                strongSelf.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
    
    func movetoHome() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ScreenBoardViewController") as! ScreenBoardViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

extension ViewController: ASAuthorizationControllerDelegate {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
            case let appleIDCredential as ASAuthorizationAppleIDCredential:
                
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            
            print(userIdentifier)
            print(fullName)
            print(email)
            if let ema = email {
                self.login(parameter: ["apple_id": userIdentifier, "social_data": "{email: \(ema)}", "type": LoginType.apple.rawValue])
            } else {
                self.login(parameter: ["apple_id": userIdentifier, "social_data": "", "type": LoginType.apple.rawValue])
            }
                // For the purpose of this demo app, store the `userIdentifier` in the keychain.
//                self.saveUserInKeychain(userIdentifier)
                
                // For the purpose of this demo app, show the Apple ID credential information in the `ResultViewController`.
//                self.showResultViewController(userIdentifier: userIdentifier, fullName: fullName, email: email)
            
            case let passwordCredential as ASPasswordCredential:
            
                // Sign in using an existing iCloud Keychain credential.
                let username = passwordCredential.user
                let password = passwordCredential.password
                
                // For the purpose of this demo app, show the password credential as an alert.
//                DispatchQueue.main.async {
//                    self.showPasswordCredentialAlert(username: username, password: password)
//                }
                
            default:
                break
            }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error)
    }
}


extension UIViewController: ASAuthorizationControllerPresentationContextProviding {
    public func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}


