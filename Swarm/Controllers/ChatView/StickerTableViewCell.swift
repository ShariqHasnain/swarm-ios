//
//  StickerTableViewCell.swift
//  Swarn
//
//  Created by Waseem Ahmed on 26/11/2021.
//

import UIKit

class StickerTableViewCell: UITableViewCell, Registerable {

    @IBOutlet weak var stickerImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
