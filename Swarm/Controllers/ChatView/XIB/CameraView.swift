//
//  CameraView.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 27/11/2021.
//

import UIKit
import AVFoundation

class CameraView : UIView {
//    @IBOutlet weak var view : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    // AVFoundation properties
      let captureSession = AVCaptureSession()
      var captureDevice: AVCaptureDevice!
      var captureDeviceFormat: AVCaptureDevice.Format?
      let stillImageOutput = AVCapturePhotoOutput()
      var cameraLayer: AVCaptureVideoPreviewLayer?

      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          initCamera()
      }

      func initCamera() {
          captureSession.beginConfiguration()


//          stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]

          // get the back camera
          if let device = cameraDeviceForPosition(position: AVCaptureDevice.Position.back) {

              captureDevice = device
              captureDeviceFormat = device.activeFormat

              let error: NSErrorPointer = nil

              do {
                  try captureDevice!.lockForConfiguration()
              } catch let error1 as NSError {
//                  error.memory = error1
              }
              captureDevice!.focusMode = AVCaptureDevice.FocusMode.locked
              captureDevice!.unlockForConfiguration()

              var deviceInput: AVCaptureDeviceInput!
              do {
                  deviceInput = try AVCaptureDeviceInput(device: captureDevice)
              } catch let error1 as NSError {
//                  error.memory = error1
                  deviceInput = nil
              }
              if(error == nil) {
                  captureSession.addInput(deviceInput)
              }

              captureSession.addOutput(stillImageOutput)

              // use the high resolution photo preset
              captureSession.sessionPreset = AVCaptureSession.Preset.photo


              // setup camera preview
              cameraLayer = AVCaptureVideoPreviewLayer(session: captureSession)


              if let player = cameraLayer {
                  player.videoGravity = AVLayerVideoGravity.resizeAspectFill
                  self.layer.addSublayer(player)
                  player.frame = self.layer.bounds
                  player.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeRight
              }

              // commit and start capturing
              captureSession.commitConfiguration()
              captureSession.startRunning()
          }

          captureSession.commitConfiguration()
      }

      func setFocusWithLensPosition(pos: CFloat) {
          let _: NSErrorPointer = nil
          do {
              try captureDevice!.lockForConfiguration()
          } catch let error1 as NSError {
//              error.memory = error1
          }
          captureDevice!.setFocusModeLocked(lensPosition: pos, completionHandler: nil)
          captureDevice!.unlockForConfiguration()
      }

      // return the camera device for a position
    func cameraDeviceForPosition(position:AVCaptureDevice.Position) -> AVCaptureDevice?
      {
          for device:AnyObject in AVCaptureDevice.devices() {
              if (device.position == position) {
                  return device as? AVCaptureDevice;
              }
          }

          return nil
      }

    @IBAction func btnSendTapped(_ sender : UIButton) {}
    @IBAction func btnCameraFlipTapped(_ sender : UIButton) {}
}
