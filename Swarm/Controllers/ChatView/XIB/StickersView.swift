//
//  StickersView.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 27/11/2021.
//

import Foundation
import UIKit

class StickersView : UIView {
    
    @IBOutlet weak var tableView : UITableView! {
        didSet {
            //tableView.register(types: StickerCell.self)
//            tableView.delegate = self
//            tableView.dataSource = self
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//extension StickersView : UITableViewDelegate , UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 4
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.getCell(type: StickerCell.self) else { return UITableViewCell()}
//        return cell
//    }
//}
