//
//  ChatViewController.swift
//  Swarn
//
//  Created by Waseem Ahmed on 24/11/2021.
//

import UIKit
import AttachmentInput
import RxSwift
import RxDataSources


class ChatViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var bottomMsgView: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    var count = 0
    var typeIncomming : String = ""
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var swamMsgView: UIView!
    private var attachmentInput: AttachmentInput!
    @IBOutlet weak var msgText: UITextField!
    var dataDict : [[String : Any]] = [["cellType" : "msg_incomming" , "msg" : "HI"] , ["cellType" : "msg_outgoing" , "msg" : "I am Fine"] , ["cellType" : "msg_image" , "img" : UIImage(named: "square_profile_picture")!],["cellType" : "msg_sticker" , "img" : "sticker1"]]
    
    let pickerController = UIImagePickerController()

    
    private var showInputView = false
//    private var bottomView: UIView!
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        chatTableView.dataSource = self
        chatTableView.delegate = self
        setupAttachmentInput()
        chatTableView.register(types: IncommingTableViewCell.self , OutGoingCellTableViewCell.self , ImageViewTableViewCell.self,StickerTableViewCell.self,EmptyTableViewCell.self)
    
        msgText.delegate = self
        
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.mediaTypes = ["public.image"]
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            pickerController.sourceType = .camera
        } else {
            pickerController.sourceType = .savedPhotosAlbum
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !self.isFirstResponder {
            self.becomeFirstResponder()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isFirstResponder {
            self.resignFirstResponder()
        }
    }

    private func setupAttachmentInput() {
        let config = AttachmentInputConfiguration()
        config.thumbnailSize = CGSize(width: 105 * UIScreen.main.scale, height: 105 * UIScreen.main.scale)
        self.attachmentInput = AttachmentInput(configuration: config)
        self.attachmentInput.delegate = self
    }
    
    override var inputView: UIView? {
        if self.isFirstResponder && self.showInputView {
            return self.attachmentInput.view
        } else {
            return nil
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
    }
    
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        return true
    }
    
    //MARK: Table View delegates
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataDic = self.dataDict[indexPath.row]
        typeIncomming = dataDic["cellType"] as? String ?? ""
        if typeIncomming == "msg_incomming" {
            let incommingCell = tableView.dequeueReusableCell(withIdentifier: "IncommingTableViewCell", for: indexPath) as! IncommingTableViewCell
           
            
            incommingCell.msgSendTxt.text = dataDic["msg"] as? String ?? ""
            return incommingCell
        }else if typeIncomming == "msg_outgoing" {
            let outGoingCell = tableView.dequeueReusableCell(withIdentifier: "OutGoingCellTableViewCell", for: indexPath)  as! OutGoingCellTableViewCell
            let dataDic = self.dataDict[indexPath.row]
            outGoingCell.msgSendTxt.text = dataDic["msg"] as? String ?? ""
            
            return outGoingCell
        }else if typeIncomming == "msg_image" {
            let msgCell = tableView.dequeueReusableCell(withIdentifier: "ImageViewTableViewCell", for: indexPath) as! ImageViewTableViewCell
            
            msgCell.imageVw.image = (dataDic["img"] as! UIImage)
            
            return msgCell
            
        }else if typeIncomming == "msg_sticker" {
            let stickerCell = tableView.dequeueReusableCell(withIdentifier: "StickerTableViewCell", for: indexPath) as! StickerTableViewCell
            
            stickerCell.stickerImg.image = UIImage(named: dataDic["img"] as? String ?? "")
            
            return stickerCell
            
            
        }else if typeIncomming == "msg_Empty"{
            let stickerCell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell", for: indexPath) as! EmptyTableViewCell
            
           
            return stickerCell
            
            
        
        }else {
            return UITableViewCell.init(frame: .zero)
        }
       
    }
    
    //MARK: Actions
    
    @IBAction func btnSmileyTapped(_ sender : UIButton) {
//        let stickers : StickersView = .fromNib()
//        chatTextView.inputAccessoryView = stickers
    }
    
    @IBAction func btnCameraTapped(_ sender : UIButton) {
        self.present(pickerController, animated: true, completion: nil)
    }
    
//    override var inputAccessoryView: UIView? {
//        if self.isFirstResponder {
////            return self.bottonView
//        } else {
//            return nil
//        }
//    }

    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    @IBAction func didSendMsg(_ sender: Any) {
        if sendButton.title(for: .normal) == "Send" {
            dataDict.append(["cellType" : "msg_outgoing" , "msg" : msgText.text ?? ""])
            chatTableView.reloadData()
            chatTableView.scrollToBottom()
        }
    }
    
    
    @IBAction func didTapCloseMsgBtn(_ sender: Any) {
        swamMsgView.isHidden = true
    }
    
    @IBAction func btnCrossTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func toggleFirstResponder() {
        if self.showInputView {
            self.showInputView = false
//            self.bottonView.isHidden = true
            self.reloadInputViews()
        } else {
            self.showInputView = true
//            self.bottonView.isHidden = false
            self.reloadInputViews()
        }
    }
    
    
}

extension ChatViewController: AttachmentInputDelegate {
    func inputImage(imageData: Data, fileName: String, fileSize: Int64, fileId: String, imageThumbnail: Data?) {
//        self.viewModel.addData(fileName: fileName, fileSize: fileSize, fileId: fileId, imageThumbnail: imageThumbnail)
    }
    
    func inputMedia(url: URL, fileName: String, fileSize: Int64, fileId: String, imageThumbnail: Data?) {
//        self.viewModel.addData(fileName: fileName, fileSize: fileSize, fileId: fileId, imageThumbnail: imageThumbnail)
    }
    
    func removeFile(fileId: String) {
//        self.viewModel.removeData(fileId: fileId)
    }
    
    func imagePickerControllerDidDismiss() {
        // Do nothing
    }
    
    func onError(error: Error) {
        let nserror = error as NSError
        if let attachmentInputError = error as? AttachmentInputError {
            print(attachmentInputError.debugDescription)
        } else {
            print(nserror.localizedDescription)
        }
    }
}


extension ChatViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        sendButton.setImage(UIImage(), for: .normal)
        sendButton.setTitle("Send", for: .normal)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField.text?.count == 0 {
            sendButton.setImage(UIImage(named: "Stickers"), for: .normal)
            sendButton.setTitle("", for: .normal)
        }
    }

}


extension ChatViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else {
                    return
                }
        dataDict.append(["cellType" : "msg_image" , "img" : image])
        chatTableView.reloadData()
        chatTableView.scrollToBottom()
    
        pickerController.dismiss(animated: true, completion: nil)
    }
}
