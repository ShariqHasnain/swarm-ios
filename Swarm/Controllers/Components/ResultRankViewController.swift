//
//  ResultRankViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class ResultRankViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let gesture = UITapGestureRecognizer(target: self, action: #selector(moveereTo))
        self.view.addGestureRecognizer(gesture)
    }
    
    @objc func moveereTo(){
        self.dismiss(animated: true) {
            let storyboard = UIStoryboard.init(name: "Trivia", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "TriviaQuestionViewController") as! TriviaQuestionViewController
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalPresentationStyle  = .overFullScreen
            controller.modalTransitionStyle = .crossDissolve
            if let vc = UIApplication.getTopViewController() {
                vc.present(controller, animated: true, completion: nil)
            }
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
