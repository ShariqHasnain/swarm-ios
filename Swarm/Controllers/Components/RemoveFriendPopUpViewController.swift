//
//  RemoveFriendPopUpViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 28/11/2021.
//

import UIKit
import DropDown

class RemoveFriendPopUpViewController: UIViewController {

    var onComplete : ((Bool)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func btnYesTapped(_ sender : UIButton) {
        if let v = onComplete {
            v(true)
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnNoTapped(_ sender : UIButton) {
        if let v = onComplete {
            v(false)
        }
        self.dismiss(animated: true, completion: nil)
    }
}
