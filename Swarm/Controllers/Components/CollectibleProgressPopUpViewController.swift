//
//  CollectibleProgressPopUpViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 26/11/2021.
//

import UIKit
import SwiftGifOrigin

class CollectibleProgressPopUpViewController: UIViewController {

    @IBOutlet weak var popUpView : UIView!
    @IBOutlet weak var imgBackground : UIImageView!
    @IBOutlet weak var imgCheckIn : UIImageView!
    @IBOutlet weak var lblProgressOutof1000 : UILabel!
    @IBOutlet weak var lblPlaceCategory : UILabel!
    @IBOutlet weak var progressBar : UIProgressView!
    @IBOutlet weak var lblTopCheckInPlace : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imgBackground.loadGif(name: "Spiral")
        
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        popUpView.cornerRadius = 8
        progressBar.layer.cornerRadius = 10
    }
    
    @IBAction func btnCrossTapped(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnShareTapped(_ sender : UIButton) {}

}
