//
//  BottomSheetViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 12/11/2021.
//

import UIKit
import FloatingPanel

protocol BottomSheetNavigation {
    func navigateToVisited()
    func navigateToSaved()
}

class BottomSheetViewController: UIViewController,FloatingPanelControllerDelegate, CustomSegmentedControlDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nothingView: UIView!
    
    var selectedIndex: Int = 0
    
    var delegate: BottomSheetNavigation?
    
    @IBOutlet weak var segmentControl: CustomSegmentedControl! {
        didSet {
            segmentControl.setButtonTitles(buttonTitles: ["\u{2022} SAVED","\u{2022} VISITED"])
            segmentControl.selectorViewColor = UIColor.underlineColor
            segmentControl.selectorTextColor = UIColor.selectedTextColor
            segmentControl.textColor = UIColor.btnTextColor
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "VisitedPlacesTableViewCell", bundle: nil), forCellReuseIdentifier: "VisitedPlacesTableViewCell")
        nothingView.isHidden = true
        segmentControl.delegate = self
        
    }
    
    
    func floatingPanel(_ fpc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout {
        IntrinsicPanelLayout()
    }


class IntrinsicPanelLayout: FloatingPanelLayout {
    
    let position: FloatingPanelPosition = .bottom
       let initialState: FloatingPanelState = .half
       var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
           return [
            
            .half: FloatingPanelLayoutAnchor(fractionalInset: 0.4, edge: .bottom, referenceGuide: .safeArea),
            
           ]
       
    }
}

    // MARK: - Action
  
    //MARK: Segment Delegate
    func change(to index:Int) {
        print("segmentedControl index changed to \(index)")
        selectedIndex = index
    }


}
public protocol FloatingPanelIntrinsicLayout: FloatingPanelLayout { }

extension BottomSheetViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisitedPlacesTableViewCell") as! VisitedPlacesTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndex == 0 {
            delegate?.navigateToSaved()
            
        } else {
            delegate?.navigateToVisited()
        }
    }
    
}
