//
//  CheckInPopUpViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 26/11/2021.
//

import UIKit
import SwiftGifOrigin

class CheckInPopUpViewController: UIViewController {

    @IBOutlet weak var firstView : UIView!
    @IBOutlet weak var secondView : UIView!
    @IBOutlet weak var lastView : UIView!

    @IBOutlet weak var coinView : UIView!
    @IBOutlet weak var imgCoin : UIView!
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblPosition : UILabel!
    @IBOutlet weak var lblEarnedPoints : UILabel!
    @IBOutlet weak var lblFirstCheckinPoints : UILabel!
    @IBOutlet weak var lblFirstFriendCheckInPoints : UILabel!
    @IBOutlet weak var imgMayorProfile : UIImageView!
    @IBOutlet weak var lblTopCheckInPlace : UILabel!
    
    @IBOutlet weak var stackImg: UIImageView!
    @IBOutlet weak var coinGif: UIImageView!
    var closeController : ((Bool)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.coinGif.loadGif(name: "coin-small")
        self.stackImg.isHidden = true

        DispatchQueue.main.asyncAfter(wallDeadline: .now() + 2) {
            self.coinGif.isHidden = true
            self.stackImg.isHidden = false
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        self.animateView()

        firstView.cornerRadius = 8
        secondView.cornerRadius = 8
        lastView.cornerRadius = 8
        
        firstView.shake()
        secondView.shake()
        
    }

    @IBAction func didCloseTap(_ sender: Any) {
        self.dismiss(animated: true) {
            self.closeController?(true)
        }
    }
    
}
