//
//  TipPopupViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 24/12/2021.
//

import Foundation
import UIKit

class TipPopupViewController : UIViewController {
    
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var notUpvotedButton: UIButton!
    @IBOutlet weak var upVotedButton: UIButton!
    @IBOutlet weak var viewinButton: UIButton!
    @IBOutlet weak var upvoteThisTipButton: UIButton!
    
    override func viewDidLoad() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dismiss(animated:completion:)))
        self.view.addGestureRecognizer(gesture)
        
        upvoteThisTipButton.layer.cornerRadius = upvoteThisTipButton.frame.size.height / 2
        upvoteThisTipButton.clipsToBounds = true
        
        notUpvotedButton.isHidden = false
        upVotedButton.isHidden = true
        upvoteThisTipButton.isEnabled = true
        
        notUpvotedButton.setTitle("", for: .normal)
        upVotedButton.setTitle("", for: .normal)
    }
    
    override func viewDidLayoutSubviews() {
        popupView.cornerRadius = 8
    }

    
    @IBAction func onNotUpvotedTapped(_ sender: Any) {
      
    }
    
    
    @IBAction func onUpvoteTipTapped(_ sender: Any) {
        notUpvotedButton.isHidden = true
        upVotedButton.isHidden = false
        upvoteThisTipButton.isEnabled = false
        upvoteThisTipButton.setTitle("Tip upvoted!", for: .disabled)
        //self.dismiss(animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.4) {
            self.dismiss(animated: true) {
                let storyboard = UIStoryboard.init(name: "Components", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "ResultRankViewController") as! ResultRankViewController
                controller.modalPresentationStyle = .overCurrentContext
                controller.modalPresentationStyle  = .overFullScreen
                controller.modalTransitionStyle = .crossDissolve
                if let vc = UIApplication.getTopViewController() {
                    vc.present(controller, animated: true, completion: nil)
                }
            }
        }
    }
    
    
}
