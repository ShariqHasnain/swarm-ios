//
//  AccessPermissionPopupViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 24/12/2021.
//

import Foundation
import UIKit

class AccessPermissionPopupViewController : UIViewController {
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var outsideImageView: UIImageView!
    @IBOutlet weak var changeSettingButton: UIButton!
    
    
    override func viewDidLoad() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dismiss(animated:completion:)))
        self.view.addGestureRecognizer(gesture)
        
        changeSettingButton.layer.cornerRadius = changeSettingButton.frame.size.height / 2
        changeSettingButton.clipsToBounds = true
    }
    
    override func viewDidLayoutSubviews() {
        containerView.cornerRadius = 8
    }
    
    
    @IBAction func onSettingTapped(_ sender: Any) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }

//        if UIApplication.shared.canOpenURL(settingsUrl) {
//            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                print("Settings opened: \(success)") // Prints true
//            })
//        }
    
        //self.dismiss(animated: true, completion: nil)
        
        self.dismiss(animated: true) {
            let storyboard = UIStoryboard.init(name: "Components", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "TipPopupViewController") as! TipPopupViewController
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalTransitionStyle = .crossDissolve
            controller.modalPresentationStyle  = .overFullScreen
            if let vc = UIApplication.getTopViewController() {
                vc.present(controller, animated: true, completion: nil)
            }
        }
    }
}
