//
//  MayorshipsViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit

class MayorshipsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
   

    @IBOutlet weak var tableview: UITableView!
    var dataTable : [[String : Any]] = [["cell" : "MayorshipMsgTableViewCell"],["cell" : "BecomeRegularTableViewCell"]]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.register(types: MayorshipMsgTableViewCell.self , BecomeRegularTableViewCell.self)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        setupNavigationBar()
        // Do any additional setup after loading the view.
    }
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [], NavigationTitle: "MayorShips" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataTable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let datatable = dataTable[indexPath.row]
        let data = datatable["cell"] as? String ?? ""
        switch data {
        case "MayorshipMsgTableViewCell" :
            guard let cellCollection = tableView.getCell(type: MayorshipMsgTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    
                return cellCollection
        case "BecomeRegularTableViewCell" :
            guard let cellCollection = tableView.getCell(type: BecomeRegularTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    
                return cellCollection
        default :
            return UITableViewCell.init(frame: .zero)
        }
  
    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
