//
//  GalleryViewController.swift
//  Swarn
//
//  Created by Q.M.S on 15/12/2021.
//

import Foundation
import UIKit

class GalleryViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "GalleryPlaceholderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GalleryPlaceholderCollectionViewCell")
        collectionView.register(UINib(nibName: "GalleryIconCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GalleryIconCollectionViewCell")
    }
    
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension GalleryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryPlaceholderCollectionViewCell", for: indexPath) as! GalleryPlaceholderCollectionViewCell
            cell.configure(with: "cameraGallery", title: "Camera")
            return cell
        } else if indexPath.row == 8 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryPlaceholderCollectionViewCell", for: indexPath) as! GalleryPlaceholderCollectionViewCell
            cell.configure(with: "libraryGallery", title: "Photo Library")
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryIconCollectionViewCell", for: indexPath) as! GalleryIconCollectionViewCell
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/3, height: UIScreen.main.bounds.height/6)
    }
    
}
