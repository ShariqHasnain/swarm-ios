//
//  OTPViewController.swift
//  Swarm
//
//  Created by Syed Zeeshan Rizvi on 2/25/22.
//

import UIKit
import ProgressHUD

class OTPViewController: UIViewController {

    
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var lblMessage : UILabel!
    @IBOutlet weak var btnProfile : UIButton!
    
    var parameters: [String: Any]!
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    @IBAction func btnSignUpTappedHandler(_ sender: UIButton) {
        
        guard let otp = txtEmail.text, !otp.isReallyEmpty else {
            self.showAlert(title: "Message", message: "OTP is required")
            return
        }
        parameters["otp"] = otp
        signUp()
    }
    
    func movetoHome() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScreenBoardViewController") as! ScreenBoardViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func signUp() {
        ProgressHUD.show()
        Networking.shared.signUp(image: image, endPoint: .register, parameters: parameters) { response in
            ProgressHUD.dismiss()
            switch response {
            case .success(let value):
                if value.status {
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(value.data!.token, forKey: "token")
                    userDefaults.synchronize()
                    self.movetoHome()
                } else {
                    self.showAlert(title: "Message", message: value.message)
                }
            case .failure(let error):
                self.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
    
}
