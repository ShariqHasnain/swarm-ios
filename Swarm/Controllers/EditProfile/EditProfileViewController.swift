//
//  EditProfileViewController.swift
//  Swarn
//
//  Created by Waseem Ahmed on 13/11/2021.
//

import UIKit
import ProgressHUD
import ActionSheetPicker_3_0

class EditProfileViewController: BaseViewController {
    
    @IBOutlet weak var txtFirstName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtPhone : UITextField!
    @IBOutlet weak var txtGender : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtBirthday : UITextField!
    @IBOutlet weak var textFeildPassword: UITextField!
    @IBOutlet weak var btnProfile : UIButton!
    
    @IBOutlet weak var uploadImageButton: UIButton!
    
    var isCommingFromLogin : Bool = false
    
    var signUpButton: UIButton = UIButton()
    
    let pickerController = UIImagePickerController()
    
    var selectedImage: UIImage?
    
    var selectedDate: Double?
    
    // MARK: - Life Cycle
    
    override func viewWillLayoutSubviews() {
        uploadImageButton.layer.cornerRadius = uploadImageButton.frame.width/2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPhone.keyboardType = .numberPad
        if isCommingFromLogin {
            setupNavigationBarLogin()
        }else {
            self.setupNavigationBar()
        }
        uploadImageButton.setImage(selectedImage ?? UIImage(named: "uploadCamera"), for: .normal)
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.mediaTypes = ["public.image"]
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            pickerController.sourceType = .camera
        } else {
            pickerController.sourceType = .savedPhotosAlbum
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupNavigationBarLogin() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        view.backgroundColor = .clear
        signUpButton = UIButton(frame: view.bounds)
        signUpButton.setTitle("Sign Up", for: .normal)
        signUpButton.setTitleColor(.orange, for: .normal)
        signUpButton.backgroundColor = .white
        signUpButton.addTarget(self, action: #selector(tapOnSignupBtn), for: .touchUpInside)
        signUpButton.layer.cornerRadius = 16
        isSignupButtonEnabled(isEnabled: true)
        view.addSubview(signUpButton)
        
        let menuBarItem1 = UIBarButtonItem()
        menuBarItem1.customView = view
        
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [menuBarItem1], NavigationTitle: "" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        let menuBtn1 = UIButton(type: .custom)
        menuBtn1.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn1.imageView?.contentMode = .scaleAspectFit
        menuBtn1.setImage(UIImage(named:"SAVE"), for: .normal)
        menuBtn1.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem1 = UIBarButtonItem(customView: menuBtn1)
        let currWidth1 = menuBarItem1.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth1?.isActive = true
        let currHeight1 = menuBarItem1.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight1?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [menuBarItem1], NavigationTitle: "Swarm" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    func isSignupButtonEnabled(isEnabled: Bool) {
        if isEnabled {
            signUpButton.backgroundColor = .white
        } else {
            signUpButton.backgroundColor = UIColor(red: 255/255, green: 205/225, blue: 134/255, alpha: 1)
        }
        signUpButton.isEnabled = isEnabled
    }
    
    // MARK: - Actions
    @IBAction func btnProfileTapped(_ sender : UIButton) {
        let vc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "GalleryViewController")
        present(vc, animated: true, completion: nil)
        isSignupButtonEnabled(isEnabled: true)
    }
    
    @objc func tapOnSignupBtn() {
        guard let firstName = txtFirstName.text, !firstName.isReallyEmpty else {
            self.showAlert(title: "Message", message: "First Name is required.")
            return
        }
        guard let lastName = txtLastName.text, !lastName.isReallyEmpty else {
            self.showAlert(title: "Message", message: "Last Name is required.")
            return
        }
        guard let email = txtEmail.text, !email.isReallyEmpty else {
            self.showAlert(title: "Message", message: "Email is required.")
            return
        }
        guard email.isEmail else {
            self.showAlert(title: "Message", message: "Email Format is incorrect.")
            return
        }
        guard let password = textFeildPassword.text, !password.isReallyEmpty else {
            self.showAlert(title: "Message", message: "Gender is required.")
            return
        }
        guard password.isValidPassword else {
            self.showAlert(title: "Message", message: "Password must have at least one uppercase, one lower case, one digit and one symbol.")
            return
        }
        guard let gender = txtGender.text, !gender.isReallyEmpty else {
            self.showAlert(title: "Message", message: "Gender is required.")
            return
        }
        var parameters = ["first_name": firstName, "last_name": lastName, "email": email, "password": password, "gender": gender.lowercased()]
        
        if let phone = txtPhone.text, !phone.isReallyEmpty {
            parameters["phone_number"] = phone
        }
        
        if let birth = txtBirthday.text, !birth.isReallyEmpty {
            parameters["birth_date"] = "\(selectedDate!)"
        }
        
        ProgressHUD.show()
        Networking.shared.signUp(image: nil, endPoint: .requestEmail, parameters: parameters) { response in
            ProgressHUD.dismiss()
            switch response {
            case .success(let value):
                if value.status {
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                    vc.parameters = parameters
                    vc.image = self.selectedImage
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.showAlert(title: "Message", message: value.message)
                }
            case .failure(let error):
                self.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
        
    }
    
    func signUp(parameters: [String: Any]) {
        ProgressHUD.show()
        Networking.shared.signUp(image: nil, endPoint: .requestEmail, parameters: parameters) { response in
            ProgressHUD.dismiss()
            switch response {
            case .success(let value):
                if value.status {
                    
                } else {
                    self.showAlert(title: "Message", message: value.message)
                }
            case .failure(let error):
                self.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func uploadImageTapped(_ sender: UIButton) {
        self.present(pickerController, animated: true, completion: nil)
    }
    
    @IBAction func btnBirthdayTappedHandler(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Select Date",
                                               datePickerMode: UIDatePicker.Mode.date,
                                               selectedDate: Date(),
                                               doneBlock: { picker, dateString, origin in
                                                    print("picker = \(String(describing: picker))")
                                                    print("date = \(String(describing: dateString))")
                                                    print("origin = \(String(describing: origin))")
            let date = dateString as! Date
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year, .month, .day, .hour], from: date)
            self.txtBirthday.text = "\(components.day!)-\(components.month!)-\(components.year!)"
            self.selectedDate = date.timeIntervalSince1970
            
            return
            }, cancel: { picker in
            return
            }, origin: (sender as! UIButton).superview!.superview)
//        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
//        datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
//        datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
        if #available(iOS 14.0, *) {
            datePicker?.datePickerStyle = .inline
        }
        datePicker?.show()
    }
    
    @IBAction func btnGenderTappedHandler(_ sender: Any) {
        _ = ActionSheetStringPicker.show(withTitle: "Select Gender",
                                     rows: ["Male", "Female", "Others"],
                                     initialSelection: 1,
                                     doneBlock: { picker, index, value in
            print("picker = \(String(describing: picker))")
            print("index = \(index)")
            print("value = \(String(describing: value))")
           
           self.txtGender.text = value as? String
            return
        }, cancel: { picker in
            return
        }, origin: sender)
    }
    
}

extension EditProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
        selectedImage = image
        uploadImageButton.setImage(selectedImage!, for: .normal)
        pickerController.dismiss(animated: true, completion: nil)
    }
}
