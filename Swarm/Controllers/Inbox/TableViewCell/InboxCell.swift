//
//  InboxCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 20/11/2021.
//

import UIKit
protocol InboxDelgate {
    func didTapOnAccept()
    func didTapOnDeclined()
}

class InboxCell: UITableViewCell, Registerable {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btnAccept : UIButton!
    @IBOutlet weak var btnDecline : UIButton!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var imgProfile : UIImageView!
    var inboxDelegate : InboxDelgate?
    @IBOutlet weak var tapBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        tapBtn.isHidden = true
        // Initialization code
    }

    @IBAction func btnAcceptTapped(_ sender : UIButton) {
        
        stackView.isHidden = true
        tapBtn.isHidden = false
    }
    
    @IBAction func btnDeclineTapped(_ sender : UIButton) {
        
    }
    
    @IBAction func didTapOnCell(_ sender: Any) {
        print("TapONCell")
    }
    
    
    @IBAction func didTapOnAccept(_ sender: Any) {
        self.inboxDelegate?.didTapOnAccept()
    }
    
    @IBAction func didTapOnDeclined(_ sender: Any) {
        self.inboxDelegate?.didTapOnDeclined()
    }
    
}
