//
//  FriendAddTableViewCell.swift
//  Swarn
//
//  Created by Waseem Ahmed on 28/11/2021.
//

import UIKit
protocol FriendAddDelegate {
    func didTapOnCell()
}
class FriendAddTableViewCell: UITableViewCell , Registerable {
    @IBOutlet weak var userName: UILabel!
    var friendAddDelegate : FriendAddDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func didTaponCell(_ sender: Any) {
        self.friendAddDelegate?.didTapOnCell()
    }
    
}
