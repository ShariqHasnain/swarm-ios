//
//  InboxViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit

class InboxViewController: BaseViewController,UITableViewDelegate , UITableViewDataSource, FriendAddDelegate, InboxDelgate {
   
 
    

    @IBOutlet weak var tableview: UITableView!
    var isNotify : Bool = false
    var isAccepted : Bool?
    var dataDic : [[String : Any]] = [["cell" : "InboxCell"] , ["cell" : "FriendAddTableViewCell"]]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.register(types: NoNotificationMsgTableViewCell.self , InboxCell.self , FriendAddTableViewCell.self)
        setupNavigationBar()
        isNotify = true

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBtn2 = UIButton(type: .custom)
        menuBtn2.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn2.imageView?.contentMode = .scaleAspectFit
        menuBtn2.setImage(UIImage(named:"MsgIcon"), for: .normal)
        menuBtn2.addTarget(self, action: #selector(tapOnMsgBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        let menuBarItem2 = UIBarButtonItem(customView: menuBtn2)
        let currWidth2 = menuBarItem2.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth2?.isActive = true
        let currHeight2 = menuBarItem2.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight2?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [menuBarItem2], NavigationTitle: "Inbox" , isSearchBar: false , isLable: true , isBackColor: false)
       
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            // Sets shadow (line below the bar) to a blank image
        self.navigationController?.navigationBar.shadowImage = UIImage()
     
            // Sets the translucent background color
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isNotify {
            return dataDic.count
        }else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if isNotify {
            
            let data = dataDic[indexPath.row]
            let datacell = data["cell"] as? String ?? ""
            switch datacell {
            case "InboxCell" :
                guard let cellCollection = tableView.getCell(type: InboxCell.self,for: indexPath)  else { return UITableViewCell() }
                        
                cellCollection.inboxDelegate = self
                    return cellCollection
            case "FriendAddTableViewCell" :
                guard let cellCollection = tableView.getCell(type: FriendAddTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                        
                cellCollection.friendAddDelegate = self
                
                if isAccepted ?? false {
                    if indexPath.row == 0 {
                    cellCollection.userName.text = "Sajjid Saleem"
                    }
                }
                    return cellCollection
            default :
                return UITableViewCell.init(frame: .zero)
            }
            
        }else {
            guard let cellCollection = tableView.getCell(type: NoNotificationMsgTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    
        
                return cellCollection
        }
      
        
      
    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func tapOnMsgBtn() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "FindFriendsViewController") as! FindFriendsViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @objc func didTapOnCell() {
        let storyboardprofile = UIStoryboard(name: "Chat", bundle: nil)
                let controller = storyboardprofile.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//        controller.isCommingfromMsg = true
//                self.tabBarController?.tabBar.isTranslucent = true
//                self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(controller, animated: true)
    }

    func didTapOnAccept() {
        dataDic.removeFirst()
        dataDic.append(["cell" : "FriendAddTableViewCell"])
        isAccepted = true
        tableview.reloadData()
    }
    
    func didTapOnDeclined() {
        dataDic.removeFirst()
        tableview.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
