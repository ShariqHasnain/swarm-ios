//
//  MiddleCheckInTableViewCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali onon 11/11/2021.
//

import UIKit

class MiddleCheckInTableViewCell: UITableViewCell,Registerable {

    @IBOutlet weak var dayAndDateLabel: UILabel!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateData(checkIn: DateCheckIns) {
        dayAndDateLabel.text = checkIn.created_at
        placeName.text = checkIn.place?.name
        countryNameLabel.text = "England"
        pointsLabel.text = "0"
        dateAndTimeLabel.text = checkIn.created_at
        descriptionLabel.text = checkIn.description
    }
    

}
