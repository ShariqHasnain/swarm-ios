//
//  CheckInHeaderView.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 20/11/2021.
//

import UIKit

class CheckInHeaderView : UIView {
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var stickyBottomView: UIView!
    @IBOutlet weak var stickyTopView: UIView!
    @IBOutlet weak var grayPointCircle: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        grayPointCircle.layer.cornerRadius = grayPointCircle.frame.width/2
    }
}
