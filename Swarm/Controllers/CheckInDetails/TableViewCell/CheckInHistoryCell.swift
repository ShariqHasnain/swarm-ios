//
//  CheckInHistoryCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 20/11/2021.
//

import UIKit

protocol CheckinHistoryDelegate {
    func crossTapped(sender: UIButton)
}

class CheckInHistoryCell: UITableViewCell, Registerable {

    @IBOutlet weak var lblPlaceName : UILabel!
    @IBOutlet weak var lblCountry : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var tickButton: UIButton!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var imgProfile : UIImageView!
    
    var delegate: CheckinHistoryDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func initState() {
        
        tickButton.layer.cornerRadius = tickButton.frame.width/2
        tickButton.clipsToBounds = true
        
        crossButton.layer.cornerRadius = crossButton.frame.width/2
        crossButton.clipsToBounds = true
        
//        let image1 = UIImage(named: "btnTick")?.withRenderingMode(.alwaysTemplate)
//        tickButton.setImage(image1, for: .normal)
//        tickButton.tintColor = UIColor.white
//
//
//        let image2 = UIImage(named: "btnCross")?.withRenderingMode(.alwaysTemplate)
//        crossButton.setImage(image2, for: .normal)
//        crossButton.tintColor =  UIColor.white

    }

    @IBAction func btnCrossTapped(_ sender : UIButton) {
        delegate?.crossTapped(sender: sender)
    }
    @IBAction func btnTickTapped(_ sender : UIButton) {}
}
