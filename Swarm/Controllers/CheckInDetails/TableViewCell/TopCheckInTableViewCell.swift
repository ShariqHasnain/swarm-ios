//
//  TopCheckInTableViewCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali onon 11/11/2021.
//

import UIKit
import SkeletonView
protocol TopCheckInDelegate {
    func didTapOnCircle(sender : UIButton)
    func didTapOnCell(sender : UIButton)
    func didTapOnFirstImage()
    func didTapOnSecondImage()
    func didTapOnThirdImage()
    func didTapOnfourthImage()
} 
class TopCheckInTableViewCell: UITableViewCell , Registerable {
    @IBOutlet weak var numbersOfCoin: UILabel!
    @IBOutlet weak var coinImg: UIImageView!
    @IBOutlet weak var commentUser: UILabel!
    @IBOutlet weak var dateWithTime: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var stickView: UIView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var imageViewMainStack: UIStackView!
    @IBOutlet weak var firstStackImage: UIStackView!
    @IBOutlet weak var secondStackImage: UIStackView!
    
    @IBOutlet weak var firstImage: UIImageView!
   
    @IBOutlet weak var secondImage: UIImageView!
    
    @IBOutlet weak var thirdImage: UIImageView!
    
    @IBOutlet weak var fourthImage: UIImageView!
    
    
    var topCheckInDelegate : TopCheckInDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.circleView.layer.cornerRadius = self.circleView.frame.height/2
        self.circleView.clipsToBounds = true
        let firstImageTapGeasture = UITapGestureRecognizer.init(target: self, action: #selector(didTapFirstImage))
        firstImage.isUserInteractionEnabled = true
        firstImage.addGestureRecognizer(firstImageTapGeasture)
        
        let secondImageTapGeasture = UITapGestureRecognizer.init(target: self, action: #selector(didTapSecondImage))
        secondImage.isUserInteractionEnabled = true
        secondImage.addGestureRecognizer(secondImageTapGeasture)
        
//        let thirdImageTapGeasture = UITapGestureRecognizer.init(target: self, action: #selector(didTapThirdImage))
//        thirdImage.isUserInteractionEnabled = true
//        thirdImage.addGestureRecognizer(thirdImageTapGeasture)
//
//        let forthImageTapGeasture = UITapGestureRecognizer.init(target: self, action: #selector(didTapFourthImage))
//        fourthImage.isUserInteractionEnabled = true
//        fourthImage.addGestureRecognizer(forthImageTapGeasture)
        
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didTapOnCircle(_ sender: UIButton) {
        self.topCheckInDelegate?.didTapOnCircle(sender: sender)
    }
    
    @IBAction func didTapOnCell(_ sender: UIButton) {
        self.topCheckInDelegate?.didTapOnCell(sender: sender)
    }
    
    @objc func didTapFirstImage() {
        self.topCheckInDelegate?.didTapOnFirstImage()
    }
    
   @objc func  didTapSecondImage() {
       self.topCheckInDelegate?.didTapOnSecondImage()
    }
    
//    @objc func didTapThirdImage() {
//        self.topCheckInDelegate?.didTapOnThirdImage()
//    }
//
//    @objc func didTapFourthImage() {
//        self.topCheckInDelegate?.didTapOnfourthImage()
//    }
    

}
