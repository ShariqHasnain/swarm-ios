//
//  CheckInAPICalls.swift
//  Swarm
//
//  Created by Mohammad Arsalan on 19/03/2022.
//

import Foundation
import Alamofire

extension Networking {
    
    func checkIn(image: UIImage, endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<CheckInResponse, Error>) -> ()) {
        requestMultipart(image: image, endPoint: endPoint, parameters: parameters, completion: completion)
      //  requestPost(endPoint: endPoint, parameters: parameters, completion: completion)
    }
    
    func editCheckIn(endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<CheckInResponse, Error>) -> ()) {
        requestPost(endPoint: endPoint, parameters: parameters, completion: completion)
    }
    
    func rateCheckIn(endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<CheckInResponse, Error>) -> ()) {
        requestPost(endPoint: endPoint, parameters: parameters, completion: completion)
    }
    
    func addMediaCheckIn(image: UIImage?, endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<CheckInResponse, Error>) -> ()) {
        requestMultipart(image: image, endPoint: endPoint, parameters: parameters, completion: completion)
    }
    
    func commentCheckIn(endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<CheckInResponse, Error>) -> ()) {
        requestPost(endPoint: endPoint, parameters: parameters, completion: completion)
    }
    
    func removeCheckIn(endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<GeneralResponse, Error>) -> ()) {
        requestPost(endPoint: endPoint, parameters: parameters, completion: completion)
    }
    
    func checkInDetails(endPoint: EndPoint, completion: @escaping (Result<Intro, Error>) -> ()) {
        requestGet(endPoint: endPoint, completion: completion)
    }
    
    func checkInLists(endPoint: EndPoint, queryItems: [URLQueryItem], completion: @escaping (Result<CheckInList, Error>) -> ()) {
        requestGet(endPoint: endPoint, queryItems: queryItems, completion: completion)
    }
}
