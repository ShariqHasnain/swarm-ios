//
//  CheckInDetailViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on on 11/11/2021.
//

import UIKit
import SkeletonView
import CoreLocation
import GoogleMaps
import ProgressHUD
class CheckInDetailViewController: BaseViewController,UITableViewDelegate , UITableViewDataSource, TopCheckInDelegate, CLLocationManagerDelegate {

    var stickyHeaderHeight = 205
    
     weak var mapView: GMSMapView!
    var leftnavigationBar : [UIBarButtonItem]?
      var rightnavigationBar : [UIBarButtonItem]?
    var locationManager = CLLocationManager()
    @IBOutlet weak var tableview: UITableView!
//    var dataTable :  [[String : Any]] = [
//        ["cell":"TotalCheckIns"],
//        ["cell":"TotalCheckInHeader"],
//        ["cell":"TopCheckInTableViewCell"],
//        ["cell":"MiddleCheckInTableViewCell"],
//        ["cell":"LastCheckInTableViewCell"],
//        ["cell" : "CheckInHistoryCell"],
//        ["cell" : "CheckInHistoryCell"],
//        ["cell" : "CheckInHistoryCell"]
//    ]
    var dataTable :  [[String : Any]] = [
        ["cell":"TotalCheckIns"],
        ["cell":"TotalCheckInHeader"],
        ["cell":"MiddleCheckInTableViewCell"],
        ["cell":"MiddleCheckInTableViewCell"],
        ["cell":"MiddleCheckInTableViewCell"],
        ["cell":"MiddleCheckInTableViewCell"],
//        ["cell" : "CheckInHistoryCell"],
//        ["cell" : "CheckInHistoryCell"],
//        ["cell" : "CheckInHistoryCell"]
    ]
    
    var checkINs: CheckIns?
    
    //let headerView = Bundle.main.loadNibNamed("HeaderMapView", owner: self, options: nil)?.first as! HeaderMapView
    var headerView : HeaderMapView = {
       return Bundle.main.loadNibNamed("HeaderMapView", owner: self, options: nil)?.first as! HeaderMapView
    }()
    
    var previousScrollOffset: CGFloat = 0
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        view.startSkeletonAnimation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = false
        if isOpenCoin {
            openCoin()
        
            
        }
        
        setupNavigationBar()
        fetchCheckInListData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        headerView.delegate = self
        tableview.register(types: TopCheckInTableViewCell.self,MiddleCheckInTableViewCell.self,LastCheckInTableViewCell.self ,CheckInHistoryCell.self)
        
//             if #available(iOS 15.0, *) {
//                 UITableView.appearance().sectionHeaderTopPadding = CGFloat(0)
//             }
        self.tableview.contentInsetAdjustmentBehavior = .never
        self.tableview.contentInset = UIEdgeInsets(top: -45, left: 0, bottom: 0, right: 0)
        setupNavigationBar()
        
      //  editCheckIn()

    }
    
    
    func commentCheckIn() {
        ProgressHUD.show()
        
        let params: [String: Any] = ["checkin_refrence": 33,
                                     "sticker": 1,
                                     "comment": "this is comment"]

        Networking.shared.commentCheckIn(endPoint: .commentCheckIn, parameters: params) { result in
            ProgressHUD.dismiss()
            switch result {
            case.success(let value):
                print(value)
                dump(value)
            case .failure(let error):
                self.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
    
    
    func addMediaCheckIn() {
        ProgressHUD.show()
        
        let params: [String: Any] = ["checkin_refrence": 33,
                                     "media[]": "Image"]

        Networking.shared.addMediaCheckIn(image: UIImage(), endPoint: .addMediaCheckIn, parameters: params) { result in
            ProgressHUD.dismiss()
            switch result {
            case.success(let value):
                print(value)
                dump(value)
            case .failure(let error):
                self.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
    
    func rateCheckIn() {
        ProgressHUD.show()
        
        let params: [String: Any] = ["checkin_refrence": 33,
                                     "rate": 2]

        Networking.shared.rateCheckIn(endPoint: .rateCheckIn, parameters: params) { result in
            ProgressHUD.dismiss()
            switch result {
            case.success(let value):
                print(value)
                dump(value)
            case .failure(let error):
                self.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
    func editCheckIn() {
        ProgressHUD.show()
        
        let params: [String: Any] = ["description": "this is my 1st checkin",
                                     "checkin_refrence": 33,
                                     "off_the_grid": 0]

        Networking.shared.editCheckIn(endPoint: .editCheckIn, parameters: params) { result in
            ProgressHUD.dismiss()
            switch result {
            case.success(let value):
                print(value)
                dump(value)
            case .failure(let error):
                self.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
//    func checkIn() {
//        ProgressHUD.show()
//        
//        let params: [String: Any] = ["description": "this is my 1st checkin",
//                                     "sticker": 1,
//                                     "lat": 24.934982442468545,
//                                     "lng": 67.04006558466043,
//                                     "place_object": "",
//                                     "off_the_grid": 0,
//                                     "share_on_fb": 1,
//                                     "share_on_twitter": 0,
//                                     "media[]": ""]
//
//        Networking.shared.checkIn(endPoint: .checkIn, parameters: params) { result in
//            ProgressHUD.dismiss()
//            switch result {
//            case.success(let value):
//                print(value)
//                dump(value)
//            case .failure(let error):
//                self.showAlert(title: "Message", message: error.localizedDescription)
//            }
//        }
//    }
    
    func fetchCheckInListData() {
        ProgressHUD.show()
        
        
        
        let queryItems = [ URLQueryItem(name: "per_page", value: "\(20)"),
                           URLQueryItem(name: "page", value: "\(1)")]

        getReq(endPoint: .checkInLists, queryItems: queryItems) { data, response in
            ProgressHUD.dismiss()
            if let data = data {
                let decoder = JSONDecoder()
                
                do {
                    let result = try decoder.decode(CheckInList.self, from: data)
                    
                    self.checkINs = result.data?.checkIns
                    DispatchQueue.main.async {
                        self.tableview.reloadData()
                    }
                    
                    print(result)

                } catch {
                    self.showAlert(title: "Message", message: error.localizedDescription)
                    print(error.localizedDescription)
                    
                }
            }
        } failure: { error, data in
            print(error.localizedDescription)
            self.showAlert(title: "Message", message: error.localizedDescription)
        }
        
    }
  
    
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"checkImage"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnProfileBtn), for: .touchUpInside)
        //menuBtn.addTarget(self, action: #selector(moveereTo), for: .touchUpInside)
        let menuBtn2 = UIButton(type: .custom)
        menuBtn2.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn2.imageView?.contentMode = .scaleAspectFit
        menuBtn2.setImage(UIImage(named:"Icon awesome-inbox"), for: .normal)
        menuBtn2.addTarget(self, action: #selector(tapOnInboxBoxBtn), for: .touchUpInside)
        //menuBtn2.addTarget(self, action: #selector(moveereTo2), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        let menuBarItem2 = UIBarButtonItem(customView: menuBtn2)
        let currWidth2 = menuBarItem2.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth2?.isActive = true
        let currHeight2 = menuBarItem2.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight2?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [menuBarItem2], NavigationTitle: "", hasLeft: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return checkINs?._data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if section == 0 || section == 1{
//            return 0
//        }
        
        if section == 0 {
            let checkins = (checkINs?._data[section].checkins?.count ?? 0)
            print("\(section) checkINs \(checkins + 2)")
            return checkins + 1
        } else {
            let checkins = (checkINs?._data[section].checkins?.count ?? 0)
            print("\(section) checkINs \(checkins + 1)")
            return checkins + 1
            
        }
//        print((checkINs?.total  ?? 0) + (checkINs?._data.count ?? 0) + 1)
//        return (checkINs?.total  ?? 0) + (checkINs?._data.count ?? 0) + 1
       // return dataTable.count
        //return dataTable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath)
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = UITableViewCell.init(style: .default, reuseIdentifier: "TotalCheckIns")
                let totalCheckinHeaderView =   Bundle.main.loadNibNamed("TotalCheckInHeaderView", owner: self, options: nil)?.first as! TotalCheckInHeaderView
                // TODO: GET TOTAL NUMBER OF CHECKINS FROM API AND SHOW IT HERE
                totalCheckinHeaderView.totalNumberOfCheckIns.text = "\(checkINs?.total ?? 0)"
                cell.addSubview(totalCheckinHeaderView)
                
                return cell
                
            } else if indexPath.row == 1 {
                let cell = UITableViewCell.init(style: .default, reuseIdentifier: "TotalCheckInHeader")
                let datecCheckinHeaderView = Bundle.main.loadNibNamed("CheckInHeaderView", owner: self, options: nil)?.first as! CheckInHeaderView
                datecCheckinHeaderView.headerTitleLabel.text = checkINs?._data[indexPath.section].date ?? ""
                datecCheckinHeaderView.stickyTopView.isHidden = indexPath.section == 2 ? true : false
                cell.addSubview(datecCheckinHeaderView)
                return cell
                
            } else {
                guard let cellCollection = tableView.getCell(type: MiddleCheckInTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                guard let checkINs = checkINs, let checkIn = checkINs._data[indexPath.section].checkins else { return UITableViewCell() }
                cellCollection.updateData(checkIn: checkIn[indexPath.row - 2])
                    return cellCollection
            }
        } else {
            if indexPath.row == 0 {
               let cell = UITableViewCell.init(style: .default, reuseIdentifier: "TotalCheckInHeader")
               let datecCheckinHeaderView = Bundle.main.loadNibNamed("CheckInHeaderView", owner: self, options: nil)?.first as! CheckInHeaderView
               datecCheckinHeaderView.headerTitleLabel.text = checkINs?._data[indexPath.section].date ?? ""
               datecCheckinHeaderView.stickyTopView.isHidden = indexPath.section == 2 ? true : false
               cell.addSubview(datecCheckinHeaderView)
               return cell
               
           } else {
               guard let cellCollection = tableView.getCell(type: MiddleCheckInTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
               guard let checkINs = checkINs, let checkIn = checkINs._data[indexPath.section].checkins else { return UITableViewCell() }
               cellCollection.updateData(checkIn: checkIn[indexPath.row - 1])
                   return cellCollection
           }
        }
       
//        let datatable = dataTable[indexPath.row]
//        let data = datatable["cell"] as? String ?? ""
//        switch data {
//        case "TotalCheckIns":
//            let cell = UITableViewCell.init(style: .default, reuseIdentifier: data)
//            let totalCheckinHeaderView =   Bundle.main.loadNibNamed("TotalCheckInHeaderView", owner: self, options: nil)?.first as! TotalCheckInHeaderView
//            // TODO: GET TOTAL NUMBER OF CHECKINS FROM API AND SHOW IT HERE
//            totalCheckinHeaderView.totalNumberOfCheckIns.text = "\(checkINs?.total ?? 0)"
//            cell.addSubview(totalCheckinHeaderView)
//
//            return cell
//        case "TotalCheckInHeader":
//            let cell = UITableViewCell.init(style: .default, reuseIdentifier: data)
//            let datecCheckinHeaderView = Bundle.main.loadNibNamed("CheckInHeaderView", owner: self, options: nil)?.first as! CheckInHeaderView
//            datecCheckinHeaderView.headerTitleLabel.text = checkINs?._data[indexPath.section].date ?? ""
//            datecCheckinHeaderView.stickyTopView.isHidden = indexPath.section == 2 ? true : false
//            cell.addSubview(datecCheckinHeaderView)
//            return cell
//
//        case "TopCheckInTableViewCell" :
//            guard let cellCollection = tableView.getCell(type: TopCheckInTableViewCell.self,for: indexPath)  else { return UITableViewCell()
//
//            }
//
//            cellCollection.topCheckInDelegate = self
////            cellCollection.numbersOfCoin.showAnimatedSkeleton()
////            cellCollection.coinImg.showAnimatedSkeleton()
////            cellCollection.commentUser.showAnimatedSkeleton()
////            cellCollection.dateWithTime.showAnimatedSkeleton()
////            cellCollection.date.showAnimatedSkeleton()
////            cellCollection.stickView.showAnimatedSkeleton()
////            cellCollection.countryName.showAnimatedSkeleton()
////            cellCollection.imgView.showAnimatedSkeleton()
////            cellCollection.placeName.showAnimatedSkeleton()
////            cellCollection.circleView.showAnimatedSkeleton()
////
////            cellCollection.numbersOfCoin.startSkeletonAnimation()
////            cellCollection.coinImg.startSkeletonAnimation()
////            cellCollection.commentUser.startSkeletonAnimation()
////            cellCollection.dateWithTime.startSkeletonAnimation()
////            cellCollection.date.startSkeletonAnimation()
////            cellCollection.stickView.startSkeletonAnimation()
////            cellCollection.countryName.startSkeletonAnimation()
////            cellCollection.imgView.startSkeletonAnimation()
////            cellCollection.placeName.startSkeletonAnimation()
////            cellCollection.circleView.startSkeletonAnimation()
////
////            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
////                cellCollection.numbersOfCoin.hideSkeleton(transition: .crossDissolve(1))
////                cellCollection.coinImg.hideSkeleton(transition: .crossDissolve(1))
////                cellCollection.commentUser.hideSkeleton(transition: .crossDissolve(1))
////                cellCollection.dateWithTime.hideSkeleton(transition: .crossDissolve(1))
////                cellCollection.date.hideSkeleton(transition: .crossDissolve(1))
////                cellCollection.stickView.hideSkeleton(transition: .crossDissolve(1))
////                cellCollection.countryName.hideSkeleton(transition: .crossDissolve(1))
////                cellCollection.imgView.hideSkeleton(transition: .crossDissolve(1))
////                cellCollection.placeName.hideSkeleton(transition: .crossDissolve(1))
////                cellCollection.circleView.hideSkeleton(transition: .crossDissolve(1))
////
//
////            }
//
//                return cellCollection
//        case "MiddleCheckInTableViewCell" :
//            guard let cellCollection = tableView.getCell(type: MiddleCheckInTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//            guard let checkINs = checkINs, let checkIn = checkINs._data[indexPath.section].checkins else { return UITableViewCell() }
//            cellCollection.updateData(checkIn: checkIn[indexPath.row - 2])
//                return cellCollection
//        case "LastCheckInTableViewCell" :
//            guard let cellCollection = tableView.getCell(type: LastCheckInTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
//
//                return cellCollection
//
//        case "CheckInHistoryCell" :
//            guard let cellCollection = tableView.getCell(type: CheckInHistoryCell.self,for: indexPath)  else { return UITableViewCell() }
//                cellCollection.initState()
//            cellCollection.delegate = self
//                return cellCollection
//
//        default :
//            return UITableViewCell(frame: .zero)
//        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let headerView = Bundle.main.loadNibNamed("HeaderMapView", owner: self, options: nil)?.first as! HeaderMapView
            headerView.delegate = self
            headerView.mapView.isMyLocationEnabled = true
            self.mapView = headerView.mapView
                //Location Manager code to fetch current location
                self.locationManager.delegate = self
                self.locationManager.startUpdatingLocation()
            return headerView
        } else {
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 282
        }
//        else if section == 1 {
//            return 60
//        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @objc func tapOnProfileBtn() {
        let storyboardprofile = UIStoryboard(name: "Profile", bundle: nil)
        let controller = storyboardprofile.instantiateViewController(withIdentifier: "ProfileDetailsViewController") as! ProfileDetailsViewController
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func tapOnInboxBoxBtn() {
        let storyboardprofile = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboardprofile.instantiateViewController(withIdentifier: "InboxViewController") as! InboxViewController
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


func didTapOnCircle(sender: UIButton) {
    print("didTapOnCircle")
    moveToTripDetails()
}

func didTapOnCell(sender: UIButton) {
    print("didTapOnCell")
}

func didTapOnFirstImage() {
    print("didTapOnFirstImage")
    moveToImageView()
    
}

func didTapOnSecondImage() {
    print("didTapOnSecondImage")
    moveToImageView()
}

func didTapOnThirdImage() {
    print("didTapOnThirdImage")
    moveToImageView()
}

func didTapOnfourthImage() {
    print("didTapOnfourthImage")
    moveToImageView()
}
    
//temp
    @objc func moveereTo(){
        let storyboard = UIStoryboard.init(name: "Trivia", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TriviaQuestionViewController") as! TriviaQuestionViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalPresentationStyle  = .overFullScreen
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func moveereTo2(){
        let storyboard = UIStoryboard.init(name: "Components", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AccessPermissionPopupViewController") as! AccessPermissionPopupViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalPresentationStyle  = .overFullScreen
        controller.modalTransitionStyle = .crossDissolve
        if let vc = UIApplication.getTopViewController() {
            vc.present(controller, animated: true, completion: nil)
        }
    }
    
    func moveToImageView() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ImagePreviewerViewController") as! ImagePreviewerViewController
        controller.modalPresentationStyle = .fullScreen
        controller.modalPresentationStyle  = .overFullScreen
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    func moveToTripDetails() {
        //let controller = self.storyboard?.instantiateViewController(withIdentifier: "PlaceDetailsViewController") as! PlaceDetailsViewController
        let controller = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "PlaceDetailsViewController") as! PlaceDetailsViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func openCoin() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CoinPopUpVIewControllerViewController") as! CoinPopUpVIewControllerViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalPresentationStyle  = .overFullScreen
        controller.modalTransitionStyle = .crossDissolve
        controller.closeController = { status in
            if status {
                self.earCoinOpen()
            }
        }
        self.present(controller, animated: true, completion: nil)
    }
    func earCoinOpen() {
        let storyboard = UIStoryboard.init(name: "Components", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CheckInPopUpViewController") as! CheckInPopUpViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle  = .overFullScreen
        controller.closeController = { status in
            if status {
                self.progressPopUP()
            }
        }
        self.present(controller, animated: true, completion: nil)
    }
    
    func progressPopUP() {
        
        let storyboard = UIStoryboard.init(name: "Components", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CollectibleProgressPopUpViewController") as! CollectibleProgressPopUpViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalPresentationStyle  = .overFullScreen
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let location = locations.last

        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)

        self.mapView?.animate(to: camera)

        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()

    }
    

}


extension CheckInDetailViewController:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         let sectionHeaderHeight: CGFloat = 305
         if scrollView.contentOffset.y <= sectionHeaderHeight &&
            scrollView.contentOffset.y >= 0 {
             scrollView.contentInset = UIEdgeInsets(top: -scrollView.contentOffset.y, left: 0, bottom: 0, right: 0)
         } else if scrollView.contentOffset.y >= sectionHeaderHeight {
             scrollView.contentInset = UIEdgeInsets(top: -sectionHeaderHeight, left: 0, bottom: 0, right: 0)
         }
     }
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print(scrollView.contentOffset.y)
//        if scrollView.contentOffset.y < 0 {
//            stickyHeaderHeight += Int(abs(scrollView.contentOffset.y))
//            headerView.incrementColorAlpha(offset: CGFloat(stickyHeaderHeight))
//        }else if scrollView.contentOffset.y > 0 && self.stickyHeaderHeight >= 65 {
//            self.stickyHeaderHeight -= Int(scrollView.contentOffset.y)/100
//            headerView.decrementColorAlpha(offset: scrollView.contentOffset.y)
//            if self.stickyHeaderHeight < 65 {
//                self.stickyHeaderHeight = 65
//            }
//        }
//    }
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        if self.stickyHeaderHeight > 200 {
//            animateHeader()
//        }
//    }
//
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if self.stickyHeaderHeight > 200 {
//            animateHeader()
//        }
//    }
}

extension CheckInDetailViewController {
    func animateHeader() {
        self.stickyHeaderHeight = 200
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: [.curveEaseOut], animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

extension CheckInDetailViewController: HeaderMapDelegate {
    
    func visitedTapped() {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func savedTapped() {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func categoriesTapped() {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "CategoriesParentViewController") as! CategoriesParentViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension CheckInDetailViewController: CheckinHistoryDelegate {
    func crossTapped(sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: tableview)
        guard let indexPath = tableview.indexPathForRow(at: point) else { return }
        dataTable.remove(at: indexPath.row)
        tableview.reloadData()
        //tableview.deleteRows(at: [IndexPath(row: indexPath.row, section: 3)], with: .automatic)
    }
}
