//
//  FindAFriendViewController.swift
//  Swarn
//
//  Created by Q.M.S on 20/01/2022.
//

import Foundation
import UIKit
import Contacts

protocol FindAFriendDelegate {
    func friendsSelected(friends: [String])
}

class FindAFriendViewController: UIViewController {
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var selectedFriendLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var friendsArray : [FetchedContact]  = []
    
    var isSearching = false
    
    var searchedArray = [FetchedContact]()
    
    var selectedFriends = [String]()
    
    var delegate: FindAFriendDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchContacts()
    }
    
    func fetchContacts() {
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            if granted {
                let keys = [CNContactGivenNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                do {
                    try store.enumerateContacts(with: request, usingBlock: { contact, stopPointer in
                        self.friendsArray.append(FetchedContact(name: contact.givenName, number: contact.phoneNumbers.first?.value.stringValue ?? ""))
                    })
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } catch let err {
                    print(err)
                }
            } else {
                print("Contacts access denied")
            }
        }
    }
    
    func filterData(text: String) {
        searchedArray = friendsArray.filter { $0.name.localizedCaseInsensitiveContains(text) }
        if searchedArray.count == 0 {
            isSearching = false
        } else {
            isSearching = true
        }
        self.tableView.reloadData()
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneTapped(_ sender: UIButton) {
        delegate?.friendsSelected(friends: selectedFriends)
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func editingChanged(_ sender: UITextField) {
        if let text = sender.text {
            filterData(text: text)
        }
       
    }
    
}

extension FindAFriendViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return searchedArray.count
        } else {
            return friendsArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSearching {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageFriendsListTableViewCell", for: indexPath) as! MessageFriendsListTableViewCell
            cell.name.text = searchedArray[indexPath.row].name
            cell.checkButton.isHidden = true
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageFriendsListTableViewCell", for: indexPath) as! MessageFriendsListTableViewCell
            cell.name.text = friendsArray[indexPath.row].name
            cell.checkButton.isHidden = true
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MessageFriendsListTableViewCell
        cell.checkButton.isHidden = false
        bottomView.isHidden = false
        if isSearching {
            selectedFriendLabel.text = searchedArray[indexPath.row].name
            selectedFriends.append(searchedArray[indexPath.row].name)
        } else {
            selectedFriendLabel.text = friendsArray[indexPath.row].name
            selectedFriends.append(friendsArray[indexPath.row].name)
        }
    }
    
}
