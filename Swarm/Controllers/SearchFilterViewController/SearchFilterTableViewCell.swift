//
//  SearchFilterTableViewCell.swift
//  Swarn
//
//  Created by Ruttab Haroon on 26/12/2021.
//

import UIKit

class SearchFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var localtionLabel: UILabel!
    @IBOutlet weak var lastCheckinLabel: UILabel!
    @IBOutlet weak var dataStackView: UIStackView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var footerLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDataA() {
        profilePictureImageView.image = UIImage(named: "user_named_v")
        usernameLabel.isHidden = false
        localtionLabel.isHidden = false
        lastCheckinLabel.isHidden = false
        usernameLabel.text = "Ali Bank"
        headerLabel.text = "Places"
        footerLabel.text = "See all named Ali"
    }
    
    func setDataB() {
        profilePictureImageView.image = UIImage(named: "user_named_v")
        usernameLabel.isHidden = false
        localtionLabel.isHidden = true
        lastCheckinLabel.isHidden = true
        
        usernameLabel.text = "No friends named Ali"
        headerLabel.text = "Friends"
        footerLabel.text = "See all named Ali"
    }
    
}
