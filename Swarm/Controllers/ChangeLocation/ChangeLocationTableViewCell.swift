//
//  ChangeLocationTableViewCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit
protocol ChangeLocationDelegate  {
    func didTapOnTagFriend()
    func didTapOnCamera()
    func didTapOnCheckIn(text: String)
    func didTapOnCancel()
}
class ChangeLocationTableViewCell: UITableViewCell,Registerable, UITextFieldDelegate {

    var friendTagged: NSAttributedString? = {
        //didSet {
            let myString = "- With "
            let myAttribute = [ NSAttributedString.Key.foregroundColor: UIColor.blue,
                                NSAttributedString.Key.font: UIFont(name: "Raleway-Regular", size: 33)!]
            let myAttrString = NSAttributedString(string: myString, attributes: myAttribute)
             //friendTagged = myAttrString
        return myAttrString
        //}
    }()
    
    var mutableStr : NSMutableAttributedString?
    
    var onFriendTapped :(()->Void)?
    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var CheckInText: UITextField!
    @IBOutlet weak var bottomView: UIStackView!
    @IBOutlet weak var tagFriendImage: UIImageView!
    @IBOutlet weak var checkinTextField: UITextView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var crossButton: UIButton!
    
    @IBOutlet weak var tagFriendMultipleImage: UIImageView!
    
    var onCheckIntapped: (()->Void)?
    
    var selectedFriendsCount: Int = 0
    
    var changeLocationDelegate : ChangeLocationDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func initState(friendsCount: Int) {
        selectedFriendsCount = friendsCount
        tagFriendImage.isHidden = true
        crossButton.isHidden = tagFriendImage.isHidden
        crossButton.setTitle("\(selectedFriendsCount)", for: .normal)
        
        checkinTextField.text = "What are you up to?" 
        checkinTextField.textColor = UIColor.lightGray
        checkinTextField.delegate = self
        
        crossButton.layer.cornerRadius = crossButton.frame.width/2
        let gesture = UITapGestureRecognizer(target: self, action: #selector(removeFriendTapped))
        tagFriendImage.addGestureRecognizer(gesture)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func didTagFriend(_ sender: Any) {
        self.changeLocationDelegate?.didTapOnTagFriend()
    }
    
    @IBAction func didTapOnCamera(_ sender: Any) {
        self.changeLocationDelegate?.didTapOnCamera()
    }
    
    @IBAction func didTapOnCheckIn(_ sender: Any) {
        self.changeLocationDelegate?.didTapOnCheckIn(text: checkinTextField.text)
    }
    
    @IBAction func didTapOnCancel(_ sender: Any) {
        self.changeLocationDelegate?.didTapOnCancel()
    }
  
    @IBAction func didTapOnRemoveFriendButton(_ sender: Any) {
        if let v = onFriendTapped {
            v()
        }
    }
    
    
    @objc func removeFriendTapped() {
        if let v = onFriendTapped {
            v()
        }
    }
    
    
}

extension ChangeLocationTableViewCell : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
//        if textView.text.lowercased() == "- with alex" {
//            textView.addHyperLinksToText(originalText: textView.text, hyperLinks: ["here": "- With Alex"])
//            DispatchQueue.main.async {
//                textView.endFloatingCursor()
//            }
//        }
//        else
            if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black

        }
        else {
            textView.endFloatingCursor()
            let myAttribute = [ NSAttributedString.Key.foregroundColor: UIColor.black,
                                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 33)]
            let myAttrString = NSAttributedString(string: checkinTextField.text.replacingOccurrences(of: "- With Alex", with: " "), attributes: myAttribute)
            
            mutableStr = NSMutableAttributedString()
            mutableStr!.append(myAttrString)
            mutableStr!.append(friendTagged!)
            self.checkinTextField.attributedText = mutableStr
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "What are you up to?"
            textView.textColor = UIColor.lightGray
        }
    }
}



extension UITextView {

  func addHyperLinksToText(originalText: String, hyperLinks: [String: String]) {
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    let attributedOriginalText = NSMutableAttributedString(string: originalText)
    for (hyperLink, urlString) in hyperLinks {
        let linkRange = attributedOriginalText.mutableString.range(of: hyperLink)
        let fullRange = NSRange(location: 0, length: attributedOriginalText.length)
        attributedOriginalText.addAttribute(NSAttributedString.Key.link, value: urlString, range: linkRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: fullRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.font, value:  UIFont(name: "Raleway-Regular", size: 33)!, range: fullRange)
    }
    
    self.linkTextAttributes = [
        NSAttributedString.Key.foregroundColor: UIColor.blue,
        NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
    ]
    self.attributedText = attributedOriginalText
  }
    
//    func fixx(allText) {
//        let yourAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
//        let yourOtherAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 25)]
//
//        let partOne = NSMutableAttributedString(string: "This is an example ", attributes: yourAttributes)
//        let partTwo = NSMutableAttributedString(string: "for the combination of Attributed String!", attributes: yourOtherAttributes)
//
//        partOne.append(partTwo)
//    }
}
