//
//  ChangeLocationViewController.swift
//  Swarn
//
// Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit
import AttachmentInput
import RxSwift
import RxDataSources
import BSImagePicker
import ProgressHUD
import Photos

class ChangeLocationViewController: BaseViewController , UITableViewDelegate , UITableViewDataSource, ChangeLocationDelegate, UITextViewDelegate{
   
    @IBOutlet weak var navibarCustomView: UIView!
    var isViewDispear : Bool = false
    var friendTagged : Bool?
    private var showInputView = false
    var imagearr = [Data]()
    var bottomView : UIView!
    var friendSelectedReOpen : ((Bool)->())?
    
    var checkInImage : UIImage?
    
    var selectedContacts = ""
    var selectedContactsCount: Int = 0

    private var attachmentInput: AttachmentInput!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

 
    
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(types: ChangeLocationTableViewCell.self)
        setupNavigationBar()
//        setupAttachmentInput()
        // Do any additional setup after loading the view.
        
    }
    @objc func toggleFirstResponder() {
          if self.showInputView {
              self.showInputView = false
             
              self.reloadInputViews()
          } else {
              self.showInputView = true
             
              self.reloadInputViews()
          }
      }
    
      

   private func setupAttachmentInput() {
          let config = AttachmentInputConfiguration()
          config.thumbnailSize = CGSize(width: 105 * UIScreen.main.scale, height: 105 * UIScreen.main.scale)
          self.attachmentInput = AttachmentInput(configuration: config)
          self.attachmentInput.delegate = self
      }
    
    override var inputView: UIView? {
          if self.isFirstResponder && self.showInputView {
              if self.attachmentInput.view != nil {
//                  self.attachmentInput.view.removeFromSuperview()
              }
              
              return self.attachmentInput.view
          } else {
              return nil
          }
      }
      

   override var inputAccessoryView: UIView? {
          if self.isFirstResponder {
              if self.bottomView != nil {
//                  self.bottomView.removeFromSuperview()
              }
              
              return self.bottomView
          } else {
              return nil
          }
      }

      override var canBecomeFirstResponder: Bool {
          return true
      }
      


      
      override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
//          if !self.isFirstResponder {
//              self.becomeFirstResponder()
//          }
          setupNavigationBar()
          if isViewDispear {
//              self.navibarCustomView.isHidden =  false
          }
         
          self.navigationController?.isNavigationBarHidden = false
      }

      override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          if self.isFirstResponder {
              self.resignFirstResponder()
          }
      }

    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        let menuBtn1 = UIButton(type: .custom)
        menuBtn1.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn1.imageView?.contentMode = .scaleAspectFit
        menuBtn1.setImage(UIImage(named:"Group 17"), for: .normal)
        menuBtn1.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem1 = UIBarButtonItem(customView: menuBtn1)
        let currWidth1 = menuBarItem1.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth1?.isActive = true
        let currHeight1 = menuBarItem1.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight1?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem , menuBarItem1], navigationRightBarButtonsItems: [], NavigationTitle: "Location Name" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellCollection = tableView.getCell(type: ChangeLocationTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
        cellCollection.initState(friendsCount: selectedContactsCount)
        cellCollection.changeLocationDelegate = self
        if friendTagged ?? false {
            cellCollection.tagFriendImage.isHidden = false
            cellCollection.crossButton.isHidden = cellCollection.tagFriendImage.isHidden
            
            let myString = "- With \(selectedContacts)"
            let myAttribute = [ NSAttributedString.Key.foregroundColor: UIColor.blue,
                                NSAttributedString.Key.font: UIFont(name: "Raleway-Regular", size: 33)!]
            let myAttrString = NSAttributedString(string: myString, attributes: myAttribute)
            
            //cellCollection.friendTagged = myAttrString
            cellCollection.checkinTextField.attributedText = myAttrString
            //cellCollection.checkinTextField.delegate = self
            
            //bottomView = cellCollection.cameraView crashes here
            
            cellCollection.onFriendTapped = {
                cellCollection.checkinTextField.text = ""
                cellCollection.tagFriendImage.isHidden = true
                cellCollection.crossButton.isHidden = cellCollection.tagFriendImage.isHidden
                
            }
            
        }
        return cellCollection
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChangeLocationViewController") as! ChangeLocationViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }

    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didTapOnTagFriend() {
        print("did Tap On friend tag")
        let controler = self.storyboard?.instantiateViewController(withIdentifier: "FindAFriendViewController") as! FindAFriendViewController
        controler.modalPresentationStyle = .overFullScreen
        controler.delegate = self
        self.present(controler, animated: true, completion: nil)
    }
    
    func reloadData() {
        self.friendTagged = true
        self.tableview.reloadData()
    }
    
    func didTapOnCamera() {
        let imagePicker = ImagePickerController()

        presentImagePicker(imagePicker, select: { (asset) in
            // User selected an asset. Do something with it. Perhaps begin processing/upload?
            self.checkInImage = self.getAssetThumbnail(asset: asset)
        }, deselect: { (asset) in
            // User deselected an asset. Cancel whatever you did when asset was selected.
        }, cancel: { (assets) in
            // User canceled selection.
        }, finish: { (assets) in
            // User finished selection assets.
        })
//        toggleFirstResponder()
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset,
                                targetSize: CGSize(width: 100.0, height: 100.0),
                                contentMode: .aspectFit,
                                options: option,
                                resultHandler: {(result, info)->Void in
                thumbnail = result!
        })
        return thumbnail
    }
    func checkIn(image: UIImage, description: String) {
        ProgressHUD.show()
        
        
        let place = "{\"business_status\":\"OPERATIONAL\",\"geometry\":{\"location\":{\"lat\":24.943561555936032,\"lng\":67.0472269964398},\"viewport\":{\"northeast\":{\"lat\":-33.86624217010728,\"lng\":151.2029345298927},\"southwest\":{\"lat\":-33.86894182989272,\"lng\":151.2002348701073}}},\"icon\":\"https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/generic_business-71.png\",\"icon_background_color\":\"#7B9EB0\",\"icon_mask_base_uri\":\"https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/generic_pinlet\",\"name\":\"SydneyShowboats-DinnerCruiseWithShow\",\"opening_hours\":{\"open_now\":false},\"photos\":[{\"height\":749,\"html_attributions\":[\"AGoogleUser\"],\"photo_reference\":\"Aap_uEBj0_cGZIo6j3k3Lpfbv8_7QMKviM_RHC3pIFBxj-t_uC2Mrr4nTkgEjS42bIlUeOLnLyHhtEAvudCZ-rAXmeeIsGphDtP3KhHRtf_GCjLPHsuRRb0Wt44GIp1LtVeK04sM8klQoRVKBcco8owNghTjIzMazErRHa6eUtSSwOuPIHZL\",\"width\":1000}],\"place_id\":\"ChIJjRuIiTiuEmsRCHhYnrWiSok\",\"plus_code\":{\"compound_code\":\"46J2+XJSydney,NewSouthWales\",\"global_code\":\"4RRH46J2+XJ\"},\"rating\":4.1,\"reference\":\"ChIJjRuIiTiuEmsRCHhYnrWiSok\",\"scope\":\"GOOGLE\",\"types\":[\"travel_agency\",\"restaurant\",\"food\",\"point_of_interest\",\"establishment\"],\"user_ratings_total\":108,\"vicinity\":\"32ThePromenade,KingStreetWharf,5,Sydney\"}"
//        let place: [String: Any] = [
//            "business_status": "OPERATIONAL",
//            "geometry": [
//              "location": [ "lat": 24.943561555936032,
//                            "lng": 67.0472269964398
//                          ],
//              "viewport": [ "northeast": [ "lat": -33.86624217010728,
//                                           "lng": 151.2029345298927
//                                         ],
//                            "southwest": [ "lat": -33.86894182989272,
//                                           "lng": 151.2002348701073
//                                         ]
//                          ]
//            ],
//            "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/generic_business-71.png",
//            "icon_background_color": "#7B9EB0",
//            "icon_mask_base_uri": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/generic_pinlet",
//            "name": "Sydney Showboats - Dinner Cruise With Show",
//            "opening_hours": [
//              "open_now": false
//            ],
//            "photos": [
//              [
//                "height": 749,
//                "html_attributions": [
//                  "<a href=\"https://maps.google.com/maps/contrib/105311284660389698992\">A Google User</a>"
//                ],
//                "photo_reference": "Aap_uEBj0_cGZIo6j3k3Lpfbv8_7QMKviM_RHC3pIFBxj-t_uC2Mrr4nTkgEjS42bIlUeOLnLyHhtEAvudCZ-rAXmeeIsGphDtP3KhHRtf_GCjLPHsuRRb0Wt44GIp1LtVeK04sM8klQoRVKBcco8owNghTjIzMazErRHa6eUtSSwOuPIHZL",
//                "width": 1000
//              ]
//            ],
//            "place_id": "ChIJjRuIiTiuEmsRCHhYnrWiSok",
//            "plus_code": [
//              "compound_code": "46J2+XJ Sydney, New South Wales",
//              "global_code": "4RRH46J2+XJ"
//            ],
//            "rating": 4.1,
//            "reference": "ChIJjRuIiTiuEmsRCHhYnrWiSok",
//            "scope": "GOOGLE",
//            "types": [
//              "travel_agency",
//              "restaurant",
//              "food",
//              "point_of_interest",
//              "establishment"
//            ],
//            "user_ratings_total": 108,
//            "vicinity": "32 The Promenade, King Street Wharf, 5, Sydney"
//          ]
        
        let params: [String: Any] = ["description": description, // "this is my 1st checkin",
                                     "sticker": 1,
                                     "lat": 24.934982442468545,
                                     "lng": 67.04006558466043,
                                     "place_object": place,
                                     "off_the_grid": 0,
                                     "share_on_fb": 1,
                                     "share_on_twitter": 0]

        Networking.shared.checkIn(image: image, endPoint: .checkIn, parameters: params) { result in
            ProgressHUD.dismiss()
            switch result {
            case.success(let value):
                print(value)
                
                dump(value)
                
                if value.data?.response?.count ?? 0 > 0 {
                    self.friendSelectedReOpen?(true)
                   // self.navigationController?.popViewController(animated: true)
                    let storyboard = UIStoryboard.init(name: "Components", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "AccessPermissionPopupViewController") as! AccessPermissionPopupViewController
                    controller.modalPresentationStyle = .overCurrentContext
                    controller.modalPresentationStyle  = .overFullScreen
                    controller.modalTransitionStyle = .crossDissolve
                    if let vc = UIApplication.getTopViewController() {
                        vc.present(controller, animated: true, completion: nil)
                    }
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
                
                
            case .failure(let error):
                self.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
    
    func didTapOnCheckIn(text: String) {
        checkIn(image: checkInImage ?? UIImage(), description: text)
        
        
    }
    
    func didTapOnCancel() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func getCheckInText(text: String) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChangeLocationViewController: AttachmentInputDelegate {
    func inputImage(imageData: Data, fileName: String, fileSize: Int64, fileId: String, imageThumbnail: Data?) {
//        self.viewModel.addData(fileName: fileName, fileSize: fileSize, fileId: fileId, imageThumbnail: imageThumbnail)
        self.imagearr.append(imageThumbnail!)
        print("Image added")
        
        
    }
    
    func inputMedia(url: URL, fileName: String, fileSize: Int64, fileId: String, imageThumbnail: Data?) {
//        self.viewModel.addData(fileName: fileName, fileSize: fileSize, fileId: fileId, imageThumbnail: imageThumbnail)
    }
    
    func removeFile(fileId: String) {
//        self.viewModel.removeData(fileId: fileId)
    }
    
    func imagePickerControllerDidDismiss() {
        // Do nothing
    }
    
    func onError(error: Error) {
        let nserror = error as NSError
        if let attachmentInputError = error as? AttachmentInputError {
            print(attachmentInputError.debugDescription)
        } else {
            print(nserror.localizedDescription)
        }
    }
    
    
}

extension ChangeLocationViewController: FindAFriendDelegate {
    
    func friendsSelected(friends: [String]) {
        selectedContacts = friends.reduce("") { $0 + $1 + ", "}
        selectedContactsCount = friends.count
        reloadData()
    }
    
}
