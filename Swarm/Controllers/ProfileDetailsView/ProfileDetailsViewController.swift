//
//  ProfileDetailsViewController.swift
//  Swarn
//
//  Created by Waseem Ahmed on 13/11/2021.
//

import UIKit
import DropDown


class ProfileDetailsViewController: BaseViewController {

    @IBOutlet weak var msgFriendBtn: UIButton!
    @IBOutlet weak var imgSticker1: UIImageView!
    @IBOutlet weak var imgSticker2: UIImageView!
    @IBOutlet weak var imgSticker3: UIImageView!
    @IBOutlet weak var imgSticker4: UIImageView!
    @IBOutlet weak var imgSticker5: UIImageView!
    @IBOutlet weak var imgSticker6: UIImageView!
    @IBOutlet weak var imgSticker7: UIImageView!
    @IBOutlet weak var imgSticker8: UIImageView!
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var firstImage: UIImageView!
   
    @IBOutlet weak var secondImage: UIImageView!
    
    @IBOutlet weak var thirdImage: UIImageView!
    
    @IBOutlet weak var friendNameLabel: UILabel!
    
    @IBOutlet weak var fourthImage: UIImageView!
    @IBOutlet weak var friendView: UIView!
    
    @IBOutlet weak var previewImageView: UIImageView!
    
    var selectedImage: UIImage?
    
    let imagePickerController = UIImagePickerController()
    
    var isCommingfromMsg : Bool = false
    
    override func viewWillLayoutSubviews() {
        previewImageView.layer.cornerRadius = previewImageView.frame.width/2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let firstImageTapGeasture = UITapGestureRecognizer.init(target: self, action: #selector(didTapFirstImage))
        firstImage.isUserInteractionEnabled = true
        firstImage.addGestureRecognizer(firstImageTapGeasture)
        
        let secondImageTapGeasture = UITapGestureRecognizer.init(target: self, action: #selector(didTapSecondImage))
        secondImage.isUserInteractionEnabled = true
        secondImage.addGestureRecognizer(secondImageTapGeasture)
       
        imagePickerController.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickerController.sourceType = .camera
        } else {
            imagePickerController.sourceType = .photoLibrary
        }
        imagePickerController.mediaTypes = ["public.image"]
        imagePickerController.allowsEditing = true
//        let thirdImageTapGeasture = UITapGestureRecognizer.init(target: self, action: #selector(didTapThirdImage))
//        thirdImage.isUserInteractionEnabled = true
//        thirdImage.addGestureRecognizer(thirdImageTapGeasture)
//
//        let forthImageTapGeasture = UITapGestureRecognizer.init(target: self, action: #selector(didTapFourthImage))
//        fourthImage.isUserInteractionEnabled = true
//        fourthImage.addGestureRecognizer(forthImageTapGeasture)
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "shadowImage"), for: .default)
            // Sets shadow (line below the bar) to a blank image
//        self.navigationController?.navigationBar.shadowImage = UIImage(named: "shadowImage")
            // Sets the translucent background color
        self.navigationController?.navigationBar.backgroundColor = .systemFill
            // Set translucent. (Default value is already true, so this can be removed if desired.)
//        self.navigationController?.navigationBar.isTranslucent = true

        addTapGesture(image: imgSticker1)
        addTapGesture(image: imgSticker2)
        addTapGesture(image: imgSticker3)
        addTapGesture(image: imgSticker4)
        addTapGesture(image: imgSticker5)
        addTapGesture(image: imgSticker6)
        addTapGesture(image: imgSticker7)
        addTapGesture(image: imgSticker8)
        setupNavigationBar()
    }
    
    
    @objc func didTapFirstImage() {
        moveToImageView()
    }
    
    @objc func didTapSecondImage() {
        moveToImageView()
    }
    
    @IBAction func imageTapped(_ sender: UIButton) {
        present(imagePickerController, animated: true, completion: nil)
    }
    //    @objc func didTapThirdImage() {
//        moveToImageView()
//    }
//
//    @objc func didTapFourthImage() {
//        moveToImageView()
//    }
    
    func moveToImageView() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ImagePreviewerViewController") as! ImagePreviewerViewController
        controller.modalPresentationStyle = .fullScreen
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "shadowImage"), for: .default)
            // Sets shadow (line below the bar) to a blank image
//        self.navigationController?.navigationBar.shadowImage = UIImage(named: "shadowImage")
            // Sets the translucent background color
        self.navigationController?.navigationBar.backgroundColor = UIColor.backgroundDark
            // Set translucent. (Default value is already true, so this can be removed if desired.)
//        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationBar()
        
        if isCommingfromMsg {
            msgFriendBtn.isHidden = false
        }
    }
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        //menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        //menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBtn2 = UIButton(type: .custom)
        menuBtn2.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn2.imageView?.contentMode = .scaleAspectFit
        menuBtn2.setImage(UIImage(named:"Icon ionic-ios-settings"), for: .normal)
        menuBtn2.addTarget(self, action: #selector(tapOnSettingBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        let menuBarItem2 = UIBarButtonItem(customView: menuBtn2)
        let currWidth2 = menuBarItem2.customView?.widthAnchor.constraint(equalToConstant: 24)
        //currWidth2?.isActive = true
        let currHeight2 = menuBarItem2.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight2?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [menuBarItem2], NavigationTitle: "Profile" , isSearchBar: false , isLable: true , isBackColor: false)
       
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            // Sets shadow (line below the bar) to a blank image
        self.navigationController?.navigationBar.shadowImage = UIImage()
     
            // Sets the translucent background color
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func tapOnSettingBtn() {
        let vc = AppStoryboard.Settings.instance.instantiateViewController(identifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    //MARK: Helpers
    func addTapGesture(image : UIImageView) {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        image.isUserInteractionEnabled = true
        image.addGestureRecognizer(tapGestureRecognizer)
    }
    
    
       @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
//            let tappedImage = tapGestureRecognizer.view as! UIImageView
           
           let vc = AppStoryboard.Sticker.instance.instantiateViewController(withIdentifier: "StickersParentController") as! StickersParentController
           self.navigationController?.pushViewController(vc, animated: true)
        }
    
    
    
    //MARK: Actions
    
    @IBAction func btnShareTapped(_ sender : UIButton) {
        let dropDown = DropDown()
        dropDown.dataSource = ["Unfriend", "Mute Notification", "Contact"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            let vc = AppStoryboard.Components.instance.instantiateViewController(identifier: "RemoveFriendPopUpViewController") as! RemoveFriendPopUpViewController
           
            self.present(vc, animated: true, completion: nil)
            dropDown.hide()
        }
        dropDown.show()
    }
    
    @IBAction func didTapOnCheckIn(_ sender: UIButton) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "ViewCheckInsViewController") as! ViewCheckInsViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func didTapOnCategories(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "CategoriesParentViewController") as! CategoriesParentViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func didTapOnMayorShip(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "MayorshipsViewController") as! MayorshipsViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func didTapOnSavePlaces(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(vc, animated: true)
  
    }
    
   
    @IBAction func didTapOnStreaks(_ sender: Any) {
        let vc = AppStoryboard.Profile.instance.instantiateViewController(identifier: "StreaksViewController") as! StreaksViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func didTapOnvisitedPlace(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func didTapOnAddFriend(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "ContactListViewController") as! ContactListViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func addFriendFriendSectionTapped(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "ContactListViewController") as! ContactListViewController
        vc.contactSelected = { [weak self] friend in
            self?.setFriendName(name: friend.first ?? "")
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setFriendName(name: String) {
        self.friendNameLabel.text = name
    }
    
    @IBAction func didTapOnCheckInBtn(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "LocationViewController") as! LocationViewController
        vc.iscommingfromProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func didTpaOnFriendsAdd(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "ContactListViewController") as! ContactListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func didTapOnEdit(_ sender: Any) {
        let vc = AppStoryboard.Profile.instance.instantiateViewController(identifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapOnMessage(_ sender: Any) {
        print("Msg Friend")
        
        let vc = AppStoryboard.Chat.instance.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//                self.tabBarController?.tabBar.isTranslucent = true
//                self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSeeAllTapped(_ sender : UIButton) {
        let vc = AppStoryboard.Sticker.instance.instantiateViewController(withIdentifier: "StickersParentController") as! StickersParentController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ProfileDetailsViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else {
                    return
                }
        selectedImage = image
        previewImageView.image = selectedImage!
        imagePickerController.dismiss(animated: true, completion: nil)
    }
}

