//
//  PlaceCheckinListViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class PlaceCheckinListViewController: BaseViewController {

    @IBOutlet weak var fromDateView: UIView!
    @IBOutlet weak var toDateView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var leaders : [String] = ["",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "PlaceTableViewCell", bundle: nil), forCellReuseIdentifier: "PlaceTableViewCell")
        tableView.register(UINib(nibName: "EmptyTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyTableViewCell")
        tableView.register(UINib(nibName: "PlaceCheckinDateTableViewCell", bundle: nil), forCellReuseIdentifier: "PlaceCheckinDateTableViewCell")
        
        self.fromDateView.layer.cornerRadius = self.fromDateView.frame.size.height / 2
        self.fromDateView.clipsToBounds = true
        self.toDateView.layer.cornerRadius = self.toDateView.frame.size.height / 2
        self.toDateView.clipsToBounds = true
//
        
        setupNavigationBar()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(moveTo))
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"crossWhite"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [], NavigationTitle: "" , isSearchBar: true , isLable: true)

    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func moveTo() {
        let controller  = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "PersonProfileViewController") as! PersonProfileViewController
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

extension PlaceCheckinListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaders.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item == 0 {
            guard let leaderCell = tableView.dequeueReusableCell(withIdentifier: "PlaceTableViewCell", for: indexPath) as? PlaceTableViewCell else {
                fatalError()
            }
            return leaderCell
        }
        else if indexPath.item == 1 {
            guard let emptyCell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell", for: indexPath) as? EmptyTableViewCell else {
                fatalError()
            }
            emptyCell.containerView.backgroundColor = .lightGray.withAlphaComponent(0.3)
            return emptyCell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCheckinDateTableViewCell", for: indexPath) as? PlaceCheckinDateTableViewCell else {
            fatalError()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item == 0 {
            return 104
        }
        else if indexPath.item == 1 {
            return 20
        }
        return 195
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item == 1 {
            return 20
        }
        return UITableView.automaticDimension
    }
}
