//
//  BaseViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on on 07/11/2021.
//

import UIKit

class BaseViewController: UIViewController {
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
        var panGestureRecognizer: UIPanGestureRecognizer!


    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = .white
        addGesture()
        // Do any additional setup after loading the view.
    }
    
    func addGesture() {
            
            guard navigationController?.viewControllers.count ?? 0 > 1 else {
                return
            }
            
            panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(SwipeRightToPopViewController.handlePanGesture(_:)))
            self.view.addGestureRecognizer(panGestureRecognizer)
        }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
            
            let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
            
            switch panGesture.state {
            
            case .began:
                navigationController?.delegate = self
                _ = navigationController?.popViewController(animated: true)
                
            case .changed:
                if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                    percentDrivenInteractiveTransition.update(percent)
                }
                
            case .ended:
                let velocity = panGesture.velocity(in: view).x
                
                // Continue if drag more than 50% of screen width or velocity is higher than 1000
                if percent > 0.5 || velocity > 1000 {
                    percentDrivenInteractiveTransition.finish()
                } else {
                    percentDrivenInteractiveTransition.cancel()
                }
                
            case .cancelled, .failed:
                percentDrivenInteractiveTransition.cancel()
                
            default:
                break
            }
        }
    func ForgetPasswordCustom(navigationLeftBarButtonItems : [UIBarButtonItem] , navigationRightBarButtonsItems : [UIBarButtonItem] , NavigationTitle : String , isSearchBar : Bool = true , isLable : Bool = false , isBackColor : Bool = true, hasLeft: Bool = false) {
        
        if hasLeft { navigationItem.leftBarButtonItems = navigationLeftBarButtonItems }
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "Icon feather-arrow-down")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "Icon feather-arrow-down")
        navigationItem.rightBarButtonItems = navigationRightBarButtonsItems
        navigationItem.backButtonTitle = ""
//        rgb(254,167,51)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
        
        if isBackColor {
            navigationController?.navigationBar.backgroundColor = UIColor.init(red: 254/255, green: 167/255, blue: 39/255, alpha: 1.0)
    //        rgb(254,150,39)
            self.navigationController?.statusBarColorChange(color: UIColor.init(red: 254/255, green: 150/255, blue: 39/255, alpha: 1))
        }
       
        if isLable {
            let lbNavTitle = UILabel (frame: CGRect(x: 0, y: 40, width: 320, height: 40))
                lbNavTitle.center = CGPoint(x: 160, y: 285)
                lbNavTitle.textAlignment = .left
                lbNavTitle.textColor = .white
                lbNavTitle.font = UIFont(name: "Raleway-SemiBold", size: 22)!
                lbNavTitle.text = NavigationTitle
                self.navigationItem.titleView = lbNavTitle
        }
     
        if isSearchBar {
            let  searchBar = UISearchBar()
            searchBar.delegate = self
             searchBar.placeholder = "Search"
                 navigationItem.titleView = searchBar
            
//            let gesture = UITapGestureRecognizer(target: self, action: #selector(onSearchBarTapped))
//            searchBar.addGestureRecognizer(gesture)
        }
        
        
    }
    
    @objc func onSearchBarTapped() {
        guard let searchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchFilterViewController") as? SearchFilterViewController else {return}
        let transition = CATransition()
       transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
       navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(searchVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BaseViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        let searchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchFilterViewController") as! SearchFilterViewController
        let transition = CATransition()
       transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
       navigationController?.view.layer.add(transition, forKey: kCATransition)
        searchBar.resignFirstResponder()
        searchBar.endEditing(true)
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
}

extension BaseViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
    
    
    }

extension UITextField{
   @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}
