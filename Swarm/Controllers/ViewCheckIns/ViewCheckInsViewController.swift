//
//  ViewCheckInsViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit

class ViewCheckInsViewController: BaseViewController,UITableViewDelegate , UITableViewDataSource, TopCheckInDelegate {
   
   
    

    @IBOutlet weak var tableview: UITableView!
    var dataTable :  [[[String : Any]]] = [
        [
            ["cell":"TopCheckInTableViewCell"] ,
            ["cell":"MiddleCheckInTableViewCell"] ,
            ["cell":"LastCheckInTableViewCell"],
        ],
        [
            ["cell":"CheckInHistoryCell"],
            ["cell":"CheckInHistoryCell"],
            ["cell":"CheckInHistoryCell"],
            ["cell":"CheckInHistoryCell"],
        ]
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.delegate = self
        self.tableview.dataSource = self
        
        tableview.register(types: TopCheckInTableViewCell.self,MiddleCheckInTableViewCell.self,LastCheckInTableViewCell.self, CheckInHistoryCell.self)
         setupNavigationBar()
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        // Do any additional setup after loading the view.
    }
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [], NavigationTitle: "Check-ins" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataTable[section].count
        //return dataTable.count
    }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let sectionSet = dataTable[indexPath.section]
            let datatable = sectionSet[indexPath.row]
            let data = datatable["cell"] as? String ?? ""
            switch data {
            case "TopCheckInTableViewCell" :
                guard let cellCollection = tableView.getCell(type: TopCheckInTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                cellCollection.topCheckInDelegate = self
                cellCollection.selectionStyle = .none
                    return cellCollection
            case "MiddleCheckInTableViewCell" :
                guard let cellCollection = tableView.getCell(type: MiddleCheckInTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                cellCollection.selectionStyle = .none
                    return cellCollection
            case "LastCheckInTableViewCell" :
                guard let cellCollection = tableView.getCell(type: LastCheckInTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                cellCollection.selectionStyle = .none
                    return cellCollection
            case "CheckInHistoryCell" :
                guard let cellCollection = tableView.getCell(type: CheckInHistoryCell.self,for: indexPath)  else { return UITableViewCell() }
                    cellCollection.initState()
                cellCollection.selectionStyle = .none
                    return cellCollection
            default :
                return UITableViewCell(frame: .zero)
            }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let datecCheckinHeaderView = Bundle.main.loadNibNamed("CheckInHeaderView", owner: self, options: nil)?.first as! CheckInHeaderView
        datecCheckinHeaderView.stickyTopView.isHidden = section == 0  ? true : false
        datecCheckinHeaderView.headerTitleLabel.text = section == 0 ? "Fri, Oct 2021" : "Sat, Oct 2021"
        return datecCheckinHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 46
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckInDetailViewController") as! CheckInDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
        
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
        

    func didTapOnCircle(sender: UIButton) {
        print("didTapOnCircle")
    }
    
    func didTapOnCell(sender: UIButton) {
        print("didTapOnCell")
    }
    
    func didTapOnFirstImage() {
        print("didTapOnFirstImage")
    }
    
    func didTapOnSecondImage() {
        print("didTapOnSecondImage")
    }
    
    func didTapOnThirdImage() {
        print("didTapOnThirdImage")
    }
    
    func didTapOnfourthImage() {
        print("didTapOnfourthImage")
    }
    
}
