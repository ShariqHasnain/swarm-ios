//
//  StickersParentController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 24/11/2021.
//

import UIKit

class StickersParentController: UIViewController {

    @IBOutlet weak var firstChildView: UIView!
    @IBOutlet weak var secondChildView: UIView!
    @IBOutlet weak var segmentControl: CustomSegmentedControl! {
        didSet {
            segmentControl.setButtonTitles(buttonTitles: ["COLLECTIBLE","BONUS"])
            segmentControl.selectorViewColor = UIColor.underlineColor
            segmentControl.selectorTextColor = UIColor.selectedTextColor
            segmentControl.textColor = UIColor.btnTextColor
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
