//
//  StickerCollectionViewCell.swift
//  Swarn
//
//  Created by Q.M.S on 01/01/2022.
//

import UIKit

class StickerCollectionViewCell: UICollectionViewCell, Registerable {

    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
