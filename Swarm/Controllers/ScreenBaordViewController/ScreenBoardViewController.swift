//
//  ScreenBoardViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 08/11/2021.
//

import UIKit
import ProgressHUD
import SDWebImage

class ScreenBoardViewController: BaseViewController, UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var enableLocation: UIButton!
    @IBOutlet weak var paging: UIPageControl!
    @IBOutlet weak var dismissButton: UIButton!
    
    var count : Int = 0
    var intro : [IntroScreens] = []
    
    @IBOutlet weak var Details: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        collectionView.register(types: ScreenBoardCollectionViewCell.self)
        
        
        let buttonNext = UIBarButtonItem(title: "Next", style: .plain, target: self, action:  #selector(tapOnNextBtn))
        
        //        let buttonBack = UIBarButtonItem(image: UIImage(named: "Icon feather-arrow-down"), style: .plain, target: self, action: #selector(tapOnBackBtn))
        
        ForgetPasswordCustom(navigationLeftBarButtonItems: [], navigationRightBarButtonsItems: [buttonNext], NavigationTitle: "",isSearchBar: false , isLable: false)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(tapOnBackBtn))
        self.navigationItem.leftBarButtonItem?.isEnabled = false
        
        self.bgImage.image = UIImage(named: "Step1")
        self.Details.text = "Keep track of the places you visit and get helpful insights about your habits."
        
        getIntroData()
        // Do any additional setup after loading the view.
    }
    
    func getIntroData() {
        ProgressHUD.show()
        Networking.shared.getIntro(endPoint: .intro) { result in
            ProgressHUD.dismiss()
            switch result {
            case.success(let value):
                self.intro = value.data.intro_screens
                self.collectionView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Message", message: error.localizedDescription)
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return intro.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cellCollection = collectionView.getCell(type: ScreenBoardCollectionViewCell.self,for: indexPath)
        else { return UICollectionViewCell() }
        let introData = intro[indexPath.row]
        if let url = URL(string: Route.mainRoute + introData.media) {
            cellCollection.pics.sd_setImage(with: url, completed: nil)
        }
//        cellCollection.pics.image = UIImage(named: data["image_name"] as? String ?? "")
        
        return cellCollection
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        
        return CGSize(width:UIScreen.main.bounds.width/1.4, height: UIScreen.main.bounds.height/2.5)
        
    }
    
    
    @IBAction func pageControllerAction(_ sender: UIPageControl) {
        self.collectionView.scrollToItem(at: IndexPath(row: sender.currentPage, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @objc func  tapOnNextBtn() {
        
        count = count + 1
        
        if count > 0 {
            self.navigationItem.leftBarButtonItem?.isEnabled = true
        } else {
            self.navigationItem.leftBarButtonItem?.isEnabled = false
        }
        
        if count == intro.count-1 {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        
        //        if count > 0 && count < 2 {
        //            let buttonNext = UIBarButtonItem(title: "Next", style: .plain, target: self, action:  #selector(tapOnNextBtn))
        //
        //            let buttonBack = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(tapOnBackBtn))
        //            ForgetPasswordCustom(navigationLeftBarButtonItems: [buttonBack], navigationRightBarButtonsItems: [buttonNext], NavigationTitle: "",isSearchBar: false , isLable: false)
        //        }else if  count > 2 {
        //            let buttonNext = UIBarButtonItem(title: "Next", style: .plain, target: self, action:  #selector(tapOnNextBtn))
        //
        //            let buttonBack = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(tapOnBackBtn))
        //            ForgetPasswordCustom(navigationLeftBarButtonItems: [buttonBack], navigationRightBarButtonsItems: [], NavigationTitle: "",isSearchBar: false , isLable: false)
        //        }
        //        print("before count",count)
        //        if count > 0  && count <= 4{
        //        print("after",count)
        paging.currentPage = count
        self.collectionView.scrollToItem(at: IndexPath(row: count, section: 0), at: .centeredHorizontally, animated: true)
        //        }
        
        Details.text = intro[count].text
        
        if intro[count].is_last {
            self.enableLocation.isHidden = false
            self.dismissButton.isHidden = false
        } else {
            self.enableLocation.isHidden = true
            self.dismissButton.isHidden = true
        }
        
        switch count {
        case 0 :
            self.bgImage.image = UIImage(named: "Step1")
        case 1 :
            self.bgImage.image = UIImage(named: "Step2")
        case 2 :
            self.bgImage.image = UIImage(named: "Step3")
        case 3 :
            self.bgImage.image = UIImage(named: "Step4")
            
        default :
            print("Not Found")
        }
    }
    
    @objc func tapOnBackBtn() {
        count = count - 1
        //        if count < 0 {
        //            count = 0
        //        }
        
        if count > 0 {
            self.navigationItem.leftBarButtonItem?.isEnabled = true
            //                let buttonNext = UIBarButtonItem(title: "Next", style: .plain, target: self, action:  #selector(tapOnNextBtn))
            //
            //                let buttonBack = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(tapOnBackBtn))
            //                ForgetPasswordCustom(navigationLeftBarButtonItems: [], navigationRightBarButtonsItems: [buttonNext], NavigationTitle: "",isSearchBar: false , isLable: false)
            
        } else {
            self.navigationItem.leftBarButtonItem?.isEnabled = false
        }
        
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        
        //        print("before count",count)
        //        if count <= 4  {
        //       print("after",count)
        paging.currentPage = count
        self.collectionView.scrollToItem(at: IndexPath(row: count, section: 0), at: .centeredHorizontally, animated: true)
        //        }
        
        Details.text = intro[count].text
        
        if intro[count].is_last {
            self.enableLocation.isHidden = false
        } else {
            self.enableLocation.isHidden = true
        }
        
        switch count {
        case 0 :
            self.bgImage.image = UIImage(named: "Step1")
        case 1 :
            self.bgImage.image = UIImage(named: "Step2")
        case 2 :
            self.bgImage.image = UIImage(named: "Step3")
        case 3 :
            self.bgImage.image = UIImage(named: "Step4")
        default :
            print("Not Found")
        }
    }
    
    func goToDashboard(){
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "ContactListViewController") as! ContactListViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        //        let controller = self.storyboard?.instantiateViewController(withIdentifier: "InviteFriendsViewController") as! InviteFriendsViewController
        ////        controller.navigationController?.navigationBar.isHidden = true
        //        controller.modalTransitionStyle = .flipHorizontal
        //        controller.modalPresentationStyle = .fullScreen
        //        self.navigationController?.pushViewController(controller, animated: true)
        ////        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func dismissTapped(_ sender: UIButton) {
        goToDashboard()
    }
    
    @IBAction func didTapOnLocation(_ sender: Any) {
        openAppSettings()
    }
    
    func openAppSettings() {
        let url = URL(string: "\(UIApplication.openSettingsURLString)&path=LOCATION/\(Bundle.main.bundleIdentifier!)")
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
