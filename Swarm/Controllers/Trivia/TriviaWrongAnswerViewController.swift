//
//  TriviaWrongAnswerViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit
import SwiftGifOrigin

class TriviaWrongAnswerViewController: UIViewController {

    @IBOutlet weak var seeProfileButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageGif: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dismiss(animated:completion:)))
        self.view.addGestureRecognizer(gesture)
        
        seeProfileButton.setTitle("", for: .normal)
        
        imageGif.loadGif(name: "Spiral")
    }
    
    override func viewDidLayoutSubviews() {
        containerView.cornerRadius = 8
    }

    
    @IBAction func onUploadTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func onSeeProfileTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
