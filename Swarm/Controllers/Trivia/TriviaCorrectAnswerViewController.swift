//
//  TriviaCorrectAnswerViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit
import SwiftGifOrigin

class TriviaCorrectAnswerViewController: UIViewController {
    
    @IBOutlet weak var imagegif: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var seeProfileButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let gesture = UITapGestureRecognizer(target: self, action: #selector(moveToInCorrectTriviaScreen))
        self.view.addGestureRecognizer(gesture)
        
        seeProfileButton.setTitle("", for: .normal)
        
        imagegif.loadGif(name: "Spiral")
    }
    
    override func viewDidLayoutSubviews() {
        containerView.cornerRadius = 8
    }

    @IBAction func seeProfileTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func onCloseTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onUploadTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //temp
    @objc func moveToInCorrectTriviaScreen() {
        self.dismiss(animated: true) {  [weak self] in
            let controller = self?.storyboard!.instantiateViewController(withIdentifier: "TriviaWrongAnswerViewController") as! TriviaWrongAnswerViewController
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalPresentationStyle  = .overFullScreen
            controller.modalTransitionStyle = .crossDissolve
            if let vc = UIApplication.getTopViewController() {
                vc.present(controller, animated: true, completion: nil)
            }
        }
    }
    
}
