//
//  TriviaQuestionViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class TriviaQuestionViewController: UIViewController {

    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondbutton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var thirdButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        firstButton.layer.cornerRadius = firstButton.frame.size.height / 2
        firstButton.clipsToBounds = true
        
        secondbutton.layer.cornerRadius = secondbutton.frame.size.height / 2
        secondbutton.clipsToBounds = true
        
        thirdButton.layer.cornerRadius = thirdButton.frame.size.height / 2
        thirdButton.clipsToBounds = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dismiss(animated:completion:)))
        self.view.addGestureRecognizer(gesture)
    }
    
    override func viewDidLayoutSubviews() {
        containerView.cornerRadius = 8
    }
    

    @IBAction func onFirstTapped(_ sender: Any) {
        self.dismiss(animated: true) {  [weak self]  in
            let controller = self?.storyboard!.instantiateViewController(withIdentifier: "TriviaCorrectAnswerViewController") as! TriviaCorrectAnswerViewController
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalPresentationStyle  = .overFullScreen
            controller.modalTransitionStyle = .crossDissolve
            if let vc = UIApplication.getTopViewController() {
                vc.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onSecondTapped(_ sender: Any) {
        self.dismiss(animated: true) {  [weak self] in
            let controller = self?.storyboard!.instantiateViewController(withIdentifier: "TriviaWrongAnswerViewController") as! TriviaWrongAnswerViewController
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalPresentationStyle  = .overFullScreen
            controller.modalTransitionStyle = .crossDissolve
            if let vc = UIApplication.getTopViewController() {
                vc.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onThirdTapped(_ sender: Any) {
        self.dismiss(animated: true) {[weak self]  in
            let controller = self?.storyboard!.instantiateViewController(withIdentifier: "TriviaWrongAnswerViewController") as! TriviaWrongAnswerViewController
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalPresentationStyle  = .overFullScreen
            controller.modalTransitionStyle = .crossDissolve
            self?.present(controller, animated: true, completion: nil)
            if let vc = UIApplication.getTopViewController() {
                vc.present(controller, animated: true, completion: nil)
            }
        }
    }
}
