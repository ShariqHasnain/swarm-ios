//
//  LocationViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit

class LocationViewController: BaseViewController,UITableViewDelegate , UITableViewDataSource {
   
    @IBOutlet weak var tableview: UITableView!
    var coinPopView : ((Bool)->())?
    var isForward : Bool = false
    var iscommingfromProfile : Bool = false
    
    var places : [String] = ["Four Seasons Hotel","Four Seasons Hotel","Four Seasons Hotel","Four Seasons Hotel","Four Seasons Hotel","Four Seasons Hotel","Four Seasons Hotel","Four Seasons Hotel","Four Seasons Hotel","Four Seasons Hotel","Four Seasons Hotel"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(types: LocationTableViewCell.self)
        self.navigationController?.isNavigationBarHidden = false
        self.setupNavigationBar()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isForward = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isForward = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !isForward {
            self.navigationController?.isNavigationBarHidden = true
        }
        
        if iscommingfromProfile {
            self.navigationController?.isNavigationBarHidden = false
        }
   
    }
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"crossWhite"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [], NavigationTitle: "", isSearchBar: true, isLable: true, isBackColor: true)
        self.navigationItem.backButtonTitle = ""

        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellCollection = tableView.getCell(type: LocationTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
        cellCollection.lblPlace.text = places[indexPath.row]
            return cellCollection
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChangeLocationViewController") as! ChangeLocationViewController
      
      
   
//        self.navigationController?.isNavigationBarHidden = false
        isForward = true
        self.navigationController?.pushViewController(controller, animated: true)
        controller.friendSelectedReOpen = { status in
            if status {
                self.coinPopView?(true)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
