//
//  TipPostViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 13/11/2021.
//

import UIKit

class TipPostViewController: BaseViewController, UITextViewDelegate {

    @IBOutlet weak var txtView : UITextView!
    var placeholderLabel = UILabel()
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var addPhotoLabel: UILabel!
    
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var uploadPhotoView: UIView!
    var imagePickerController = UIImagePickerController()
    
    var selectedImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        imageContainerView.isHidden = true
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        imagePickerController.mediaTypes = ["public.image"]
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickerController.sourceType = .camera
        } else {
            imagePickerController.sourceType = .savedPhotosAlbum
        }
    }
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [], NavigationTitle: "Write a Tip" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    
    
    private func setPlaceHolder() {
        txtView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "What's good here, As?"
        placeholderLabel.font = UIFont.get(font: .semiBold, size: 16)
        placeholderLabel.sizeToFit()
        txtView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (txtView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !txtView.text.isEmpty
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func crossTapped(_ sender: UIButton) {
        imageView.image = UIImage()
        selectedImage = UIImage()
        addPhotoLabel.isHidden = false
        uploadPhotoView.isHidden = false
    }
    
    @IBAction func uploadTapped(_ sender: UIButton) {
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    
}

extension TipPostViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else {
                    return
                }
        selectedImage = image
        imageView.image = image
        imageContainerView.isHidden = false
        imagePickerController.dismiss(animated: true, completion: nil)
        addPhotoLabel.isHidden = true
        uploadPhotoView.isHidden = true
    }
}

