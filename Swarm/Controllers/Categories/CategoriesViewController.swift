//
//  CategoriesViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 11/11/2021.
//

import UIKit

class CategoriesViewController: UIViewController,UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    var dataTable : [[String : Any]] = [["cell" : "SortTableViewCell"] , ["cell" : "CategoriesTableViewCell"] ,["cell" : "CategoriesTableViewCell"],["cell" : "CategoriesTableViewCell"] ]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(types: CategoriesTableViewCell.self , SortTableViewCell.self)
       
    }
  
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataTable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let datatable = dataTable[indexPath.row]
        let data = datatable["cell"] as? String ?? ""
        
        switch data {
        case "SortTableViewCell":
            guard let cellCollection = tableView.getCell(type: SortTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    
                return cellCollection
        case "CategoriesTableViewCell":
            guard let cellCollection = tableView.getCell(type: CategoriesTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    
                return cellCollection
        default :
            return UITableViewCell.init(frame: .zero)
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let datatable = dataTable[indexPath.row]
        let data = datatable["cell"] as? String ?? ""
        switch data {
        case "SortTableViewCell":
            return 22
            
        case "CategoriesTableViewCell":
            return 88
            
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("CategoriesHeaderView", owner: self, options: nil)?.first as! CategoriesHeaderView
        
        return headerView
        
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
 

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
