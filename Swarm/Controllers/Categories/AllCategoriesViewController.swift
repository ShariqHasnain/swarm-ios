//
//  AllCategoriesViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 16/11/2021.
//

import UIKit

class AllCategoriesViewController: UIViewController {

    var dataTable : [[String : Any]] = [["cell" : "SortTableViewCell"]  , ["cell" : "CategoriesTableViewCell"] ,["cell" : "CategoriesTableViewCell"] ,["cell" : "CategoriesTableViewCell"]  ]
    @IBOutlet weak var tableView : UITableView! {
        didSet {
            tableView.register(types: CategoriesTableViewCell.self, SortTableViewCell.self)
            tableView.tableFooterView = UIView()
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
  
}
extension AllCategoriesViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataTable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let datatable = dataTable[indexPath.row]
        let data = datatable["cell"] as? String ?? ""
        switch data {
        case "SortTableViewCell":
            guard let cellCollection = tableView.getCell(type: SortTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
            cellCollection.btnSort.setTitle("Most Checked In To", for: .normal)
                return cellCollection
        case "CategoriesTableViewCell":
            guard let cellCollection = tableView.getCell(type: CategoriesTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
            cellCollection.imgCategory.image = UIImage(named: "Mug")
                return cellCollection
        default :
            return UITableViewCell.init(frame: .zero)
        }
       
    }
}
