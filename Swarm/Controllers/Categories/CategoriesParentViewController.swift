//
//  CategoriesParentViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 16/11/2021.
//

import UIKit

class CategoriesParentViewController: BaseViewController, CustomSegmentedControlDelegate {

    @IBOutlet weak var firstChildView: UIView!
    @IBOutlet weak var secondChildView: UIView!
    @IBOutlet weak var segmentControl: CustomSegmentedControl! {
        didSet {
            segmentControl.setButtonTitles(buttonTitles: ["COLLECTIBLE","ALL"])
            segmentControl.selectorViewColor = UIColor.underlineColor
            segmentControl.selectorTextColor = UIColor.selectedTextColor
            segmentControl.textColor = UIColor.btnTextColor
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentControl.delegate = self
        setupNavigationBar()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        secondChildView.isHidden = true
    }
    
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [], NavigationTitle: "Categories" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Segment Delegate
    func change(to index:Int) {
        print("segmentedControl index changed to \(index)")
        
        if index == 0 {
            firstChildView.isHidden = false
            secondChildView.isHidden = true
        } else {
            firstChildView.isHidden = true
            secondChildView.isHidden = false
        }
        
        
    }


}
