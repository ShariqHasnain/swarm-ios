//
//  ContactListViewController.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on on 11/11/2021.
//

import UIKit
import Contacts
import MessageUI

struct FetchedContact {
    var name: String
    var number: String
}

class ContactListViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet weak var tableview: UITableView! {
        didSet {
            tableview.tableFooterView = UIView()
            self.tableview.delegate = self
            self.tableview.dataSource = self
            self.tableview.register(types: ContactMessageTableViewCell.self , EmptyTableViewCell.self,ContactTableViewCell.self)
        }
    }
    
    var isContactAvailable : Bool = false
    var didSelectFriendClousre : ((Bool)->())?
    var contactSelected: (([String])->())?
    
    var selectedFriends = [String]()
    
    var dataTable : [[String : Any]] = [["cell" : "ContactMessageTableViewCell"] , ["cell" : "EmptyTableViewCell"],["cell" : "EmptyTableViewCell"],["cell" : "EmptyTableViewCell"]]
    
    var dataTableContact : [[String : Any]] = [["cell" : "ContactTableViewCell"] ,["cell" : "ContactTableViewCell"]]
    
    var friendsArray : [FetchedContact]  = []
    
    var isSearching = false
    
    var searchedArray = [FetchedContact]()
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        isContactAvailable = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.setupNavigationBar()
        fetchContacts()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Skip", style: .done, target: self, action: #selector(skipTapped))

    }
    
    @objc
    func skipTapped() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "NavigationTabBarViewController") as! NavigationTabBarViewController
        controller.navigationController?.navigationBar.isHidden = true
        controller.modalTransitionStyle = .flipHorizontal
        controller.modalPresentationStyle = .fullScreen
     
        self.present(controller, animated: true, completion: nil)
    }
    
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [], NavigationTitle: "Add Friends" , isSearchBar: false , isLable: true)
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :
                                                                UIColor.white]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.contactSelected?(selectedFriends)
    }
    
    func fetchContacts() {
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            if granted {
                let keys = [CNContactGivenNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                do {
                    try store.enumerateContacts(with: request, usingBlock: { contact, stopPointer in
                        self.friendsArray.append(FetchedContact(name: contact.givenName, number: contact.phoneNumbers.first?.value.stringValue ?? ""))
                    })
                    DispatchQueue.main.async {
                        self.tableview.reloadData()
                    }
                } catch let err {
                    print(err)
                }
            } else {
                print("Contacts access denied")
            }
        }
    }
    
    //MARK: Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isContactAvailable {
            if isSearching {
                return searchedArray.count
            } else {
                return friendsArray.count
            }
            
        }else {
            return dataTable.count
        }
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if isContactAvailable {
                if isSearching {
                    guard let cellCollection = tableView.getCell(type: ContactTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    cellCollection.index = indexPath
                    cellCollection.delegate = self
                    cellCollection.lblFriendName.text = searchedArray[indexPath.row].name
                    cellCollection.phoneNumberLabel.text = searchedArray[indexPath.row].number
                    cellCollection.imageviewAvatar.layer.cornerRadius = cellCollection.imageviewAvatar.height/2
                        return cellCollection
                } else {
                    guard let cellCollection = tableView.getCell(type: ContactTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                    cellCollection.delegate = self
                    cellCollection.lblFriendName.text = friendsArray[indexPath.row].name
                    cellCollection.phoneNumberLabel.text = friendsArray[indexPath.row].number
                    cellCollection.index = indexPath
                    cellCollection.imageviewAvatar.layer.cornerRadius = cellCollection.imageviewAvatar.height/2
                        return cellCollection
                }
        }else {
            let datatable = dataTable[indexPath.row]
            let data = datatable["cell"] as? String ?? ""
            switch data {
            case "ContactMessageTableViewCell" :
                guard let cellCollection = tableView.getCell(type: ContactMessageTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                
                    return cellCollection
            case "EmptyTableViewCell" :
                guard let cellCollection = tableView.getCell(type: EmptyTableViewCell.self,for: indexPath)  else { return UITableViewCell() }
                        
                    return cellCollection
            default :
                return UITableViewCell(frame: .zero)
            }
        }
       
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("ContactHeaderView", owner: self, options: nil)?.first as! ContactHeaderView
        
        headerView.searchWithText = { [weak self] text in
            self?.filterData(text: text)
        }
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }
    
    func filterData(text: String) {
        searchedArray = friendsArray.filter { $0.name.localizedCaseInsensitiveContains(text) }
        if searchedArray.count == 0 {
            isSearching = false
        } else {
            isSearching = true
        }
        tableview.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if isContactAvailable {
            return 0
        }else {
            return 50
        }
       
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("ContactFooterView", owner: self, options: nil)?.first as! ContactFooterView
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? ContactTableViewCell
        if let _ = cell {
            didTapOnCell()
            selectedFriends.append(friendsArray[indexPath.row].name)
        }
    }

    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    func didTapOnCell() {
        print("select Friend")
        self.didSelectFriendClousre?(true)
        //self.navigationController?.popViewController(animated: true)
    }
    
}

extension ContactListViewController: ContactTableCellDelegate {
    
    func inviteTapped(index: IndexPath) {
        if isSearching {
            let item = searchedArray[index.row]
            if MFMessageComposeViewController.canSendText() {

                    let controller = MFMessageComposeViewController()

                    controller.body = "Hello Join Swarm"
                    controller.messageComposeDelegate = self
                    controller.recipients = [item.number]
                    self.present(controller, animated: true, completion: nil)

                }
        } else {
            let item = friendsArray[index.row]
            if MFMessageComposeViewController.canSendText() {

                    let controller = MFMessageComposeViewController()

                    controller.body = "Hello Join Swarm"
                    controller.messageComposeDelegate = self
                    controller.recipients = [item.number]
                    self.present(controller, animated: true, completion: nil)

                }
        }
    }
}

extension ContactListViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}
