//
//  ContactTableViewCell.swift
//  Swarn
//
// Created by Muhammad Essa Ali on on 11/11/2021.
//

import UIKit
protocol ContactTableCellDelegate: AnyObject {
    func inviteTapped(index: IndexPath)
}

class ContactTableViewCell: UITableViewCell , Registerable {

    @IBOutlet weak var lblFriendName : UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var imageviewAvatar: UIImageView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    weak var delegate: ContactTableCellDelegate?
    var index: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setInviteTitle() {
        addButton.setTitle("Invite", for: .normal)
    }
    
    func setAddTitle() {
        addButton.setTitle("Add", for: .normal)
    }
    
    @IBAction func addTapped(_ sender: UIButton) {
        delegate?.inviteTapped(index: index)
    }
    
}
