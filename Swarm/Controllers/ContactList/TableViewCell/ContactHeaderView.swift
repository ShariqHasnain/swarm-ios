//
//  ContactHeaderView.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on11/11/2021.
//

import UIKit

class ContactHeaderView: UIView {

    @IBOutlet weak var contactField: UITextField!
    
    var searchWithText: ((String) -> Void)?
    
    @IBAction func textChanged(_ sender: UITextField) {
        if let text = sender.text {
            searchWithText?(text)
        }
    }

}
