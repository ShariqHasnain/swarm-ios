//
//  LeadershipBoardViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class LeadershipBoardViewController: BaseViewController  {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var rankearnedView: UIView!
    
    var leaders : [String] = ["","","","",]

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "LeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "LeaderTableViewCell")
        tableView.register(UINib(nibName: "EmptyTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyTableViewCell")
        tableView.register(UINib(nibName: "SuggestFriendsAllTableViewCell", bundle: nil), forCellReuseIdentifier:  "SuggestFriendsAllTableViewCell")
        
        rankearnedView.layer.cornerRadius = 10
        
        setupNavigationBar()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(moveTo))
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down-1"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        let menuBtn1 = UIButton(type: .custom)
        menuBtn1.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn1.imageView?.contentMode = .scaleAspectFit
        menuBtn1.setImage(UIImage(named:"add_friend_icon"), for: .normal)
        menuBtn1.addTarget(self, action: #selector(inviteTapped), for: .touchUpInside)
        let menuBarItem1 = UIBarButtonItem(customView: menuBtn1)
        let currWidth1 = menuBarItem1.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth1?.isActive = true
        let currHeight1 = menuBarItem1.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight1?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [menuBarItem1], NavigationTitle: " Leadership" , isSearchBar: false , isLable: true)

    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func inviteTapped() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactListViewController") as! ContactListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func moveTo() {
        let controller  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChangeLocationViewController") as! ChangeLocationViewController
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}


extension LeadershipBoardViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaders.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item <= leaders.count - 1 {
            guard let leaderCell = tableView.dequeueReusableCell(withIdentifier: "LeaderTableViewCell", for: indexPath) as? LeaderTableViewCell else {
                fatalError()
            }
            return leaderCell
        }
        else if indexPath.item == leaders.count + 1 {
            guard let suggestAll = tableView.dequeueReusableCell(withIdentifier: "SuggestFriendsAllTableViewCell", for: indexPath) as? SuggestFriendsAllTableViewCell else {
                fatalError()
            }
            suggestAll.initCell()
            return suggestAll
        }
        
        guard let emptyCell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell", for: indexPath) as? EmptyTableViewCell else {
            fatalError()
        }
        emptyCell.containerView.backgroundColor = .lightGray.withAlphaComponent(0.3)
        return emptyCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item <= leaders.count - 1{
            return 55
        }
        else if indexPath.item == leaders.count{
            return 20
        }
        return 195
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item == leaders.count{
            return 20
        }
        return UITableView.automaticDimension
    }
}
