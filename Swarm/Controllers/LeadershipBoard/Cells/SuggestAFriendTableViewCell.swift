//
//  SuggestedFriendsTableViewCell.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class SuggestAFriendTableViewCell: UITableViewCell {

    @IBOutlet weak var crossButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCell() {
        let image = UIImage(named: "crossWhite")?.withRenderingMode(.alwaysTemplate)
        crossButton.setImage(image, for: .normal)
        crossButton.tintColor = UIColor.gray
        crossButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
}
