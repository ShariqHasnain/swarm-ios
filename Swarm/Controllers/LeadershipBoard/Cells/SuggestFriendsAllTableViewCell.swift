//
//  SuggestFriendsAllTableViewCell.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class SuggestFriendsAllTableViewCell: UITableViewCell {


    @IBOutlet weak var suggesttableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        suggesttableView.register(UINib(nibName: "SuggestAFriendTableViewCell", bundle: nil), forCellReuseIdentifier: "SuggestAFriendTableViewCell")
        suggesttableView.delegate = self
        suggesttableView.dataSource = self
      
        self.suggesttableView.separatorColor = .clear
        
        initCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCell() {
        self.suggesttableView.reloadData()
    }
    
}


extension SuggestFriendsAllTableViewCell : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestAFriendTableViewCell", for: indexPath) as? SuggestAFriendTableViewCell else {fatalError()}
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0
    }
}
