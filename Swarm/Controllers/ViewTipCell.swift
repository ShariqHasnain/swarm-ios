//
//  ViewTipCell.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 28/11/2021.
//

import UIKit

class ViewTipCell: UITableViewCell , Registerable {

    @IBOutlet weak var btnUpVote : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func btnUpVoteTapped(_ sender : UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            btnUpVote.setImage(UIImage(named: "dropDown"), for: .selected)
        } else {
            btnUpVote.setImage(UIImage(named: "upArrow"), for: .normal)
        }
    }
}
