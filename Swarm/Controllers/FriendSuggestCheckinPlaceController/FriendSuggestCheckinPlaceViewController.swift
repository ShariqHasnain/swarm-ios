//
//  FriendSuggestCheckinPlaceViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class FriendSuggestCheckinPlaceViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "FriendSuggestCheckinPlaceMessageTableViewCell", bundle: nil), forCellReuseIdentifier: "FriendSuggestCheckinPlaceMessageTableViewCell")
        tableView.register(UINib(nibName: "FriendSuggestCheckinPlaceTableViewCell", bundle: nil), forCellReuseIdentifier: "FriendSuggestCheckinPlaceTableViewCell")
        tableView.register(UINib(nibName: "FriendSuggestCheckinPlaceMessageTableViewCell", bundle: nil), forCellReuseIdentifier: "FriendSuggestCheckinPlaceMessageTableViewCell")
        
        setupNavigationBar()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(moveTo))
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupNavigationBar() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage(named:"Icon feather-arrow-down"), for: .normal)
        menuBtn.addTarget(self, action: #selector(tapOnBackBtn), for: .touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        ForgetPasswordCustom(navigationLeftBarButtonItems: [menuBarItem], navigationRightBarButtonsItems: [], NavigationTitle: "" , isSearchBar: false , isLable: false)

    }
    
    @objc func tapOnBackBtn() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func moveTo() {
        let controller  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeadershipBoardViewController") as! LeadershipBoardViewController
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}


extension FriendSuggestCheckinPlaceViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item == 0 {
            guard let leaderCell = tableView.dequeueReusableCell(withIdentifier: "FriendSuggestCheckinPlaceMessageTableViewCell", for: indexPath) as? FriendSuggestCheckinPlaceMessageTableViewCell else {
                fatalError()
            }
            return leaderCell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FriendSuggestCheckinPlaceTableViewCell", for: indexPath) as? FriendSuggestCheckinPlaceTableViewCell else {
            fatalError()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item == 0 {
            return 223
        }
        return 285
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
