//
//  FriendSuggestCheckinPlaceTableViewCell.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class FriendSuggestCheckinPlaceTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
