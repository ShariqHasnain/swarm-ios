//
//  FriendSuggestCheckinPlaceMessageTableViewCell.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit

class FriendSuggestCheckinPlaceMessageTableViewCell: UITableViewCell {

    @IBOutlet weak var dontAllowButton: UIButton!
    @IBOutlet weak var allowButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dontAllowButton.layer.borderWidth = 1
        dontAllowButton.layer.borderColor = UIColor(red: 1.00, green: 0.65, blue: 0.20, alpha: 1.00).cgColor
        dontAllowButton.layer.cornerRadius = dontAllowButton.frame.height/2
        allowButton.layer.cornerRadius = dontAllowButton.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
