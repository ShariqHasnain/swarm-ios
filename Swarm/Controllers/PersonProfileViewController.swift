//
//  PersonProfileViewController.swift
//  Swarn
//
//  Created by Ruttab Haroon on 25/12/2021.
//

import UIKit
import DropDown

class PersonProfileViewController: BaseViewController {

    @IBOutlet weak var backArrowButton: UIButton!
    @IBOutlet weak var verticalButton: UIButton!
    @IBOutlet weak var addFriendButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var addFriendHeightConstraint: NSLayoutConstraint!
    
    var isAddedAsFriend = "0" {
        didSet {
            self.addFriendButton.isHidden = self.isAddedAsFriend == "1" ? true : false
            //addFriendHeightConstraint.constant = self.isAddedAsFriend == "1"  ? 0 : 50
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backArrowButton.setTitle("", for: .normal)
        verticalButton.setTitle("", for: .normal)
        addFriendButton.layer.cornerRadius = addFriendButton.frame.height/2
        
        let isadded = UserDefaults.standard.string(forKey: "isAddedAsFriend") as? String ?? "0"
        self.isAddedAsFriend = isadded
        
//        if self.isAddedAsFriend == "1" {
//            self.addFriendButton.isHidden = true
//        }
//        else {
//            self.addFriendButton.isHidden = false
//        }
        
    }
    
    
    @IBAction func onAddTapped(_ sender: Any) {
        addFriendButton.isEnabled = false
        addFriendButton.backgroundColor = .lightGray.withAlphaComponent(0.5)
        addFriendButton.setTitle("Request Pending", for: .disabled)
        UserDefaults.standard.set("1", forKey: "isAddedAsFriend")
    }
    
    
    @IBAction func onBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onMoreTapped(_ sender: Any) {
        let dropDown = DropDown()
        dropDown.dataSource = ["Unfriend", "Mute Notification", "Contact"]
        dropDown.anchorView = moreButton
        dropDown.direction = .bottom
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            let vc = AppStoryboard.Components.instance.instantiateViewController(identifier: "RemoveFriendPopUpViewController") as! RemoveFriendPopUpViewController
            vc.onComplete = { [weak self] status in
                guard let weakSelf = self else {return}
                if status == true {
                    weakSelf.isAddedAsFriend = "0"
                }
            }
            UserDefaults.standard.set("0", forKey: "isAddedAsFriend")
            self.present(vc, animated: true, completion: nil)
            dropDown.hide()
        }
        dropDown.show()

    }
    
    @IBAction func didTapOnCheckIn(_ sender: UIButton) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "ViewCheckInsViewController") as! ViewCheckInsViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func didTapOnCategories(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "CategoriesParentViewController") as! CategoriesParentViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func didTapOnMayorShip(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "MayorshipsViewController") as! MayorshipsViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func didTapOnSavePlaces(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(vc, animated: true)
  
    }
    
   
    @IBAction func didTapOnStreaks(_ sender: Any) {
        let vc = AppStoryboard.Profile.instance.instantiateViewController(identifier: "StreaksViewController") as! StreaksViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func didTapOnvisitedPlace(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(identifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
