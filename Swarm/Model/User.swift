//
//  User.swift
//  Swarm
//
//  Created by Syed Zeeshan Rizvi on 2/24/22.
//

import Foundation


struct User: Codable {
    var status: Bool
    var message: String
    var data: UserProfile?
}

struct UserProfile: Codable {
    var token: String?
    var profile: Profile?
}

struct Profile: Codable {
    var id: Int?
    var first_name: String?
    var last_name: String?
    var email: String?
    var birth_date: String?
    var gender: String?
    var phone_number: String?
    var avatar: String?
}
