//
//  CheckInList.swift
//  Swarm
//
//  Created by Mohammad Arsalan on 19/03/2022.
//

import Foundation

struct CheckInList : Codable {
    let status : Bool?
    let message : String?
    let data : CheckInListData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CheckInListData.self, forKey: .data)
    }

}

struct CheckInListData : Codable {
    let checkIns : CheckIns?

    enum CodingKeys: String, CodingKey {

        case checkIns = "checkins"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        checkIns = try values.decodeIfPresent(CheckIns.self, forKey: .checkIns)
    }

}

struct CheckIns : Codable {
    let current_page : Int?
    let _data : [CheckInsData]
    let first_page_url : String?
    let from : Int?
    let last_page : Int?
    let last_page_url : String?
    let next_page_url : String?
    let path : String?
    let per_page : String?
    let prev_page_url : String?
    let to : Int?
    let total : Int?

    enum CodingKeys: String, CodingKey {

        case current_page = "current_page"
        case _data = "data"
        case first_page_url = "first_page_url"
        case from = "from"
        case last_page = "last_page"
        case last_page_url = "last_page_url"
        case next_page_url = "next_page_url"
        case path = "path"
        case per_page = "per_page"
        case prev_page_url = "prev_page_url"
        case to = "to"
        case total = "total"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        current_page = try values.decodeIfPresent(Int.self, forKey: .current_page)
        _data = try values.decode([CheckInsData].self, forKey: ._data)
        first_page_url = try values.decodeIfPresent(String.self, forKey: .first_page_url)
        from = try values.decodeIfPresent(Int.self, forKey: .from)
        last_page = try values.decodeIfPresent(Int.self, forKey: .last_page)
        last_page_url = try values.decodeIfPresent(String.self, forKey: .last_page_url)
        next_page_url = try values.decodeIfPresent(String.self, forKey: .next_page_url)
        path = try values.decodeIfPresent(String.self, forKey: .path)
        per_page = try values.decodeIfPresent(String.self, forKey: .per_page)
        prev_page_url = try values.decodeIfPresent(String.self, forKey: .prev_page_url)
        to = try values.decodeIfPresent(Int.self, forKey: .to)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
    }

}

struct CheckInsData: Codable {
    let date: String? // "2022-03-21",
    let checkins: [DateCheckIns]?
    
    enum CodingKeys: String, CodingKey {

        case date = "date"
        case checkins = "checkins"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        checkins = try values.decodeIfPresent([DateCheckIns].self, forKey: .checkins)
    }
    
}

struct DateCheckIns : Codable {
    let id : Int?
    let user_id : Int?
    let lat : String?
    let lng : String?
    let place_id : Int?
    let description : String?
    let sticker_id : Int?
    let off_the_grid : Bool?
    let share_on_fb : Bool?
    let share_on_twitter : Bool?
    let rating : String?
    let created_at : String?
    let owner : Owner?
    let media : [Media]?
    let place : Place?
    let sticker : [Sticker]?
    let comments : [Comments]?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case lat = "lat"
        case lng = "lng"
        case place_id = "place_id"
        case description = "description"
        case sticker_id = "sticker_id"
        case off_the_grid = "off_the_grid"
        case share_on_fb = "share_on_fb"
        case share_on_twitter = "share_on_twitter"
        case rating = "rating"
        case created_at = "created_at"
        case owner = "owner"
        case media = "media"
        case place = "place"
        case sticker = "sticker"
        case comments = "comments"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        lng = try values.decodeIfPresent(String.self, forKey: .lng)
        place_id = try values.decodeIfPresent(Int.self, forKey: .place_id)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        sticker_id = try values.decodeIfPresent(Int.self, forKey: .sticker_id)
        off_the_grid = try values.decodeIfPresent(Bool.self, forKey: .off_the_grid)
        share_on_fb = try values.decodeIfPresent(Bool.self, forKey: .share_on_fb)
        share_on_twitter = try values.decodeIfPresent(Bool.self, forKey: .share_on_twitter)
        rating = try values.decodeIfPresent(String.self, forKey: .rating)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        owner = try values.decodeIfPresent(Owner.self, forKey: .owner)
        media = try values.decodeIfPresent([Media].self, forKey: .media)
        place = try values.decodeIfPresent(Place.self, forKey: .place)
        sticker = try values.decodeIfPresent([Sticker].self, forKey: .sticker)
        comments = try values.decodeIfPresent([Comments].self, forKey: .comments)
    }

}

struct Owner : Codable {
    let id : Int?
    let first_name : String?
    let last_name : String?
    let email : String?
    let birth_date : String?
    let gender : String?
    let phone_number : String?
    let fb_id : String?
    let fb_data : String?
    let google_id : String?
    let google_data : String?
    let apple_id : String?
    let apple_data : String?
    let avatar : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case first_name = "first_name"
        case last_name = "last_name"
        case email = "email"
        case birth_date = "birth_date"
        case gender = "gender"
        case phone_number = "phone_number"
        case fb_id = "fb_id"
        case fb_data = "fb_data"
        case google_id = "google_id"
        case google_data = "google_data"
        case apple_id = "apple_id"
        case apple_data = "apple_data"
        case avatar = "avatar"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        birth_date = try values.decodeIfPresent(String.self, forKey: .birth_date)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
        fb_id = try values.decodeIfPresent(String.self, forKey: .fb_id)
        fb_data = try values.decodeIfPresent(String.self, forKey: .fb_data)
        google_id = try values.decodeIfPresent(String.self, forKey: .google_id)
        google_data = try values.decodeIfPresent(String.self, forKey: .google_data)
        apple_id = try values.decodeIfPresent(String.self, forKey: .apple_id)
        apple_data = try values.decodeIfPresent(String.self, forKey: .apple_data)
        avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
    }

}

struct Media : Codable {
    let id : Int?
    let check_in_id : Int?
    let media : String?
    let is_featured : Int?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case check_in_id = "check_in_id"
        case media = "media"
        case is_featured = "is_featured"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        check_in_id = try values.decodeIfPresent(Int.self, forKey: .check_in_id)
        media = try values.decodeIfPresent(String.self, forKey: .media)
        is_featured = try values.decodeIfPresent(Int.self, forKey: .is_featured)
    }

}

struct Place : Codable {
    let id : Int?
    let lat : Double?
    let lng : Double?
    let name : String?
    let rating : String?
    let categories : [Categories]?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case lat = "lat"
        case lng = "lng"
        case name = "name"
        case rating = "rating"
        case categories = "categories"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        lng = try values.decodeIfPresent(Double.self, forKey: .lng)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        rating = try values.decodeIfPresent(String.self, forKey: .rating)
        categories = try values.decodeIfPresent([Categories].self, forKey: .categories)
    }

}

struct Categories : Codable {
    let id : Int?
    let place_id : Int?
    let category_id : Int?
    let created_at : String?
    let updated_at : String?
    let category : Category?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case place_id = "place_id"
        case category_id = "category_id"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case category = "category"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        place_id = try values.decodeIfPresent(Int.self, forKey: .place_id)
        category_id = try values.decodeIfPresent(Int.self, forKey: .category_id)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        category = try values.decodeIfPresent(Category.self, forKey: .category)
    }

}

struct Category : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

struct Sticker : Codable {
    let id : Int?
    let user_id : Int?
    let sticker_id : Int?
    let check_in_id : Int?
    let name : String?
    let media : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case sticker_id = "sticker_id"
        case check_in_id = "check_in_id"
        case name = "name"
        case media = "media"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        sticker_id = try values.decodeIfPresent(Int.self, forKey: .sticker_id)
        check_in_id = try values.decodeIfPresent(Int.self, forKey: .check_in_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        media = try values.decodeIfPresent(String.self, forKey: .media)
    }

}
struct Comments : Codable {
    let id : Int?
    let user_id : Int?
    let check_in_id : Int?
    let sticker_id : Int?
    let sticker_name : String?
    let sticker_media : String?
    let comment : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case check_in_id = "check_in_id"
        case sticker_id = "sticker_id"
        case sticker_name = "sticker_name"
        case sticker_media = "sticker_media"
        case comment = "comment"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        check_in_id = try values.decodeIfPresent(Int.self, forKey: .check_in_id)
        sticker_id = try values.decodeIfPresent(Int.self, forKey: .sticker_id)
        sticker_name = try values.decodeIfPresent(String.self, forKey: .sticker_name)
        sticker_media = try values.decodeIfPresent(String.self, forKey: .sticker_media)
        comment = try values.decodeIfPresent(String.self, forKey: .comment)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}
