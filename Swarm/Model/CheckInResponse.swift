//
//  CheckInResponse.swift
//  Swarm
//
//  Created by Mohammad Arsalan on 21/03/2022.
//

import Foundation

struct CheckInResponse : Codable {
    let status : Bool?
    let message : String?
    let data : CheckInResponseData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CheckInResponseData.self, forKey: .data)
    }
}
struct CheckInResponseData : Codable {
    let response : [CheckInResponseMessages]?
    let checkin : CheckInResponseCheckin?

    enum CodingKeys: String, CodingKey {

        case response = "response"
        case checkin = "checkin"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        response = try values.decodeIfPresent([CheckInResponseMessages].self, forKey: .response)
        checkin = try values.decodeIfPresent(CheckInResponseCheckin.self, forKey: .checkin)
    }
}
struct CheckInResponseMessages : Codable {
    let text : String?
    let media : String?

    enum CodingKeys: String, CodingKey {

        case text = "text"
        case media = "media"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        text = try values.decodeIfPresent(String.self, forKey: .text)
        media = try values.decodeIfPresent(String.self, forKey: .media)
    }
}


struct CheckInResponseCheckin : Codable {
    let id : Int?
    let user_id : Int?
    let lat : String?
    let lng : String?
    let place_id : Int?
    let description : String?
    let sticker_id : Int?
    let off_the_grid : Bool?
    let share_on_fb : Bool?
    let share_on_twitter : Bool?
    let rating : String?
    let created_at : String?
    let owner : Owner?
    let media : [Media]?
    let place : Place?
    let sticker : [Sticker]?
    let comments : [Comments]?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case lat = "lat"
        case lng = "lng"
        case place_id = "place_id"
        case description = "description"
        case sticker_id = "sticker_id"
        case off_the_grid = "off_the_grid"
        case share_on_fb = "share_on_fb"
        case share_on_twitter = "share_on_twitter"
        case rating = "rating"
        case created_at = "created_at"
        case owner = "owner"
        case media = "media"
        case place = "place"
        case sticker = "sticker"
        case comments = "comments"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        lng = try values.decodeIfPresent(String.self, forKey: .lng)
        place_id = try values.decodeIfPresent(Int.self, forKey: .place_id)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        sticker_id = try values.decodeIfPresent(Int.self, forKey: .sticker_id)
        off_the_grid = try values.decodeIfPresent(Bool.self, forKey: .off_the_grid)
        share_on_fb = try values.decodeIfPresent(Bool.self, forKey: .share_on_fb)
        share_on_twitter = try values.decodeIfPresent(Bool.self, forKey: .share_on_twitter)
        rating = try values.decodeIfPresent(String.self, forKey: .rating)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        owner = try values.decodeIfPresent(Owner.self, forKey: .owner)
        media = try values.decodeIfPresent([Media].self, forKey: .media)
        place = try values.decodeIfPresent(Place.self, forKey: .place)
        sticker = try values.decodeIfPresent([Sticker].self, forKey: .sticker)
        comments = try values.decodeIfPresent([Comments].self, forKey: .comments)
    }

}
