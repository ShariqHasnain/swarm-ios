//
//  Intro.swift
//  Swarm
//
//  Created by Syed Zeeshan Rizvi on 2/27/22.
//

import Foundation

struct Intro: Codable {
    var status: Bool
    var message: String
    var data: IntroData
}

struct IntroData: Codable {
    var intro_screens: [IntroScreens]
}

struct IntroScreens: Codable {
    var media: String
    var text: String
    var is_last: Bool
}
