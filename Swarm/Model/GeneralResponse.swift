//
//  GeneralResponse.swift
//  Swarm
//
//  Created by Syed Zeeshan Rizvi on 2/27/22.
//

import Foundation

struct GeneralResponse: Codable {
    var status: Bool
    var message: String
}
