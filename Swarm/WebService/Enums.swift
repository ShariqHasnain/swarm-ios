//
//  Enums.swift
//  Swarm
//
//  Created by Syed Zeeshan Rizvi on 2/24/22.
//

import Foundation

enum LoginType: String {
    case defaultLogin = "default"
    case fb
    case google
    case apple
}

enum EndPoint: String {
    case login
    case forgot = "password/forgot"
    case update = "password/update"
    case requestEmail = "request/verification/otp"
    case register
    case intro = "app/intro"
    
    //MARK: CHECKIN ENDPOINTS
    
    case checkIn = "check-in"
    case editCheckIn = "check-in/edit"
    case rateCheckIn = "check-in/rate"
    case addMediaCheckIn = "check-in/add-media"
    case commentCheckIn = "check-in/comment"
    case removeCheckIn = "check-in/remove"
    case checkInDetails = "check-in/details"
    case checkInLists = "check-in/list"
}
