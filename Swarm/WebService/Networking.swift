//
//  Networking.swift
//  Swarm
//
//  Created by Syed Zeeshan Rizvi on 2/24/22.
//

import Foundation
import Alamofire
import UIKit

class Networking {
    
    static var shared = Networking()
    
    private init() {}
    
    func login(endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<User, Error>) -> ()) {
        requestPost(endPoint: endPoint, parameters: parameters, completion: completion)
    }
    
    func forgetPassword(endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<GeneralResponse, Error>) -> ()) {
        requestPost(endPoint: endPoint, parameters: parameters, completion: completion)
    }
    
    func updatePassword(endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<GeneralResponse, Error>) -> ()) {
        requestPost(endPoint: endPoint, parameters: parameters, completion: completion)
    }
    
    func signUp(image: UIImage?, endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<User, Error>) -> ()) {
        requestMultipart(image: image, endPoint: endPoint, parameters: parameters, completion: completion)
    }
    
    func getIntro(endPoint: EndPoint, completion: @escaping (Result<Intro, Error>) -> ()) {
        requestGet(endPoint: endPoint, completion: completion)
    }
    
    func requestMultipart<T: Decodable>(image: UIImage?, endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<T, Error>) -> ()) {
        
        let url = Route.baseUrl + endPoint.rawValue
        
//        var request = URLRequest(url: URL(string: url)!)
//
//        request.httpMethod = HTTPMethod.post.rawValue
//        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let token = UserDefaults.standard.string(forKey: "token") ?? ""
//        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
//        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        let headers: HTTPHeaders =
                ["Content-type": "multipart/form-data",
                "Accept": "application/json",
                 "Authorization": "Bearer \(token)"]
        //        post(url: URL(string: url)!, parameters: parameters) { data, response in
        //            print(data)
        //        } failure: { error, data in
        //            print(data)
        //        }
        print(parameters)
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let img = image {
                if let imgData = img.pngData() {
                    multipartFormData.append(imgData, withName: "avatar", fileName: "image.png", mimeType: "image/png")
                }
            }
        }, to: url, method: .post, headers: headers).responseDecodable(of: T.self) { response in
            print(response)
            switch response.result {
            case.success(let value):
                completion(.success(value))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

func requestPost<T: Decodable>(endPoint: EndPoint, parameters: [String: Any], completion: @escaping (Result<T, Error>) -> ()) {
    
    let url = Route.baseUrl + endPoint.rawValue
    
    var request = URLRequest(url: URL(string: url)!)
    
    request.httpMethod = HTTPMethod.post.rawValue
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    let token = UserDefaults.standard.string(forKey: "token") ?? ""
    request.setValue( "Bearer \(token)", forHTTPHeaderField: "Authorization")
    request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
    
    //            post(url: URL(string: url)!, parameters: parameters) { data, response in
    //                print(data)
    //            } failure: { error, data in
    //                print(data)
    //            }
    
    AF.request(request)
        .validate()
        .responseDecodable(of: T.self) { response in
            switch response.result {
            case.success(let value):
                print(value)
                completion(.success(value))
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
    
}

func requestGet<T: Decodable>(endPoint: EndPoint, queryItems: [URLQueryItem]? = nil, parameters: [String: Any]? = nil, completion: @escaping (Result<T, Error>) -> ()) {
    
    var components = URLComponents(string: Route.baseUrl + endPoint.rawValue)
//    let url = Route.baseUrl + endPoint.rawValue
    components?.queryItems = queryItems
    
//    var request = URLRequest(url: URL(string: url)!)
    
    var request = URLRequest(url: (components?.url)!)
    
    request.httpMethod = HTTPMethod.get.rawValue
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    let token = UserDefaults.standard.string(forKey: "token") ?? ""
    request.setValue( "Bearer \(token)", forHTTPHeaderField: "Authorization")
    if let param = parameters {
        request.httpBody = try! JSONSerialization.data(withJSONObject: param, options: [])
    }
    
    //            post(url: URL(string: url)!, parameters: parameters) { data, response in
    //                print(data)
    //            } failure: { error, data in
    //                print(data)
    //            }
    
    AF.request(request)
        .validate()
        .responseDecodable(of: T.self) { response in
            switch response.result {
            case.success(let value):
                print(value)
                completion(.success(value))
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
    
}

func getReq(endPoint: EndPoint, queryItems: [URLQueryItem]? = nil, success: @escaping (Data?, HTTPURLResponse?) -> (), failure: @escaping (NSError, Data?) -> ()) {
    let config = URLSessionConfiguration.default
    let session = URLSession(configuration: config)
    
    var components = URLComponents(string: Route.baseUrl + endPoint.rawValue)
//    let url = Route.baseUrl + endPoint.rawValue
    components?.queryItems = queryItems
    
//    var request = URLRequest(url: URL(string: url)!)
    
    var request = URLRequest(url: (components?.url)!)
    
    request.httpMethod = HTTPMethod.get.rawValue
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    let token = UserDefaults.standard.string(forKey: "token") ?? ""
    request.setValue( "Bearer \(token)", forHTTPHeaderField: "Authorization")
    
//    var urlRequest = URLRequest(url: url)
//    urlRequest.httpMethod = "GET"
    
    do {
   
        //            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        print("------POST REQUEST START-----")
        print(request)
        print(queryItems)
        print("------POST REQUEST END-------")
        let sessionTask = session.dataTask(with: request, completionHandler: { data, response, error in
            if error != nil {
                failure(error! as NSError, data)
                return
            }
            else {
                if let httpResponse = response as? HTTPURLResponse {
                    print("------POST RESPONSE START-----")
                    print("response code = \(httpResponse.statusCode)")
                    if let responseData = data {
                        if let strResponse = String(bytes: responseData, encoding: .utf8) {
                            print("\(strResponse)")
                        } else {
                            print("could not convert response data into string")
                        }
                    } else {
                        print("no response data")
                    }
                    print("------POST RESPONSE END-------")
                    if httpResponse.statusCode == 200 {
                        //success case
                        success(data, httpResponse)
                    }
                    else if httpResponse.statusCode == 404 {
                        
                        failure(NSError(domain: "WebServiceError", code: httpResponse.statusCode), data)
                    }
                    else if httpResponse.statusCode == 400 {
                        
                        failure(NSError(domain: "WebServiceError", code: httpResponse.statusCode), data)
                    }
                    else {
                        var errorMessage = ""
                        if let responseData = data {
                            errorMessage = String(bytes: responseData, encoding: String.Encoding.utf8) ?? "Empty error response"
                        } else {
                            errorMessage = "Empty error response"
                        }
                        
                        failure(NSError(domain: "WebServiceError", code: httpResponse.statusCode, userInfo: ["message": errorMessage]), data)
                    }
                } else {
                    print("could not determine httpurlresponse")
                }
            }
        })
        sessionTask.resume()
    } catch {
        failure(error as NSError, nil)
    }
}


