//
//  Route.swift
//  Swarm
//
//  Created by Syed Zeeshan Rizvi on 2/24/22.
//

import Foundation

struct Route {
    static let baseUrl = "\(mainRoute)\(route)"
    static private let route = "api/"
    static let mainRoute = "https://swarm.mubassir.dev/"
}
