//
//  Shadowable.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 14/11/2021.
//

import UIKit

protocol Shadowable: UIView {
    func applyShadow()
    func removeShadow()
}

extension Shadowable {
    
    func applyShadow() {
        layer.masksToBounds = true
        layer.shadowRadius = 2
        layer.shadowOpacity = 1
        layer.shadowColor =  UIColor.separator.withAlphaComponent(0.48).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1)
        
    }
    
    func removeShadow() {
        layer.shadowColor = UIColor.clear.cgColor
    }
    
}
