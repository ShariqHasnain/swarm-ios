//
//  SearchBar.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 10/11/2021.
//
enum BarType {case normal,withBack}
enum SearchType {case local,server}

import UIKit

//public enum SearchType {
//    case local
//    case server
//}

class SearchBar: UIView {
    
    //MARK:- Outlets
   
    @IBOutlet private weak var btnSearch: UIButton!
    @IBOutlet private weak var borderView: UIView!
    @IBOutlet private weak var txtSearhbar: UITextField!
    //MARK:- Vars
    
    private var pendingRequestWorkItem: DispatchWorkItem?
    var type:BarType = .normal {
        didSet {
            if type == .withBack {
                btnSearch.setImage(#imageLiteral(resourceName: "iconBack"), for: .normal)
                btnSearch.isUserInteractionEnabled = true
            }
        }
    }
    
    var searchtType:SearchType = .server
    
    weak var searchdelegate:SearchBarDelegate? = nil
    //MARK:- Cycles
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @discardableResult
    override func endEditing(_ force: Bool) -> Bool {
        self.txtSearhbar.endEditing(force)
    }
    
    var placeHolder  = "" {
        didSet  {
            txtSearhbar.placeholder = "Search Contacts"
        }
    }
    
    var text  = "" {
        didSet  {
            txtSearhbar.text = text
        }
    }

    public var borderActiveColor:UIColor = UIColor.gray {
        didSet{
            borderView.backgroundColor = isActive ? borderActiveColor : borderInactiveColor
        }
    }
    
    public var borderInactiveColor:UIColor = UIColor.gray {
        didSet{
            borderView.backgroundColor = isActive ? borderActiveColor : borderInactiveColor
        }
    }
    
    public var isActive:Bool = false {
        didSet {
            borderView.backgroundColor = isActive ? borderActiveColor : borderInactiveColor
        }
    }
   
    //MARK:- LifeCycles
    override  func awakeFromNib() {
        super.awakeFromNib()
    }
    
    deinit {
        pendingRequestWorkItem?.cancel()
    }
    
    @IBAction func back(_ sender: Any) {
        if searchdelegate != nil,searchdelegate?.didPressBack != nil {
         searchdelegate?.didPressBack!()
        }
    }
    //MARK:- Helpers
    private func refreshSearch(text:String) {
        guard searchtType == .server else {
            self.searchdelegate?.textDidChanged(text: text)
            return
        }
        // Cancel the currently pending item
        pendingRequestWorkItem?.cancel()
        // Wrap our request in a work item
        let requestWorkItem = DispatchWorkItem { [weak self] in
          guard let self = self else { return }
            self.searchdelegate?.textDidChanged(text: text)
        }
        // Save the new work item and execute it after 700 ms
        pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(700),
                         execute: requestWorkItem)
      }
    
    @IBAction func editingDidChanged(_ sender: UITextField) {
        guard let text = sender.text else { return }
        refreshSearch(text: text)//delayed Call
    }
    
    @IBAction func editingDidBegin(_ sender: UITextField) {
        if let delegate = searchdelegate ,delegate.textDidBeginEditing != nil {
            delegate.textDidBeginEditing!()
        }
    }
    
    @IBAction func editingDidEnd(_ sender: UITextField) {
        guard let text = sender.text, !text.isEmpty else {
            return
        }
    }
}

extension SearchBar  : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text  else { return true }
        self.refreshSearch(text: text)
        return true
    }
}

@objc protocol SearchBarDelegate:AnyObject {
    func textDidChanged(text:String)
    func textDidEnd(text:String)
    @objc optional func didPressBack()
    @objc optional func textDidBeginEditing()
}
