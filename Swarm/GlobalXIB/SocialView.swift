//
//  SocialView.swift
//  Swarn
//
//  Created by Muhammad Essa Ali on 10/11/2021.
//

import UIKit

class SocialView: UIView {
    
    @IBOutlet weak var lblContacts : UILabel!
    @IBOutlet weak var lblFacebook : UILabel!
    @IBOutlet weak var lblTwitter : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func btnContactsTapped(_ sender : UIButton) {}
    @IBAction func btnFacebookTapped(_ sender : UIButton) {}
    @IBAction func btnTwitterTapped(_ sender : UIButton) {}
}
